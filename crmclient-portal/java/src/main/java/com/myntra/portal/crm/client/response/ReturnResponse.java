package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.ReturnEntry;

/**
 * Response for customer return with comments.
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "returnResponse")
public class ReturnResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<ReturnEntry> returnEntryList;

	public ReturnResponse() {
		super();
	}

	@XmlElementWrapper(name = "returnEntryList")
	@XmlElement(name = "returnEntry")
	public List<ReturnEntry> getReturnEntryList() {
		return returnEntryList;
	}

	public void setReturnEntryList(List<ReturnEntry> returnEntryList) {
		this.returnEntryList = returnEntryList;
	}

	@Override
	public String toString() {
		return "ReturnResponse [returnEntryList=" + returnEntryList + "]";
	}

}