package com.myntra.portal.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;

/**
 * Response for customer order comment web service
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerOrderCommentResponse")
public class CustomerOrderCommentResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<CustomerOrderCommentEntry> customerOrderCommentEntryList;

	public CustomerOrderCommentResponse() {		
	}
	
	@XmlElementWrapper(name = "customerOrderCommentEntryList")
	@XmlElement(name = "customerOrderCommentEntry")
	public List<CustomerOrderCommentEntry> getCustomerOrderCommentEntryList() {
		return customerOrderCommentEntryList;
	}	

	public void setCustomerOrderCommentEntryList(List<CustomerOrderCommentEntry> customerOrderCommentEntryList) {
		this.customerOrderCommentEntryList = customerOrderCommentEntryList;
	}

	@Override
	public String toString() {
		return "CustomerOrderCommentResponse [customerOrderCommentEntryList=" + customerOrderCommentEntryList + "]";
	}
}