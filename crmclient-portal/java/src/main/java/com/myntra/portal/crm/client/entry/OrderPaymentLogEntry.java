package com.myntra.portal.crm.client.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for order payment log
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderPaymentLogEntry")
public class OrderPaymentLogEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private Long logId;
	private String login;
	private Long orderId;

	private String userAgent;
	private String IPAddress;
	private String gateway;
	private String paymentOption;
	private String paymentIssuer;
	private String bankTransactionId;
	private String gatewayPaymentId;
	private String completedVia;
	private String cardBankName;
	private Long binNumber;

	// amount
	private Double amount;
	private Double amountPaid;
	private Double amountToBePaid;

	private Boolean isInline;
	private Boolean isTampered;
	private Boolean isComplete;
	private Boolean isFlagged;

	private String responseCode;
	private String responseMessage;

	// date
	private Date logInsertTime;
	private Date returnTime;
	private Date transactionTime;
	
	public OrderPaymentLogEntry(){
		
	}

	public OrderPaymentLogEntry(Long logId, String login, Long orderId, String userAgent, String iPAddress,
			String gateway, String paymentOption, String paymentIssuer, String bankTransactionId,
			String gatewayPaymentId, String completedVia, String cardBankName, Long binNumber, Double amount,
			Double amountPaid, Double amountToBePaid, Boolean isInline, Boolean isTampered, Boolean isComplete,
			Boolean isFlagged, String responseCode, String responseMessage, Date logInsertTime, Date returnTime,
			Date transactionTime) {

		this.logId = logId;
		this.login = login;
		this.orderId = orderId;
		this.userAgent = userAgent;
		this.IPAddress = iPAddress;
		this.gateway = gateway;
		this.paymentOption = paymentOption;
		this.paymentIssuer = paymentIssuer;
		this.bankTransactionId = bankTransactionId;
		this.gatewayPaymentId = gatewayPaymentId;
		this.completedVia = completedVia;
		this.cardBankName = cardBankName;
		this.binNumber = binNumber;
		this.amount = amount;
		this.amountPaid = amountPaid;
		this.amountToBePaid = amountToBePaid;
		this.isInline = isInline;
		this.isTampered = isTampered;
		this.isComplete = isComplete;
		this.isFlagged = isFlagged;
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.logInsertTime = logInsertTime;
		this.returnTime = returnTime;
		this.transactionTime = transactionTime;		
	}

	public Long getLogId() {
		return logId;
	}

	public String getLogin() {
		return login;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public String getIPAddress() {
		return IPAddress;
	}

	public String getGateway() {
		return gateway;
	}

	public String getPaymentOption() {
		return paymentOption;
	}

	public String getPaymentIssuer() {
		return paymentIssuer;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public String getGatewayPaymentId() {
		return gatewayPaymentId;
	}

	public String getCompletedVia() {
		return completedVia;
	}

	public String getCardBankName() {
		return cardBankName;
	}

	public Long getBinNumber() {
		return binNumber;
	}

	public Double getAmount() {
		return amount;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public Double getAmountToBePaid() {
		return amountToBePaid;
	}

	public Boolean getIsInline() {
		return isInline;
	}

	public Boolean getIsTampered() {
		return isTampered;
	}

	public Boolean getIsComplete() {
		return isComplete;
	}

	public Boolean getIsFlagged() {
		return isFlagged;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public Date getLogInsertTime() {
		return logInsertTime;
	}

	public Date getReturnTime() {
		return returnTime;
	}

	public Date getTransactionTime() {
		return transactionTime;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public void setIPAddress(String iPAddress) {
		IPAddress = iPAddress;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}

	public void setPaymentIssuer(String paymentIssuer) {
		this.paymentIssuer = paymentIssuer;
	}

	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public void setGatewayPaymentId(String gatewayPaymentId) {
		this.gatewayPaymentId = gatewayPaymentId;
	}

	public void setCompletedVia(String completedVia) {
		this.completedVia = completedVia;
	}

	public void setCardBankName(String cardBankName) {
		this.cardBankName = cardBankName;
	}

	public void setBinNumber(Long binNumber) {
		this.binNumber = binNumber;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public void setAmountToBePaid(Double amountToBePaid) {
		this.amountToBePaid = amountToBePaid;
	}

	public void setIsInline(Boolean isInline) {
		this.isInline = isInline;
	}

	public void setIsTampered(Boolean isTampered) {
		this.isTampered = isTampered;
	}

	public void setIsComplete(Boolean isComplete) {
		this.isComplete = isComplete;
	}

	public void setIsFlagged(Boolean isFlagged) {
		this.isFlagged = isFlagged;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public void setLogInsertTime(Date logInsertTime) {
		this.logInsertTime = logInsertTime;
	}

	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}

	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}

	
	@Override
	public String toString() {
		return "OrderPaymentLogEntry [logId=" + logId + ", login=" + login + ", orderId=" + orderId + ", userAgent="
				+ userAgent + ", IPAddress=" + IPAddress + ", gateway=" + gateway + ", paymentOption=" + paymentOption
				+ ", paymentIssuer=" + paymentIssuer + ", bankTransactionId=" + bankTransactionId
				+ ", gatewayPaymentId=" + gatewayPaymentId + ", completedVia=" + completedVia + ", cardBankName="
				+ cardBankName + ", binNumber=" + binNumber + ", amount=" + amount + ", amountPaid=" + amountPaid
				+ ", amountToBePaid=" + amountToBePaid + ", isInline=" + isInline + ", isTampered=" + isTampered
				+ ", isComplete=" + isComplete + ", isFlagged=" + isFlagged + ", responseCode=" + responseCode
				+ ", responseMessage=" + responseMessage + ", logInsertTime=" + logInsertTime + ", returnTime="
				+ returnTime + ", transactionTime=" + transactionTime + "]";
	}
}