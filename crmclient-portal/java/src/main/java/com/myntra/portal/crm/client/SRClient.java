package com.myntra.portal.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.portal.crm.client.constant.CRMPortalClientConstants;
import com.myntra.portal.crm.client.response.SRResponse;

/**
 * Web client for SR web service
 * 
 * @author Arun Kumar
 */
public class SRClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/SR/";

	public static SRResponse getSRByOrder(String serviceURL, Long orderId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "order/{orderId}", orderId);

		return client.get(SRResponse.class);
	}

	public static SRResponse getSRById(String serviceURL, Long SRId) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "{Id}", SRId);

		return client.get(SRResponse.class);
	}
	
	public static SRResponse getSRByLoginOrMobile(String serviceURL, String loginOrMobile) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMPortalClientConstants.CRM_PORTAL_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "customer/{loginOrMobile}", loginOrMobile);

		return client.get(SRResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		SRResponse test = getSRByOrder("http://localhost:7092/crmservice-portal", 6994448L);
		System.out.println(test);

		SRResponse test1 = getSRById("http://localhost:7092/crmservice-portal", 271494L);
		System.out.println(test1);
		
		SRResponse test2 = getSRByLoginOrMobile("http://localhost:7092/crmservice-portal", "arun.kumar@myntra.com");
		System.out.println(test2);
		
		SRResponse test3 = getSRByLoginOrMobile("http://localhost:7092/crmservice-portal", "9739099837");
		System.out.println(test3);
	}

}