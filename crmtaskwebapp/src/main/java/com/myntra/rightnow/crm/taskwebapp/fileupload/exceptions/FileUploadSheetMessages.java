/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author preetam
 */
public class FileUploadSheetMessages implements Serializable {
    
    private String sheetName;
    private List<FileUploadExceptionMessage> errorMessages;

    public FileUploadSheetMessages() {
    }

    public FileUploadSheetMessages(String sheetName, List<FileUploadExceptionMessage> errorMessages) {
        this.sheetName = sheetName;
        this.errorMessages = errorMessages;
    }
    
    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public List<FileUploadExceptionMessage> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<FileUploadExceptionMessage> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
