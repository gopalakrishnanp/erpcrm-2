/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.rightnow.crm.taskwebapp.controller;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.client.RightnowTaskClient;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskListEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskOrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskUDCallingEntry;
import com.myntra.rightnow.crm.client.response.RightnowBaseTaskResponse;
import com.myntra.rightnow.crm.taskwebapp.fileupload.BaseFileUploader;
import com.myntra.rightnow.crm.taskwebapp.fileupload.exceptions.FileUploadSheetMessages;
import com.myntra.rightnow.crm.taskwebapp.utils.FileUploadUtils;
import com.myntra.rightnow.crm.taskwebapp.utils.TaskwebappUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author preetam
 */
@ManagedBean(name = "fileUpload")
@ViewScoped
public class FileUploadController extends BaseFileUploader implements Serializable {
    
    private static final String MAIN_CATEGORY_COLUMN_NAME = "Main Category";
    private static final String SUB_CATEGORY_COLUMN_NAME = "Sub Category";
    private static final String SUB_SUB_CATEGORY_COLUMN_NAME = "Sub Sub Category";
    private static final String ORDER_ID_COLUMN_NAME = "Order Id";
    private static final String COMMENTS_COLUMN_NAME = "Comments";
    private static final String UD_REASON_COLUMN_NAME = "UD Reason";
    private static final String UD_QUEUED_DATE_COLUMN_NAME = "UD Queued Date";
    
    
    private int mainCategoryColumnIndex = -1;
    private int subCategoryColumnIndex = -1;
    private int subSubCategoryColumnIndex = -1;
    private int orderIdColumnIndex = -1;
    private int commentsColumnIndex = -1;
    private int udReasonColumnIndex = -1;
    private int udQuedDateColumnIndex = -1;
        
    @Override
    public void processEachExcelSheet(MyntraSheetReader sheet) {
        RightnowBaseTaskListEntry taskListEntry = new RightnowBaseTaskListEntry();
        List<RightnowBaseTaskEntry> taskEntries = new ArrayList<RightnowBaseTaskEntry>();
        //in ubuntu, it takes all the rows (65535) of the excel sheet even if rows are blank.
        //so we will have to see if consecutively 3 rows do not have task id (first column) treat them as blank and halt the processing of any further row
        try {
            //validate columns
            if(!validateColumnHeaders(sheet))
                return;

            //we are here, it means the excel columns are perfectly alright and 
            //we should proceed to reading of the actual data as eneterd by user
            int emptyRowsCount = 0; //initialize to zero before starting to read the rows
            while (sheet.hasNextRow()) {
                sheet.nextRow();
                //check for empty row, done by checking whether the Main Category value exists or not
                if (FileUploadUtils.isEmptyRow(sheet, mainCategoryColumnIndex)) {
                    emptyRowsCount++;
                } else {
                    //create a task entry object for the current row
                    RightnowTaskOrderEntry taskEntry = createTaskEntryForCurrentRow(sheet);
                    if (taskEntry != null) {
                        taskEntries.add(taskEntry);
                    }
                    emptyRowsCount = 0;
                }

                if (emptyRowsCount == MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING) {
                    break;
                }
            }

            if (taskEntries.isEmpty()) {
                addErrorMessage("There are no valid entries for any of the rows.");
            }

            if (errorMessagesList.size() > 0) {
                return;
            }

            //create the payload RightnowTaskListEntry
            taskListEntry.setTaskEntryList(taskEntries);
            //save the task object
            RightnowBaseTaskResponse check = RightnowTaskClient.createProactiveTasks(null, taskEntries);
            
            if (TaskwebappUtils.isSuccessResponse(check)) {
                if(check.getStatus().getStatusCode() == RightnowErrorCodes.TASK_CREATION_ABORTED.getStatusCode() && 
                        check.getRightnowBaseTaskEntryList().size() > 0) {
                    String message = "Upload failed as no contacts exist in Rightnow for the following Order IDs:<br/>";
                    for(RightnowBaseTaskEntry taskEntry : check.getRightnowBaseTaskEntryList()) {
                        RightnowTaskOrderEntry orderEntry = (RightnowTaskOrderEntry) taskEntry;
                        message += orderEntry.getOrderId() + "\n";
                    }
                    addErrorMessage(message);
                } else if(check.getStatus().getStatusCode() == RightnowErrorCodes.NO_TASK_SAVED_IN_RIGHTNOW.getStatusCode()){
                    addErrorMessage("Error while creating tasks! Please contact administrator.");
                }
            } 

        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ERPServiceException ex) {
            ex.printStackTrace();
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            addErrorMessage("Error while creating tasks");
        }        
    }
    
    private RightnowTaskOrderEntry createTaskEntryForCurrentRow(MyntraSheetReader sheet) {

        String mainCategory = null;
        String subCategory = null;
        String subSubCategory = null;
        long orderId = 0;
        String comments = null;
        String udReason = null;
        Date udQueuedDate = null;
        StringBuilder error = new StringBuilder("Error at Row Number: " + sheet.currentRowNum() + ". Incorrect/Empty Fields. ");
        //read the Main Category, Sub Category, Sub Sub Category  & order Id      
        try {
            sheet.setColumn(mainCategoryColumnIndex);
            mainCategory = sheet.readCellValue().toString();
            sheet.setColumn(subCategoryColumnIndex);
            subCategory = sheet.readCellValue().toString();
            sheet.setColumn(subSubCategoryColumnIndex);
            subSubCategory = sheet.readCellValue().toString();
            sheet.setColumn(orderIdColumnIndex);
            Object orderObj = sheet.readCellValue();
            if(orderObj instanceof Double) {
                orderId = ((Double) orderObj).longValue();
            } else if(orderObj instanceof Long) {
                orderId = ((Long) orderObj).longValue();
            } else {
                orderId = Long.parseLong((String) orderObj);
            }
            if(commentsColumnIndex > -1) {
                sheet.setColumn(commentsColumnIndex);
                comments = sheet.readCellValue().toString();
            }
            if(udReasonColumnIndex > -1) {
                sheet.setColumn(udReasonColumnIndex);
                udReason = sheet.readStringCellValue().toString();
            }
            if(udQuedDateColumnIndex > -1) {
                sheet.setColumn(udQuedDateColumnIndex);
                Object val = sheet.readCellValue();
                if(val instanceof Date) {
                    udQueuedDate = (Date) val;
                }
            }
        } catch (ExcelPOIWrapperException e) {
            error.append(e.getMessage());
        } catch (NumberFormatException e) {
            error.append(e.getMessage());
        }
        if (mainCategory == null || mainCategory.trim().isEmpty() || subCategory == null || subCategory.trim().isEmpty() ||
                subSubCategory == null || subSubCategory.trim().isEmpty() || orderId == 0 || udReason == null ||
                udReason.trim().isEmpty() || udQueuedDate == null) {
            addErrorMessage(error.toString());
            return null;

        } else {           
            RightnowTaskUDCallingEntry taskEntry = new RightnowTaskUDCallingEntry();
            taskEntry.setOrderId(orderId);
            taskEntry.setMainCategory(mainCategory);
            taskEntry.setSubCategory(subCategory);
            taskEntry.setSubSubCategory(subSubCategory);
            taskEntry.setUdReason(udReason);
            taskEntry.setUdQueuedDate(udQueuedDate);
            if(comments != null && !comments.trim().isEmpty()) {
                taskEntry.setComments(comments);
            }            
            return taskEntry;
        }        
    }

    @Override
    public boolean validateColumnHeaders(MyntraSheetReader sheet) {
        int columnIndex = 0;
        try {
            
            if (sheet.hasNextRow()) {
                sheet.nextRow();
            } else {
                addErrorMessage("The sheet doesn't have a colum header as first row.");
                return false;
            }
            
            while (sheet.hasCell()) {
                String columnName = sheet.readStringCellValue();
                if(MAIN_CATEGORY_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    mainCategoryColumnIndex = columnIndex;
                } else if (SUB_CATEGORY_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    subCategoryColumnIndex = columnIndex;
                } else if (SUB_SUB_CATEGORY_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    subSubCategoryColumnIndex = columnIndex;
                } else if(ORDER_ID_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    orderIdColumnIndex = columnIndex;
                } else if(COMMENTS_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    commentsColumnIndex = columnIndex;
                } else if(UD_REASON_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    udReasonColumnIndex = columnIndex;
                } else if(UD_QUEUED_DATE_COLUMN_NAME.equalsIgnoreCase(columnName)) {
                    udQuedDateColumnIndex = columnIndex;
                }
                columnIndex++;
            }

            //if any of the required columns are missing, show an error message to the user
            if (mainCategoryColumnIndex > -1 && subCategoryColumnIndex > -1 && subSubCategoryColumnIndex > -1 && orderIdColumnIndex > -1 
                    && udReasonColumnIndex > -1 && udQuedDateColumnIndex > -1) {
                return true;
            } else {
                if(mainCategoryColumnIndex == -1) {
                    addErrorMessage("'Main Category' column is mandatory");
                }
                if(subCategoryColumnIndex == -1) {
                    addErrorMessage("'Sub Category' column is mandatory");
                }
                if(subSubCategoryColumnIndex == -1) {
                    addErrorMessage("'Sub Sub Category' column is mandatory");
                }
                if(orderIdColumnIndex == -1) {
                    addErrorMessage("'Order Id' column is mandatory");
                }
                if(udReasonColumnIndex == -1) {
                    addErrorMessage("'UD Reason' column is mandatory");
                }
                if(udQuedDateColumnIndex == -1) {
                    addErrorMessage("'UD Queued Date' column is mandatory");
                }
                return false;
            }
        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public void validateData(MyntraSheetReader sheet) {
        
    }
    
    @Override
    public void showUploadStatusMessages(List<FileUploadSheetMessages> errorMessages, String fileName) {
        FileUploadUtils.showUploadStatusMessages(errorMessages, "taskCreationMessage", fileName);
    }
}
