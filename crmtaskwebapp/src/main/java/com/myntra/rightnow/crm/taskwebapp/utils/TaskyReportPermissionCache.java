package com.myntra.rightnow.crm.taskwebapp.utils;

import com.myntra.client.security.response.UserEntry;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.lms.client.DeliveryCenterServiceClient;
import com.myntra.lms.client.response.DeliveryCenterEntry;
import com.myntra.lms.client.response.DeliveryCenterResponse;
import com.myntra.primecore.utils.UserPagePermissionCache;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;

/**
 * This class is used to hold the user page permission for the logged in user on
 * the client side.
 *
 */
@ManagedBean(name = "taskyReportPermissionCache")
@SessionScoped
public class TaskyReportPermissionCache implements Serializable {

    private static ContextInfo context;
    private static UserEntry userEntry;
    private static final Logger logger = Logger.getLogger(TaskyReportPermissionCache.class.getName());
    private MenuModel deliveryMLmodel;
    
    @ManagedProperty(value = "#{userPagePermissionCache}")
    private UserPagePermissionCache userPagePermissionCache;

    public UserPagePermissionCache getUserPagePermissionCache() {
        return userPagePermissionCache;
    }

    public void setUserPagePermissionCache(UserPagePermissionCache userPagePermissionCache) {
        this.userPagePermissionCache = userPagePermissionCache;
    }

    public MenuModel getDeliveryMLmodel() {
        return deliveryMLmodel;
    }
    
    public UserEntry getUserEntry() {
        return userEntry;
    }

    @PostConstruct
    private void init() {
        logger.log(Level.INFO, "Initializing Tasky permission cache");
        context = (ContextInfo) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("contextInfo");
        userEntry = (UserEntry) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userProfile");
        
        DefaultSubMenu firstSubMenu = new DefaultSubMenu("Delivery - ML");

        deliveryMLmodel = new DefaultMenuModel();
        deliveryMLmodel.addElement(firstSubMenu);
        
        if (userPagePermissionCache.getPermission("tasky.All") != null) {
            DefaultMenuItem itemAll = new DefaultMenuItem("ML All (India)");
            itemAll.setIcon("ui-icon-document");
            itemAll.setGlobal(true);
            itemAll.setCommand("#{taskwebapp.getReportData('ML All')}");
            itemAll.setUpdate(":taskForm,:taskButtonForm,:taskDetailForm");
            itemAll.setOncomplete("taskyLayout.layout.close('west');taskyLayout.layout.close('east');");

            firstSubMenu.addElement(itemAll);
            return;
        }
        
        if (userPagePermissionCache.getPermission("tasky.DeliveryMLReports")==null ||
                userEntry.getDeliveryCenter()==null || 
                userEntry.getDeliveryCenter().trim().equals("")) {
            // Reset delivery model.    
            deliveryMLmodel = null;
            return;
        }
        
        if(userEntry == null || userEntry.getDeliveryCenter()==null)
            return; 
        
        String deliveryCenterCSV = userEntry.getDeliveryCenter();

        String[] deliveryIDs = deliveryCenterCSV.split(",");
        List<Long> list = new ArrayList<Long>();

        for (String id : deliveryIDs) {
            try {
                if (id != null) {
                    Integer deliveryID = Integer.parseInt(id.trim());
                    Long deliveryIDlong = deliveryID.longValue();
                    if (!list.contains(deliveryIDlong)) {
                        list.add(deliveryIDlong);
                    }
                }
            } catch (Exception e) {
            }
        }

        if (!list.isEmpty()) {
            DefaultMenuItem itemAssigned = new DefaultMenuItem("ML All (Assigned)");
            itemAssigned.setIcon("ui-icon-document");
            itemAssigned.setGlobal(true);
            itemAssigned.setUpdate(":taskForm,:taskButtonForm,:taskDetailForm");
            itemAssigned.setOncomplete("taskyLayout.layout.close('west');taskyLayout.layout.close('east');");
            
            firstSubMenu.addElement(itemAssigned);
            
            TreeMap tmState = new TreeMap();
            
            HashMap<String, String> codeFilters = new HashMap<String, String>();
            
            for (Long code : list) {
                try {
                    DeliveryCenterResponse dcResponse =
                            DeliveryCenterServiceClient.findById(null, code, context);

                    if (TaskwebappUtils.isSuccessResponse(dcResponse)
                            && dcResponse.getStatus().getTotalCount() > 0) {
            
                        DeliveryCenterEntry deliveryCentre = dcResponse.getDeliveryCenters().get(0);

                        TreeMap tmStateDCcodes = (TreeMap) tmState.get(deliveryCentre.getState());
                        if(tmStateDCcodes==null) {
                            tmStateDCcodes = new TreeMap<String, DefaultMenuItem>();
                            tmState.put(deliveryCentre.getState(), tmStateDCcodes);
                        }
                        
                        String deliveryCenterFilters = "'" + deliveryCentre.getCityCode() + "/" + deliveryCentre.getCode() + "'";
                        deliveryCenterFilters += ",'DC-" + deliveryCentre.getName() + "'";

                        if (codeFilters.get("ML Assigned") == null) {
                            codeFilters.put("ML Assigned", deliveryCenterFilters);
                        } else {
                            codeFilters.put("ML Assigned", codeFilters.get("ML Assigned") + "," + deliveryCenterFilters);
                        }
                        
                        
                        if (codeFilters.get(" All. " + deliveryCentre.getState()) == null) {
                            codeFilters.put(" All. " + deliveryCentre.getState(), deliveryCenterFilters);
                        } else {
                            codeFilters.put(" All. " + deliveryCentre.getState(), 
                                    codeFilters.get(" All. " + deliveryCentre.getState()) + "," + deliveryCenterFilters);
                        }
                        
                        String dcCityCentreCode = deliveryCentre.getCityCode() + " / " + deliveryCentre.getCode();
                        
                        DefaultMenuItem item = new DefaultMenuItem(dcCityCentreCode);
                        item.setIcon("ui-icon-document");
                        item.setGlobal(true);
                        item.setCommand("#{taskwebapp.getReportDataML(\"ML All\" , \"" + deliveryCenterFilters + "\")}");
                        item.setUpdate(":taskForm,:taskButtonForm,:taskDetailForm");
                        item.setOncomplete("taskyLayout.layout.close('west');taskyLayout.layout.close('east');");
                        
                        tmStateDCcodes.put(dcCityCentreCode, item);
                    }
                } catch (ERPServiceException ex) {
                }
            }
            
            Set set = tmState.entrySet();
            Iterator i = set.iterator();
            
            while(i.hasNext()) {
               Map.Entry me = (Map.Entry)i.next();
               DefaultSubMenu stateMenu = new DefaultSubMenu((String) me.getKey());
               String label = (String) me.getKey();
               TreeMap values = (TreeMap) me.getValue();

               if(values.size()>1) {
                    DefaultMenuItem item = new DefaultMenuItem("All. " + label);
                    item.setIcon("ui-icon-document");
                    item.setGlobal(true);
                    item.setCommand("#{taskwebapp.getReportDataML(\"ML All\" , \"" + codeFilters.get(" All. " + label) + "\")}");
                    item.setUpdate(":taskForm,:taskButtonForm,:taskDetailForm");
                    item.setOncomplete("taskyLayout.layout.close('west');taskyLayout.layout.close('east');");                   
                    stateMenu.addElement(item);
                }
               
               Set set2 = values.entrySet();
               Iterator i2 = set2.iterator();
               
               while(i2.hasNext()) {
                   Map.Entry dc = (Map.Entry)i2.next();
                   stateMenu.addElement((MenuElement) dc.getValue());
               }
               
               label = label + " (" + values.size() + ")";
               stateMenu.setLabel(label);
               
               firstSubMenu.addElement(stateMenu);
            }
            
            itemAssigned.setCommand("#{taskwebapp.getReportDataML(\"ML All\" , \"" + codeFilters.get("ML Assigned") + "\")}");
        }
    }

    public Integer getPermission(String pageName) {
        if (pageName == null || userPagePermissionCache == null) {
            return null;
        }

        Integer allReports = userPagePermissionCache.getPermission("tasky.All");

        if (allReports != null) {
            return allReports;
        }

        return userPagePermissionCache.getPermission(pageName);
    }
}
