package com.myntra.rightnow.crm.taskwebapp.controller;

import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraExcelReader;
import com.myntra.commons.excel.poiImpl.MyntraExcelWriter;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.commons.excel.poiImpl.MyntraSheetWriter;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.rightnow.crm.client.RightnowReportClient;
import com.myntra.rightnow.crm.client.RightnowTaskClient;
import com.myntra.rightnow.crm.client.entry.RightnowFileAttachmentEntry;
import com.myntra.rightnow.crm.client.entry.RightnowNoteEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportColumnEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportRowEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskListEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;
import com.myntra.rightnow.crm.client.response.RightnowTaskResponse;
import com.myntra.rightnow.crm.taskwebapp.model.RightnowReportModel;
import com.myntra.rightnow.crm.taskwebapp.utils.FileUploadUtils;
import com.myntra.rightnow.crm.taskwebapp.utils.TaskwebappUtils;
import com.myntra.rightnow.crm.taskwebapp.utils.TaskyReportPermissionCache;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.util.StringUtils;

/**
 *
 * @author pravin
 */ 
@ManagedBean(name = "taskwebapp")
@ViewScoped
public class TaskwebappController implements Serializable {

    private static final long serialVersionUID = 1L;
    private RightnowReportModel rightnowReportModel;
    private String reportName;
    private String reportFilter;
    
    private String message;
    private String newNote;
    private StreamedContent excelReport;
    private int showExcelUploader = 0;
    private static final String TASK_ID_COLUMN_NAME = "Task ID";
    private static final int MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING = 3;
    private static final int MAX_NO_ROWS_ALLOWED_IN_EXCEL = 700;
    private Long selectedTaskID;
    private RightnowReportRowEntry[] selectedTasks;
    private List<RightnowFileAttachmentEntry> fileList;
    private String loggedInUserName;
    private static final String ADDED_BY_TEXT = "<br/> ---- Added By ";
    
    @ManagedProperty("#{taskyReportPermissionCache}")
    private TaskyReportPermissionCache  taskyReportPermissionCache;

    public TaskwebappController() {
    }
    
    public int getMaxTasksAllowedToUpdate() {
        return MAX_NO_ROWS_ALLOWED_IN_EXCEL;
    }

    public RightnowReportModel getRightnowReportModel() {
        return rightnowReportModel;
    }

    public void setRightnowReportModel(RightnowReportModel rightnowReportModel) {
        this.rightnowReportModel = rightnowReportModel;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportFilter() {
        return reportFilter;
    }

    public void setReportFilter(String reportFilter) {
        this.reportFilter = reportFilter;
    }
    
    public String getDisplayName() {
        String displayName = reportName;
        if(reportFilter!=null)
            displayName = displayName + " : " + reportFilter;
        return displayName;
    }
    
    public String getNewNote() {
        return newNote;
    }

    public void setNewNote(String newNote) {
        this.newNote = newNote;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String errorMessage) {
        this.message = errorMessage;
    }

    public int getShowExcelUploader() {
        return showExcelUploader;
    }

    public void setShowExcelUploader(int showExcelUploader) {
        this.showExcelUploader = showExcelUploader;
    }

    public List<RightnowFileAttachmentEntry> getFileList() {
        return fileList;
    }

    public void setFileList(List<RightnowFileAttachmentEntry> fileList) {
        this.fileList = fileList;
    }

    public Long getSelectedTaskID() {
        return selectedTaskID;
    }

    public void setSelectedTaskID(Long selectedTaskID) {
        this.selectedTaskID = selectedTaskID;
    }

    public TaskyReportPermissionCache getTaskyReportPermissionCache() {
        return taskyReportPermissionCache;
    }

    public void setTaskyReportPermissionCache(TaskyReportPermissionCache taskyReportPermissionCache) {
        this.taskyReportPermissionCache = taskyReportPermissionCache;
    }

    public String getLoggedInUserName() {
        if(this.taskyReportPermissionCache == null) {
            FacesContext context = FacesContext.getCurrentInstance();    
            Application app = context.getApplication();    
            this.taskyReportPermissionCache = (TaskyReportPermissionCache) app.evaluateExpressionGet(context, "#{taskyReportPermissionCache}", TaskyReportPermissionCache.class);
        }
        return this.taskyReportPermissionCache.getUserEntry().getLoginId();
    }

    public void setLoggedInUserName(String loggedInUserName) {
        this.loggedInUserName = loggedInUserName;
    }
    
//    @PostConstruct
//    public void init() {
//        loggedInUserName = taskyReportPermissionCache.getUserEntry().getName();
//    }
    
    public void getReportDataML(String reportName, String dcFilterString) {
        setShowExcelUploader(0);
        setSelectedTaskID(null);
        
        setReportName("ML Report");
        setReportFilter(dcFilterString);
        
        rightnowReportModel = new RightnowReportModel();

        try {
            
            List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();

            RightnowReportFilterEntry  filter = new RightnowReportFilterEntry();
            filter.setProperty("DC Name");
            filter.setValues(new String[]{dcFilterString});
            filter.setDataType(5);
            filter.setOperator(10);
            
            filters.add(filter);

            RightnowReportResponse response = 
                    RightnowReportClient.getReportData(null, reportName, filters);

            if (TaskwebappUtils.isSuccessResponse(response)) {
                rightnowReportModel.setRightnowReportColumnEntryList(response.getRightnowReportColumnEntryList());

                if (response.getRightnowReportRowEntryList().size() > 0) {
                    rightnowReportModel.setRightnowReportRowEntryList(response.getRightnowReportRowEntryList());
                } else {
                    setMessage("No Tasks in this ML Report");
                }
            } else {
                setMessage(response.getStatus().getStatusMessage());
            }
        } catch (ERPServiceException ex) {
            setMessage("ERPServiceExpection : " + ex.getMessage());
        } catch (Exception ex) {
            setMessage("Exception : " + ex.getMessage());
        }
    }


    public void getReportData(String reportName) {
        setShowExcelUploader(0);
        setSelectedTaskID(null);
        if (reportName == null || reportName.trim().equals("")) {
            return;
        }

        setReportName(reportName);
        setReportFilter(null);

        rightnowReportModel = new RightnowReportModel();

        try {
            String rnReportName = TaskwebappUtils.getReportName(reportName);
            List<RightnowReportFilterEntry> filters = TaskwebappUtils.getReportFilters(reportName);
            RightnowReportResponse response;
            if(filters == null || filters.isEmpty()) {
                response = RightnowReportClient.getReportData(null, rnReportName);
            } else {
                response = RightnowReportClient.getReportData(null, rnReportName, filters);
            }

            if (TaskwebappUtils.isSuccessResponse(response)) {
                rightnowReportModel.setRightnowReportColumnEntryList(response.getRightnowReportColumnEntryList());

                if (response.getRightnowReportRowEntryList().size() > 0) {
                    rightnowReportModel.setRightnowReportRowEntryList(response.getRightnowReportRowEntryList());
                } else {
                    setMessage("No Tasks in this Report");
                }
            } else {
                setMessage(response.getStatus().getStatusMessage());
            }
        } catch (ERPServiceException ex) {
            setMessage("ERPServiceExpection : " + ex.getMessage());
        } catch (Exception ex) {
            setMessage("Exception : " + ex.getMessage());
        }
    }
    
    
    
    public void getMissingReportData(String reportName) {
        
        setShowExcelUploader(0);
        setSelectedTaskID(null);
        if (reportName == null || reportName.trim().equals("")) {
            return;
        }

        setReportName(reportName);
        setReportFilter(null);

        rightnowReportModel = new RightnowReportModel();
        
        List<String> allQMTReports= new ArrayList<String>();
        allQMTReports.add("3PL All");
        allQMTReports.add("Catalogue Tasks");
        allQMTReports.add("Feedback");
        allQMTReports.add("Finance Tasks");
        allQMTReports.add("Finance NEFT");
        allQMTReports.add("ML All");
        allQMTReports.add("3PL All");
        allQMTReports.add("3PL invalid tracking number and ODA");
        allQMTReports.add("Missing Component");
        allQMTReports.add("Pick Up");
        allQMTReports.add("RTO");
        allQMTReports.add("Return");
        allQMTReports.add("WIP_Packed More than 24 hrs");
        allQMTReports.add("Engg All");
        
        setReportFilter("Remove Tasks in -- " + 
            StringUtils.arrayToCommaDelimitedString(allQMTReports.toArray()));
        
        try {
            RightnowReportResponse response = RightnowReportClient.getReportData(null, reportName);

            if (TaskwebappUtils.isSuccessResponse(response)) {
                rightnowReportModel.setRightnowReportColumnEntryList(response.getRightnowReportColumnEntryList());
                List<RightnowReportRowEntry> allOpenTasks = response.getRightnowReportRowEntryList();
                
                HashSet<Long> set = new HashSet<Long>();
                
                for(String taskyReports : allQMTReports) {
                    RightnowReportResponse response2 = RightnowReportClient.getReportData(null, taskyReports);
                    if (TaskwebappUtils.isSuccessResponse(response2)) {
                        List<RightnowReportRowEntry> someReportTasks = response2.getRightnowReportRowEntryList();
                        for (RightnowReportRowEntry entry : someReportTasks) {
                            set.add(entry.getId());
                        }
                    }
                }

                for(Iterator<RightnowReportRowEntry> iter = allOpenTasks.iterator(); iter.hasNext();) {
                    RightnowReportRowEntry entry = iter.next();
                    if(set.contains(entry.getId()))
                        iter.remove();
                }

                if (allOpenTasks.size() > 0) {
                    rightnowReportModel.setRightnowReportRowEntryList(allOpenTasks);
                } else {
                    setMessage("No Missing Tasks Identified");
                }
            } else {
                setMessage(response.getStatus().getStatusMessage());
            }
        } catch (ERPServiceException ex) {
            setMessage("ERPServiceExpection : " + ex.getMessage());
        } catch (Exception ex) {
            setMessage("Exception : " + ex.getMessage());
        }
    }
    

    public RightnowReportRowEntry[] getSelectedTasks() {
        return selectedTasks;
    }

    public void setSelectedTasks(RightnowReportRowEntry[] selectedTasks) {
        this.selectedTasks = selectedTasks;
    }

    public void saveTaskStatus() {
        try {
            //proceed only if one or more tasks are selected
            if (selectedTasks.length == 0) {
                FacesMessage noTasksSelectedError = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please select one or more tasks");
                FacesContext.getCurrentInstance().addMessage("taskStatusMessage", noTasksSelectedError);
                return;
            }
            RightnowTaskListEntry taskEntriesList = new RightnowTaskListEntry();
            List<RightnowTaskEntry> taskEntries = new ArrayList<RightnowTaskEntry>();

            int statusColumnIndex = rightnowReportModel.getStatusColumnIndex();

            for (RightnowReportRowEntry row : selectedTasks) {
                RightnowTaskEntry myntraTask = new RightnowTaskEntry();
                myntraTask.setId(row.getId());

                String status = row.getRightnowReportDataList().get(statusColumnIndex);
                myntraTask.setSrStatus(row.getRightnowReportDataList().get(statusColumnIndex));

                if (status != null && !status.trim().equals("") && !status.equalsIgnoreCase("Open")) {
                    taskEntries.add(myntraTask);
                }
            }

            if (taskEntries.isEmpty()) {
                FacesMessage allOpenTasks = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "All Tasks in Open Status. Nothing to Save");
                FacesContext.getCurrentInstance().addMessage("taskStatusMessage", allOpenTasks);
                return;
            }

            taskEntriesList.setTaskEntryList(taskEntries);

            RightnowTaskResponse check = RightnowTaskClient.updateTasks(null, taskEntriesList);

            if (TaskwebappUtils.isSuccessResponse(check)) {
                return;
            }

        } catch (ERPServiceException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveTaskNotes() {
        
        if (newNote == null || newNote.trim().equals("")) {
            FacesContext.getCurrentInstance().addMessage("taskStatusMessage",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Note cannot be blank"));
            return;
        }

        if (selectedTasks.length == 0) {
            FacesContext.getCurrentInstance().addMessage("taskStatusMessage",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please select one or more tasks"));
            return;
        }

        try {
            //append new note with added by info
            String note = newNote + ADDED_BY_TEXT + getLoggedInUserName();
            RightnowTaskListEntry taskEntriesList = new RightnowTaskListEntry();
            List<RightnowTaskEntry> taskEntries = new ArrayList<RightnowTaskEntry>();

            for (RightnowReportRowEntry row : selectedTasks) {
                RightnowTaskEntry myntraTask = new RightnowTaskEntry();
                myntraTask.setId(row.getId());
                myntraTask.setNote(note);
                taskEntries.add(myntraTask);
            }

            taskEntriesList.setTaskEntryList(taskEntries);

            RightnowTaskResponse check = RightnowTaskClient.updateTasks(null, taskEntriesList);

            if (TaskwebappUtils.isSuccessResponse(check)) {
                rightnowReportModel.clearNotes(taskEntries);
            }

        } catch (ERPServiceException ex) {
            ex.printStackTrace();
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowToggle(ToggleEvent event) {
        RightnowReportRowEntry rowEntry = (RightnowReportRowEntry) event.getData();
        rightnowReportModel.getTaskDetailsFromId(rowEntry.getId());
    }

    public void onRowSelect(SelectEvent event) {
        RightnowReportRowEntry rowEntry = (RightnowReportRowEntry) event.getObject();
        selectedTaskID = rowEntry.getId();
        rightnowReportModel.getTaskDetailsFromId(selectedTaskID);
    }

    public void retrieveTaskDetails(RightnowReportRowEntry rowEntry) {
        selectedTaskID = rowEntry.getId();
        rightnowReportModel.getTaskDetailsFromId(selectedTaskID);
    }

    public List<RightnowFileAttachmentEntry> getAttachments() {
        if (selectedTaskID == null || selectedTaskID == 0) {
            return null;
        }
        return rightnowReportModel.getTaskAttachments(selectedTaskID);
    }

    public List<RightnowNoteEntry> getNotes() {
        if (selectedTaskID == null || selectedTaskID == 0) {
            return null;
        }
        return rightnowReportModel.getTaskNotes(selectedTaskID);
    }

    public StreamedContent getExcelReport() {
        excelReport = null;
        HashMap<Long, String> taskNoteMap = null;
        Map<Integer, Boolean> numericColumns = new HashMap<Integer, Boolean>();

        try {
            DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            File tempFile = File.createTempFile(reportName, ".xlsx");
            FileOutputStream foStream = new FileOutputStream(tempFile);
            MyntraExcelWriter excelWriter = new MyntraExcelWriter(foStream);

            MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter(reportName);

            //protect the sheet to make the cell non editable
            //sheetWriter.protectSheet("myntra");

            //create the column headers in the sheet
            int columnIndex = 0;
            for (RightnowReportColumnEntry columnEntry : rightnowReportModel.getRightnowReportColumnEntryList()) {
                if(columnEntry.getColumnName().toLowerCase().endsWith(" id") || 
                        columnEntry.getColumnName().toLowerCase().endsWith(" in days")) {
                    numericColumns.put(columnIndex, true);
                }
                sheetWriter.writeHeader(columnEntry.getColumnName(), 15, false);
                columnIndex++;
            }

            //if task ID is there, create a column for 'Notes' (non editable)
            //and two columns for 'Add Note' & 'Update Status' each

            //for notes
            sheetWriter.writeHeader("Notes", 30, false);

            //for Add Note & Update Status, make the column highlighted to indicate tehse are editable
            sheetWriter.writeHeader("Add Note", 30, true);
            sheetWriter.writeHeader("Update Status", 15, true);

            List<String> statusList = new ArrayList<String>();
            statusList.add("Resolved");
            statusList.add("Additional Information Required");

            // Fetch All Task Notes now.
            taskNoteMap = getTaskNotes();

            for (RightnowReportRowEntry rowEntry : rightnowReportModel.getRightnowReportRowEntryList()) {
                sheetWriter.nextRow();
                columnIndex = 0;
                for (String values : rowEntry.getRightnowReportDataList()) {
                    //sheetWriter.protectCell();
                    if(numericColumns.get(columnIndex) != null) {
                        try {
                            //write as numeric if the column name ends with 'ID'
                            //if it is not a valid number (in case of NumberFormatException), write it as text
                            sheetWriter.writeNumber(Double.parseDouble(values));
                        } catch (NumberFormatException e) {
                            sheetWriter.writeText(values);
                        }
                    } else {
                        sheetWriter.writeText(values);
                    }
                    columnIndex++;
                }

                //sheetWriter.protectCell();
                if (taskNoteMap != null && taskNoteMap.get(rowEntry.getId()) != null) {
                    sheetWriter.writeText(taskNoteMap.get(rowEntry.getId()).replaceAll("<br/>", "\n"));
                } else {
                    sheetWriter.writeText("");
                }

                //sheetWriter.unprotectCell();
                sheetWriter.writeText("");
                //sheetWriter.unprotectCell();
                sheetWriter.writeDropDown(statusList, null);
            }

            excelWriter.save();

            FileInputStream filepath = new FileInputStream(tempFile);
            excelReport = new DefaultStreamedContent(filepath, "application/vnd.ms-excel.12", reportName + "." + sdf.format(new Date()) + ".xlsx");
            //filepath.close();
            //foStream.close();
            //tempFile.delete();
        } catch (Exception ex) {
            //ex.printStackTrace();
        }

        return excelReport;
    }

    private HashMap<Long, String> getTaskNotes() {
        if(rightnowReportModel.taskNotes == null || rightnowReportModel.taskNotes.isEmpty()) {
            rightnowReportModel.retrieveAllTaskNotesForCurrentReport();
        }

        List<RightnowTaskEntry> taskList = new ArrayList<RightnowTaskEntry>();
        HashMap<Long, String> taskNoteMap = new HashMap<Long, String>();
        for(RightnowReportRowEntry rowEntry : rightnowReportModel.getRightnowReportRowEntryList()) {
            String noteText = TaskwebappUtils.convertNoteListToTextualForm(rightnowReportModel.getTaskNotes(rowEntry.getId()));
            taskNoteMap.put(rowEntry.getId(), noteText);
        }
        return taskNoteMap;
        /*try {
            for (RightnowReportRowEntry rowEntry : rightnowReportModel.getRightnowReportRowEntryList()) {
                RightnowTaskEntry taskEntry = new RightnowTaskEntry();
                taskEntry.setId(rowEntry.getId());
                taskList.add(taskEntry);
            }
            if (taskList.size() > 0) {
                RightnowTaskListEntry taskListEntry = new RightnowTaskListEntry();
                taskListEntry.setTaskEntryList(taskList);
                RightnowTaskResponse response = RightnowTaskClient.getListOfTaskNotes(taskListEntry);
                taskNoteMap = new HashMap<Long, String>();
                for (RightnowTaskEntry taskEntry : response.getRightnowTaskEntryList()) {
                    StringBuilder notes = new StringBuilder();
                    int size = taskEntry.getNoteList().size();
                    int i = size - 1;
                    while (i >= 0) {
                        RightnowNoteEntry noteEntry = taskEntry.getNoteList().get(i);
                        notes.append(i + 1).append(". ").append(noteEntry.getText()).append("\n");
                        i--;
                    }

                    taskNoteMap.put(taskEntry.getId(), notes.toString());
                }
            }
        } catch (ERPServiceException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return taskNoteMap;*/
    }

    /**
     * This method reads the file uploaded by the user and updates the notes &
     * statuses of each task present in the file.
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {
        InputStream inputStream;

        try {
            UploadedFile file = event.getFile();
            inputStream = file.getInputstream();
            MyntraExcelReader excelReader = new MyntraExcelReader(inputStream);

            //get all the sheets of the excel file and iterate them to process the data
            List<MyntraSheetReader> sheets = excelReader.getSheetReaders();

            List<FacesMessage> allErrorMessages = new ArrayList<FacesMessage>();
            FacesMessage msgBlank = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "");
            if (sheets.size() > 0) {
                MyntraSheetReader sheet = sheets.get(0);
                List<FacesMessage> sheetErrorMessages = processEachExcelSheet(sheet);
                if (!sheetErrorMessages.isEmpty()) {
                    allErrorMessages.add(msgBlank);
                    allErrorMessages.add(msgBlank);
                    allErrorMessages.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "FOR SHEET WITH NAME \"" + sheet.getSheetName() + "\""));
                    allErrorMessages.addAll(sheetErrorMessages);
                }
            }

            /*Add error if any to the facecontext*/
            Iterator<FacesMessage> errorItr = allErrorMessages.iterator();
            if (allErrorMessages.size() > 0) {
                FacesMessage fileMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error in the excel file " + file.getFileName());
                FacesContext.getCurrentInstance().addMessage("excelUploadMessage", fileMessage);
            }
            while (errorItr.hasNext()) {
                FacesContext.getCurrentInstance().addMessage("excelUploadMessage", errorItr.next());
            }

            //after all sheets are processed, if error does not exists then save the data
            if (allErrorMessages.isEmpty()) {
                FacesMessage successfulMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Upload SuccessFul", "Upload is successful");
                FacesContext.getCurrentInstance().addMessage("excelUploadMessage", successfulMsg);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("Exception in handleFileUpload " + e.getMessage());
        }
    }

    private List<FacesMessage> processEachExcelSheet(MyntraSheetReader sheet) {
        List<FacesMessage> errorMessagesList = new ArrayList<FacesMessage>();
        RightnowTaskListEntry taskListEntry = new RightnowTaskListEntry();
        List<RightnowTaskEntry> taskEntries = new ArrayList<RightnowTaskEntry>();
        boolean addNoteColumnExists = false;
        boolean updateStatusColumnExists = false;
        int columnIndex = 0;
        int taskIdColumnIndex = -1;
        int addNoteColumnIndex = -1;
        int updateStatusColumnIndex = -1;
        //in ubuntu, it takes all the rows (65535) of the excel sheet even if rows are blank.
        //so we will have to see if consecutively 3 rows do not have task id (first column) treat them as blank and halt the processing of any further row
        int emptyRowsCount = 0;
        try {
            //the first column should mandatorily be Task ID
            //so we will check the first column of first row to be labeled as "Task ID"
            //if not, we will raise an exception and halt the further processing of the current sheet
            String firstColumnName = null;
            if (sheet.hasNextRow()) {
                sheet.nextRow();
                if (sheet.hasCell()) {
                    firstColumnName = sheet.readStringCellValue();
                }
            }
            //if first column is not Task ID, show an error message to the user
            //else set the Task ID column index
            if (!TASK_ID_COLUMN_NAME.equals(firstColumnName)) {
                String noTaskIDColumnError = "'Task ID' column is mandatory and should be the first column.";
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", noTaskIDColumnError);
                errorMessagesList.add(msg);
                return errorMessagesList;
            } else {
                taskIdColumnIndex = columnIndex;
            }

            //look for 'Add Note' & 'Update Status' columns and set their respective indexes
            //will be useful for reading user inputs in next set of rows
            while (sheet.hasCell()) {
                columnIndex++;
                String columnName = sheet.readStringCellValue();
                if ("Add Note".equals(columnName)) {
                    addNoteColumnExists = true;
                    addNoteColumnIndex = columnIndex;
                } else if ("Update Status".equals(columnName)) {
                    updateStatusColumnExists = true;
                    updateStatusColumnIndex = columnIndex;
                }
            }

            //if any of the two columns are missing, show an error message to the user
            if (!addNoteColumnExists || !updateStatusColumnExists) {
                String noTaskIDColumnError = "'Add Note' & 'Update Status' columns must be present in you excel file.";
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", noTaskIDColumnError);
                errorMessagesList.add(msg);
                return errorMessagesList;
            }

            //we are here, it means the excel columns are perfectly alright and 
            //we should proceed to reading of the actual data as eneterd by user
            while (sheet.hasNextRow()) {
                sheet.nextRow();
                //check for empty row, done by checking whether the Task ID value exists or not
                if (isEmptyRow(sheet, taskIdColumnIndex)) {
                    emptyRowsCount++;
                } else {
                    //create a task entry object for the current row
                    RightnowTaskEntry taskEntry = createTaskEntryForCurrentRow(sheet, taskIdColumnIndex, addNoteColumnIndex, updateStatusColumnIndex, errorMessagesList);
                    if (taskEntry != null) {
                        taskEntries.add(taskEntry);
                    }
                    emptyRowsCount = 0;
                }

                if (emptyRowsCount == MAX_NO_OF_EMPTY_ROWS_TO_STOP_PRCESSING) {
                    break;
                }
            }

            if (taskEntries.isEmpty()) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", "There are no notes & status provided for any of the tasks.");
                errorMessagesList.add(msg);
            }else if(taskEntries.size() > MAX_NO_ROWS_ALLOWED_IN_EXCEL) {
                errorMessagesList.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", 
                        "Maximum number tasks allowed to be updated in single upload is " + MAX_NO_ROWS_ALLOWED_IN_EXCEL));                
            }

            if (errorMessagesList.size() > 0) {
                return errorMessagesList;
            }

            //create the payload RightnowTaskListEntry
            taskListEntry.setTaskEntryList(taskEntries);
            //save the task object
            RightnowTaskResponse check = RightnowTaskClient.updateTasks(null, taskListEntry);

            if (!TaskwebappUtils.isSuccessResponse(check)) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", "Error while updating tasks");
                errorMessagesList.add(msg);
            }

        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ERPServiceException ex) {
            ex.printStackTrace();
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", "Error while updating tasks");
            errorMessagesList.add(msg);
        }
        return errorMessagesList;
    }

    private RightnowTaskEntry createTaskEntryForCurrentRow(MyntraSheetReader sheet, int taskIdIndex, int addNoteIndex, int updateStatusIndex, List<FacesMessage> errorMessages) {

        RightnowTaskEntry taskEntry = null;
        long taskId = 0;
        String note = "";
        String status = "";
        StringBuilder error = new StringBuilder("Error at Row Number: " + sheet.currentRowNum() + ". Incorrect Task ID value. ");
        //read the task id
        sheet.setColumn(taskIdIndex);
        try {
            String id = sheet.readCellValue().toString();
            taskId = Double.valueOf(id).longValue();
        } catch (ExcelPOIWrapperException e) {
            error.append(e.getMessage());
        } catch (NumberFormatException e) {
            error.append(e.getMessage());
        }
        if (taskId == 0) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "error", error.toString());
            errorMessages.add(message);

        } else {
            //get notes
            sheet.setColumn(addNoteIndex);
            try {
                Object noteObj = sheet.readCellValue();
                if (noteObj != null) {
                    note = noteObj.toString();
                }
            } catch (ExcelPOIWrapperException ex) {
                Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            }

            //get status
            sheet.setColumn(updateStatusIndex);
            try {
                Object statusObj = sheet.readCellValue();
                if (statusObj != null) {
                    status = (String) statusObj;
                    status = status.trim();
                    if (status.isEmpty() || status.equals("Open") || status.equals("Resolved")
                            || status.equals("Additional Information Required")) {
                        // Do nothing. These are valid states.
                    } else {
                        // Excel sheet has wrong status set for some tasks. Show error message
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Wrong Status in excel for Task " + taskId + " : " + status);
                        errorMessages.add(message);
                    }
                }
            } catch (ExcelPOIWrapperException ex) {
                Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            } 

            if (!note.isEmpty() || !status.isEmpty()) {
                taskEntry = new RightnowTaskEntry();
                taskEntry.setId(taskId);
                if (!note.isEmpty()) {
                    String newNote = note + ADDED_BY_TEXT + getLoggedInUserName();
                    taskEntry.setNote(newNote);
                }
                if (!status.isEmpty()) {
                    taskEntry.setSrStatus(status);
                }
            }
        }

        return taskEntry;
    }

    private boolean isEmptyRow(MyntraSheetReader sheet, int taskIdIndex) {
        boolean isEmpty = false;
        try {
            sheet.setColumn(taskIdIndex);
            if (!sheet.hasCell() || sheet.isCellEmpty()) {
                return true;
            }
            Object taskIdVal = sheet.readCellValue();
            if (taskIdVal == null) {
                isEmpty = true;
            } else {
                String taskIdValString = taskIdVal.toString().trim();
                if (taskIdValString.equals("")) {
                    isEmpty = true;
                } else {
                    long taskId = Double.valueOf(taskIdValString).longValue();
                }
            }

        } catch (ExcelPOIWrapperException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            isEmpty = true;
        } catch (NumberFormatException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
            isEmpty = true;
        }
        return isEmpty;
    }

    public void enableExcelUploader() {
        setShowExcelUploader(1);
        selectedTaskID = null;
    }
    
    public void enableCreateTaskUploader() {
        setShowExcelUploader(2);
        selectedTaskID = null;
    }
    
    public void enableDetractorUploader() {
        setShowExcelUploader(3);
        selectedTaskID = null;
    }
    
    public void enableCloseTasksUploader() {
        setShowExcelUploader(4);
        selectedTaskID = null;
    }

    public void getTaskAttachment() {
        try {
            RightnowTaskResponse response = RightnowTaskClient.getTaskAttachments(null, 603L);

            if (TaskwebappUtils.isSuccessResponse(response)) {
                if (response.getRightnowTaskEntryList() != null && response.getRightnowTaskEntryList().size() > 0) {
                    if (response.getRightnowTaskEntryList().get(0) != null
                            && response.getRightnowTaskEntryList().get(0).getFileList() != null) {
                        fileList = response.getRightnowTaskEntryList().get(0).getFileList();
                    }
                } else {
                    setMessage("No Tasks in this Report");
                }
            } else {
                setMessage(response.getStatus().getStatusMessage());
            }
        } catch (ERPServiceException ex) {
            setMessage("ERPServiceException " + ex.getMessage());
        }
    }

    public StreamedContent downloadAttachment(RightnowFileAttachmentEntry fileEntry) {
        StreamedContent attachment = null;
        try {
            
            RightnowTaskResponse response = RightnowTaskClient.getOriginalAttachment(null, fileEntry);
            if(TaskwebappUtils.isSuccessResponse(response)) {
                if(response.getRightnowTaskEntryList() != null && response.getRightnowTaskEntryList().size() > 0) {
                    RightnowTaskEntry taskEntry = response.getRightnowTaskEntryList().get(0);
                    if(taskEntry.getFileList() != null && taskEntry.getFileList().size() > 0) {
                        fileEntry = taskEntry.getFileList().get(0);
                        InputStream fis = new URL(FileUploadUtils.getPropertyValue("crmRightnowURL") + "tmp/images/crm/attachments/" + fileEntry.getFileName()).openStream();
                        attachment = new DefaultStreamedContent(fis, fileEntry.getContentType(), fileEntry.getFileName());
                    }
                }
            }
        } catch (ERPServiceException ex) {
            Logger.getLogger(TaskwebappController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return attachment;
    }

    public void showUploadAttachment() {
        /*if (selectedTasks != null && selectedTasks.length > 0) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("uploadDlg.show()");
        } else {
            FacesMessage noTasksSelectedError = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please select one or more tasks");
            FacesContext.getCurrentInstance().addMessage("taskStatusMessage", noTasksSelectedError);
        }*/
        if (selectedTaskID != null && selectedTaskID > 0) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("uploadDlg.show()");
        } else {
            FacesMessage noTasksSelectedError = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Please select a task before uploading attachment");
            FacesContext.getCurrentInstance().addMessage("taskUploadMessage", noTasksSelectedError);
        }
    }

    public void handleAttachmentUpload(FileUploadEvent event) throws IOException {
        InputStream in = null;
        OutputStream out = null;
        try {
            //get the uploaded file stream
            UploadedFile file = event.getFile();
            in = file.getInputstream();
            RightnowTaskResponse check = RightnowTaskClient.uploadAttachment(null, in, file.getFileName(), file.getContentType(), selectedTaskID);
            
            if (TaskwebappUtils.isSuccessResponse(check)) {
                FacesMessage uploadSuccess = new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Upload attachment successful");
                FacesContext.getCurrentInstance().addMessage("taskUploadMessage", uploadSuccess);
                rightnowReportModel.refetchTaskDetails(selectedTaskID);
            } else {
                FacesMessage uploadFailure = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Upload attachment Failed. Please contact support!");
                FacesContext.getCurrentInstance().addMessage("taskUploadMessage", uploadFailure);
                return;
            }
        } catch (FileNotFoundException ex) {
            setMessage("FileNotFoundException: " + ex.getMessage());
            FacesMessage exceptionMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error while uploading attachment: \n" + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage("taskUploadMessage", exceptionMessage);
        } catch (IOException ex) {
            setMessage("IOException: " + ex.getMessage());
            FacesMessage exceptionMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error while uploading attachment: \n" + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage("taskUploadMessage", exceptionMessage);
        } catch (ERPServiceException ex) {
            setMessage("ERPServiceException: " + ex.getMessage());
            FacesMessage exceptionMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error while uploading attachment: \n" + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage("taskUploadMessage", exceptionMessage);
        } finally {
            if (out != null) {
                out.close();
            }

            if (in != null) {
                in.close();
            }
        }
    }
}
