package com.myntra.commons.excel.poiImpl;

import com.myntra.commons.excel.MyntraSheetWriterInterface;
import static com.myntra.commons.excel.MyntraSheetWriterInterface.MODE_ROW;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import java.util.Collection;
import java.util.Date;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class MyntraSheetWriter implements MyntraSheetWriterInterface {

    private Sheet sheet;
    
    /**To store Excel Writer object which was used to create this MyntraSheetWriter object
     * Some of the functions common to all MyntraSheetWriters created from single MyntraExcelWriter
     * can be accessed from MyntraExcelWriter only, so this instance needs to be maintained.
     */
    private MyntraExcelWriter parentExcelWriter; 
    
    private int rowNum; /*To keep track of current row number*/

    private int colNum; /*To keep track of current column number*/

    private String mode; /*To keep track of writing move (row or column)*/

    private CellStyle unlockedCellStyle; /*Cell style to set cell as unprotected*/

    private CellStyle lockedCellStyle; /*Cell style to set cell as protected*/
    
    private XSSFCellStyle columnHeaderCellStyle; /* Cell style to be used for column header */

    private Boolean defaultUprotectedCell; /*To know default behavior of cell to be protected or unprotected if sheet is protected*/

    /*This needs to be removed as now we are maintaining this map in parentExcelWRiter*/
    //private Map<String, String> namedCellMap;//to store the mapping of named string to its reference

    /**
     * This function creates a sheet in the given workbook, assign its name as
     * given input //	* sets variable (defaultUnprotectedCell) to protect cell
     * by default if sheet is protected //	* Creates 1st row and 1st column and
     * set its style to protected Creates cellStyles for protected and
     * unprotected cells references stores all the named reference made in the
     * sheet so as to check while referencing a cell by its named reference
     *
     * @param sheetName name of the generated sheet
     * @param workbook workbook from which sheet should be generated
     * @throws ExcelPOIWrapperException
     */
    public MyntraSheetWriter(String sheetName, Workbook workbook,MyntraExcelWriter excelWriter) throws ExcelPOIWrapperException {
        if (workbook == null) {
            throw new ExcelPOIWrapperException("cannot create SheetWriter as workbook is null");
        }
        if (sheetName == null) {
            this.sheet = workbook.createSheet();
        } else {
            this.sheet = workbook.createSheet(sheetName);
        }
        parentExcelWriter = excelWriter;
        this.rowNum = 0;//this.rowNum=-1;rowNum++;
        this.colNum = 0;//this.colNum=-1;colNum++;
        //this.mode = "row";
        this.mode = MODE_ROW;
        defaultUprotectedCell = false;
        unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);
        lockedCellStyle = workbook.createCellStyle();
        lockedCellStyle.setLocked(true);
        columnHeaderCellStyle = (XSSFCellStyle)workbook.createCellStyle();
        
        XSSFColor highlightColor = new XSSFColor(java.awt.Color.YELLOW);
        columnHeaderCellStyle.setFillBackgroundColor(highlightColor);
        columnHeaderCellStyle.setFillForegroundColor(highlightColor);
        columnHeaderCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        columnHeaderCellStyle.setLocked(true);
        
//		Row row = sheet.createRow(rowNum);
//		Cell cell = row.createCell(colNum);
//		if(defaultUprotectedCell){
//			cell.setCellStyle(unlockedCellStyle);
//		}	

        //namedCellMap = new HashMap<String, String>();
    }

    /**
     * This function creates a sheet in the given workbook //	* sets variable
     * (defaultUnprotectedCell) to protect cell by default if sheet is protected
     * //	* Creates 1st row and 1st column and set its style to protected
     * Creates cellStyles for protected and unprotected cells references stores
     * all the named reference made in the sheet so as to check while
     * referencing a cell by its named reference
     *
     * @param sheetName name of the generated sheet
     * @param workbook workbook from which sheet should be generated
     * @throws ExcelPOIWrapperException
     */
    public MyntraSheetWriter(Workbook workbook,MyntraExcelWriter excelWriter) throws ExcelPOIWrapperException {
        if (workbook == null) {
            throw new ExcelPOIWrapperException("cannot create SheetWriter as workbook is null");
        }
        this.parentExcelWriter = excelWriter;
        this.sheet = workbook.createSheet();
        this.rowNum = 0;//this.rowNum=-1;rowNum++;
        this.colNum = 0;//this.colNum=-1;colNum++;
        this.mode = MODE_ROW;
        defaultUprotectedCell = false;
        unlockedCellStyle = workbook.createCellStyle();
        unlockedCellStyle.setLocked(false);
        lockedCellStyle = workbook.createCellStyle();
        lockedCellStyle.setLocked(true);
        columnHeaderCellStyle = (XSSFCellStyle)workbook.createCellStyle();
        
        XSSFColor highlightColor = new XSSFColor(java.awt.Color.YELLOW);
        columnHeaderCellStyle.setFillBackgroundColor(highlightColor);
        columnHeaderCellStyle.setFillForegroundColor(highlightColor);
        columnHeaderCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        columnHeaderCellStyle.setLocked(true);
//		Row row = sheet.createRow(rowNum);
//		Cell cell = row.createCell(colNum);
//		if(defaultUprotectedCell){
//			cell.setCellStyle(unlockedCellStyle);
//		}
        //namedCellMap = new HashMap<String, String>();
    }

    @Override
    public void hideSheet() {
        Workbook workbook = sheet.getWorkbook();
        workbook.setSheetHidden(workbook.getSheetIndex(sheet), true);
    }

    @Override
    public void unhideSheet() {
        Workbook workbook = sheet.getWorkbook();
        workbook.setSheetHidden(workbook.getSheetIndex(sheet), false);
    }

    @Override
    public void protectSheet(String password) {// throws ExcelPOIWrapperException{
        if (password == null) {
            password = "";
        }
        //throw new ExcelPOIWrapperException("With null as password, sheet will not be protected");
        this.sheet.protectSheet(password);
    }

    @Override
    public void setCellUnprotectedByDefault() {
        defaultUprotectedCell = true;
//		Row row = sheet.getRow(rowNum);
//		if(row!=null){
//			Cell cell = row.createCell(colNum);
//			if(cell!=null){
//				cell.setCellStyle(unlockedCellStyle);
//			}
//		}
    }

    @Override
    public void setCellProtectedByDefault() {
        defaultUprotectedCell = false;
//		Row row = sheet.getRow(rowNum);
//		if(row!=null){
//			Cell cell = row.createCell(colNum);
//			if(cell!=null){
//				cell.setCellStyle(lockedCellStyle);
//			}
//		}
    }

    @Override
    public void protectCell() {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//row.getCell(colNum);
        cell.setCellStyle(lockedCellStyle);
    }

    @Override
    public void unprotectCell() {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//row.getCell(colNum);
        cell.setCellStyle(unlockedCellStyle);

    }
    
    @Override
    public void setBackgroundColor(java.awt.Color color){
        XSSFCellStyle style = (XSSFCellStyle)sheet.getWorkbook().createCellStyle();
        XSSFColor color1 = new XSSFColor(color);
        style.setFillBackgroundColor(color1);
        style.setFillForegroundColor(color1);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Cell cell = getCell();
        cell.setCellStyle(style);
    }

    @Override
    public void setSheetHidden(){
        Workbook wb = sheet.getWorkbook();
        int sheetNum = wb.getSheetIndex(sheet.getSheetName());
        wb.setSheetHidden(sheetNum, 1);//1 de
    }
    
    @Override
    public void setSheetVeryHidden(){
        Workbook wb = sheet.getWorkbook();
        int sheetNum = wb.getSheetIndex(sheet.getSheetName());
        wb.setSheetHidden(sheetNum, 2); //int 2 denotes very hidden
    }
    
    @Override
    public void nextRow() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_COLUMN)) {
            throw new ExcelPOIWrapperException("Cannot call function nextRow() in Column mode.Switch to row mode or call nextColumn");
        }
        rowNum++;
        colNum = -1;
//		Row row = sheet.getRow(rowNum);
//		if(row==null){
//			row = sheet.createRow(rowNum);
//		}
        colNum++;
//		Cell cell = row.getCell(colNum);
//		if(cell==null){
//			cell = row.createCell(colNum);
//			if(defaultUprotectedCell){
//				cell.setCellStyle(unlockedCellStyle);
//			}
//		}
    }

    @Override
    public void nextColumn() throws ExcelPOIWrapperException {
        if (mode.equals(MODE_ROW)) {
            throw new ExcelPOIWrapperException("Cannot call function nextColumn() in row mode.Switch to column mode or call nextRow()");
        }
        colNum++;
        rowNum = -1;
        rowNum++;
//		Row row = sheet.getRow(rowNum);
//		if(row == null){
//			row = sheet.createRow(rowNum);
//		}
//		Cell cell = row.getCell(colNum);
//		if(cell==null){
//			cell =row.createCell(colNum);
//			if(defaultUprotectedCell){
//				cell.setCellStyle(unlockedCellStyle);
//			}
//		}
    }

    @Override
    public void nextCell(){
        if (mode.equals(MODE_ROW)) {
//			Row row = sheet.getRow(rowNum);
//			if(row==null) throw new ExcelPOIWrapperException("Row does not exists");
            colNum++;
//			Cell cell = row.getCell(colNum);
//			if(cell==null){
//				cell = row.createCell(colNum);
//				if(defaultUprotectedCell){
//					cell.setCellStyle(unlockedCellStyle);
//				}
//			}
        } //column mode
        else {
            rowNum++;
//			Row row = sheet.getRow(rowNum);
//			if(row == null){
//				row = sheet.createRow(rowNum);
//			}
//			Cell cell = row.getCell(colNum);
//			if(cell==null){
//				cell =row.createCell(colNum);
//				if(defaultUprotectedCell){
//					cell.setCellStyle(unlockedCellStyle);
//				}
//			}
        }

    }

    @Override
    public void setColumnMode() {
        mode = MODE_COLUMN;
    }

    @Override
    public void setRowMode() {
        mode = MODE_ROW;
    }

    @Override
    public int getRow() {
        return rowNum;
    }

    @Override
    public int getColumn() {
        return colNum;
    }

    @Override
    public void setRow(int rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public void setColumn(int colNum) {
        this.colNum = colNum;
    }

    @Override
    public void writeCellComment(String message) throws ExcelPOIWrapperException {
        if (message == null) {
            throw new ExcelPOIWrapperException("comments cannot be null");
        }
        ///creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
        
        Drawing drawing = cell.getSheet().createDrawingPatriarch();
        CreationHelper factory = cell.getSheet().getWorkbook().getCreationHelper();

        ClientAnchor anchor = factory.createClientAnchor();
        //int len = (message.length() / 8) == 0 ? 1 : message.length() / 8;
        /*Sets cells of anchor*/
        anchor.setCol1(cell.getColumnIndex());
        //anchor.setCol2(cell.getColumnIndex() + len);
        anchor.setCol2(cell.getColumnIndex() + 1);
        anchor.setRow1(cell.getRowIndex());
        //anchor.setRow2(cell.getRowIndex() + 2);
        anchor.setRow2(cell.getRowIndex() + 3);
        
        //sets the co-ordinates for anchor in the cell 
	    /*anchor.setDx1(100); //x coordinate of 1st cell
         anchor.setDx2(100); //x coordinate of 2nd cell
         anchor.setDy1(100); //y coordinate of 1st cell
         anchor.setDy2(100); //y coordinate of 2nd cell
         */	    // Create the comment and set the text
        Comment comment = drawing.createCellComment(anchor);
        RichTextString str = factory.createRichTextString(message);
        comment.setVisible(Boolean.FALSE);
        comment.setString(str);
        
        // Assign the comment to the cell
        cell.setCellComment(comment);
        
    }

    @Override
    public void setNamedRange(String name,int horizontalLength,int verticalLength) throws ExcelPOIWrapperException{
        if(name == null || name.isEmpty()){
            throw new ExcelPOIWrapperException("name is not valid. Name of the range of cells cannot be null or empty");
        }
        //int actualHorLen = horizontalLength;
        //int actualVerLen = verticalLength;
        int row = rowNum;
        int col = colNum;
        if(verticalLength < 0){
                row = row + verticalLength + 1;
                verticalLength = -verticalLength;
            }
            if(horizontalLength < 0){
                col = col + horizontalLength +1;
                horizontalLength = - horizontalLength;
            }
        if(row<0 || col < 0){
            throw new ExcelPOIWrapperException("Cell index is out of bound while writing drop down");    
        }    
        
        Name namedRange = sheet.getWorkbook().createName();
            namedRange.setNameName(name);
            String formula = sheet.getSheetName()+"!";//"sheetname!A1:A3"
            CellReference crStart = new CellReference(row, col);
            CellReference crEnd = new CellReference(row+verticalLength-1, col+horizontalLength-1);
            formula = formula + crStart.formatAsString()+":"+crEnd.formatAsString();
            namedRange.setRefersToFormula(formula);
            parentExcelWriter.setNamedCell(name, namedRange);
            
    }
    
    
//    @Override
//    public void setNamedCell(String name) {
//        CellReference cellR = new CellReference(rowNum, colNum);
//        String ret = cellR.formatAsString();
//        //System.out.println(ret)
//        namedCellMap.put(name, ret);
//        //referencesSet.add(ret);
//        //return ret;
//    }

    @Override
    public void goToNamedCell(String name) throws ExcelPOIWrapperException{
        if(name == null || name.isEmpty()){
            throw new ExcelPOIWrapperException("Reference name is invalid. It cannot be null/empty");
        }
        Name namedCell = parentExcelWriter.getNamedCell(name);
        if(namedCell == null){
            throw new ExcelPOIWrapperException("Reference " + name + " does not exists");
        }
        AreaReference area =  new AreaReference(namedCell.getRefersToFormula());
        CellReference cellR = area.getFirstCell();
        rowNum = cellR.getRow();
        colNum = cellR.getCol();
    }
    
//    public void moveToNamedCell1(String name) throws ExcelPOIWrapperException {
//        //if(!referencesSet.contains(name)) throw new ExcelPOIWrapperException("Reference "+name+" does not exists");
//        if (namedCellMap.get(name) == null) {
//            throw new ExcelPOIWrapperException("Reference " + name + " does not exists");
//        }
//        String ref = namedCellMap.get(name);
//        CellReference cellR = new CellReference(ref);
//        rowNum = cellR.getRow();
//        colNum = cellR.getCol();
//    }

    
    @Override
    public int getCurrentColumnWidth() {
        return (int)sheet.getColumnWidth(colNum)/255;
    }
    
    @Override
    public void setCurrentColumnWidth(int width) {
        sheet.setColumnWidth(colNum, width*255);
    }

    @Override
    public void writeBlank(Boolean... writeAndMoveParams){
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
  
        cell.setCellType(Cell.CELL_TYPE_BLANK);
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)){
            nextCell();
        }
    }

    @Override
    public void writeNumber(Double value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);

        if(value == null){
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        }
        else{
            cell.setCellValue(value);
        }
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

    @Override
    public void writeHeader(String value, Integer columnWidth, Boolean highlightCell, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();
        if(value !=null){
            cell.setCellValue(value);
            if(columnWidth == null){
                columnWidth = value.length();
            }
            if(columnWidth > getDefaultColumnWidth()){
                setCurrentColumnWidth(columnWidth);
            }
        }
        if(Boolean.TRUE.equals(highlightCell)){
            cell.setCellStyle(columnHeaderCellStyle);
        }
        else {
            cell.setCellStyle(lockedCellStyle);
        }
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }
    
    @Override
    public void writeText(String value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
        if(value !=null){
            cell.setCellValue(value);
        }
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

    @Override
    public void writeBoolean(Boolean value, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
        if(value !=null){
            cell.setCellValue(value);
        }
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

    @Override
    public void writeDate(Date date, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
        if(date !=null){
            CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
            CreationHelper createHelper = sheet.getWorkbook().getCreationHelper();
            cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("m/d/yy h:mm"));
            cell.setCellValue(date);
            cell.setCellStyle(cellStyle);
        }
        //sheet.autoSizeColumn(colNum);
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

//    @Override
//    public void writeDropDown(String[] values, String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
//        CellRangeAddressList range = new CellRangeAddressList(rowNum, rowNum, colNum, colNum);
//        //DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(values);
//        //DataValidation dataValidation = new HSSFDataValidation(range,dvConstraint);
//        //
//		/*DataValidationHelper dvHelper = sheet.getDataValidationHelper();
//         DataValidationConstraint dvc = dvHelper.createExplicitListConstraint(values);
//         DataValidation dataValidation = dvHelper.createValidation(dvc, range);
//         */
//        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
//        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(values);
//        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);
//
//        dataValidation.setSuppressDropDownArrow(true);
//        sheet.addValidationData(dataValidation);
//        //creates and returns a cell if it does not exits or returns the cell if it already exists
//        Cell cell = getCell();//sheet.getRow(rowNum).getCell(colNum);
//        cell.setCellValue(defaultValue);
//        //move to next cell if parameters say so
//        if (moveToNextCell(writeAndMoveParams)) {
//            nextCell();
//        }
//    }
    
    /**
     * This is to write drop downs in a range of cells instead of just one cell.
     * @param values
     * @param defaultValue
     * @param writeAndMoveParams
     * @throws ExcelPOIWrapperException 
     */
//    public void writeDropDownCells(Collection<String> values, String defaultValue,int horizontalLength,int verticalLength,Boolean... writeAndMoveParams) throws ExcelPOIWrapperException{
//        int actualHorLen = horizontalLength;
//        int actualVerLen = verticalLength;
//        int row = rowNum;
//        int col = colNum;
//        if(verticalLength < 0){
//                row = row + verticalLength + 1;
//                verticalLength = -verticalLength;
//            }
//            if(horizontalLength < 0){
//                col = col + horizontalLength +1;
//                horizontalLength = - horizontalLength;
//            }
//        if(row<0 || col < 0){
//            throw new ExcelPOIWrapperException("Cell index is out of bound while writing drop down");    
//        }    
//        
//        CellRangeAddressList range = new CellRangeAddressList(row, row + verticalLength -1, col, col + horizontalLength - 1);
//        String[] values1 = (String[]) values.toArray(new String[0]);
//        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
//        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(values1);
//        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);
//
//        dataValidation.setSuppressDropDownArrow(true);
//        sheet.addValidationData(dataValidation);
//        
//        //setting the default value
//        if(defaultValue !=null){
//            Cell cell;
//            Row row1;
//            for(int i=row;i<row+verticalLength;i++){
//                for(int j= col;j<col+horizontalLength;j++){
//                    row1 = sheet.getRow(i);
//                    if(row1==null) row1 = sheet.createRow(i);
//                    cell = row1.getCell(j);
//                    if(cell==null) cell = row1.createCell(j);
//                    cell.setCellValue(defaultValue);
//                }
//            }
//        }
//        //moving to next cell after writing dropdowns in all the cells present in specified range
//        if (moveToNextCell(writeAndMoveParams)) {
//            if(actualVerLen < 0){
//                rowNum = rowNum + actualVerLen +1;
//            }
//            else{
//                rowNum = rowNum + actualVerLen -1;
//            }
//            if(actualHorLen < 0){
//                colNum = colNum + actualHorLen +1;
//            }else{
//                colNum = colNum + actualHorLen -1;
//            }
//            
//            if (mode.equals(MODE_ROW)) {
//                colNum++;
//            }
//            else{
//                rowNum++;
//            }
//        }
//         
//    }
    
//    public void writeDropDownCells(Name namedCell, String defaultValue,int horizontalLength,int verticalLength,Boolean... writeAndMoveParams) throws ExcelPOIWrapperException{
//        List<Object> values = new ArrayList<Object>();
//        
//        int actualHorLen = horizontalLength;
//        int actualVerLen = verticalLength;
//        int row = rowNum;
//        int col = colNum;
//        if(verticalLength < 0){
//                row = row + verticalLength + 1;
//                verticalLength = -verticalLength;
//            }
//            if(horizontalLength < 0){
//                col = col + horizontalLength +1;
//                horizontalLength = - horizontalLength;
//            }
//        if(row<0 || col < 0){
//            throw new ExcelPOIWrapperException("Cell index is out of bound while writing drop down");    
//        }    
//        
//        CellRangeAddressList range = new CellRangeAddressList(row, row + verticalLength -1, col, col + horizontalLength - 1);
//        String[] values1 = (String[]) values.toArray(new String[0]);
//        
//        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
//        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(values1);
//        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);
//
//        dataValidation.setSuppressDropDownArrow(true);
//        sheet.addValidationData(dataValidation);
//        
//        //setting the default value
//        if(defaultValue !=null){
//            Cell cell;
//            Row row1;
//            for(int i=row;i<row+verticalLength;i++){
//                for(int j= col;j<col+horizontalLength;j++){
//                    row1 = sheet.getRow(i);
//                    if(row1==null) row1 = sheet.createRow(i);
//                    cell = row1.getCell(j);
//                    if(cell==null) cell = row1.createCell(j);
//                    cell.setCellValue(defaultValue);
//                }
//            }
//        }
//        //moving to next cell after writing dropdowns in all the cells present in specified range
//        if (moveToNextCell(writeAndMoveParams)) {
//            if(actualVerLen < 0){
//                rowNum = rowNum + actualVerLen +1;
//            }
//            else{
//                rowNum = rowNum + actualVerLen -1;
//            }
//            if(actualHorLen < 0){
//                colNum = colNum + actualHorLen +1;
//            }else{
//                colNum = colNum + actualHorLen -1;
//            }
//            
//            if (mode.equals(MODE_ROW)) {
//                colNum++;
//            }
//            else{
//                rowNum++;
//            }
//        }
//         
//    }

    @Override
    public void writeDropDown(Collection<String> values, String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        CellRangeAddressList range = new CellRangeAddressList(rowNum, rowNum, colNum, colNum);
        String[] values1 = (String[]) values.toArray(new String[0]);
        //DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(values1);
        //DataValidation dataValidation = new HSSFDataValidation(range,dvConstraint);
		/*DataValidationHelper dvHelper = sheet.getDataValidationHelper();
         DataValidationConstraint dvc = dvHelper.createExplicitListConstraint(values1);
         DataValidation dataValidation = dvHelper.createValidation(dvc, range);
         */

        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createExplicitListConstraint(values1);
        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);

        dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();
        cell.setCellValue(defaultValue);
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

    /**
     * @param writeAndMoveParams variable argument(varargs : present in java
     * >1.5). Only the 1st argument is read and it denotes if cursor moves to
     * next cell after writing. If no arguments are provided by default it
     * returns true
     * @return false if 1st argument is false else true
     * @throws ExcelPOIWrapperException
     */
    private Boolean moveToNextCell(Boolean... writeAndMoveParams){// throws ExcelPOIWrapperException {
        //check if boolean parameters are present. If not return true. Else use 1st param bool value
        Boolean writeAndMove = true;
//        if (writeAndMoveParams == null) {
//            throw new ExcelPOIWrapperException("Cannot pass null value. Pass boolean or don't pass anthing");
//        }
        if(writeAndMoveParams !=null){
            if (writeAndMoveParams.length > 0) {
                if (writeAndMoveParams[0] != null) {
                    writeAndMove = writeAndMoveParams[0].booleanValue();
                }
//                    else {
//                    throw new ExcelPOIWrapperException("Cannot pass null value. Pass boolean or don't pass anthing");
//                }
            } else {
                writeAndMove = true;
            }
        }
        return writeAndMove;
    }
    
    /**
     * This function checks if given row exists.If not it creates a row. 
     * Then checks if given cell exists. If not, then it creates the cell.
     * @return the current cell
     */
    private Cell getCell(){
        //create row and cell if they don't exists
        Row row = sheet.getRow(rowNum);
        if(row==null){
            row = sheet.createRow(rowNum);
        }
        Cell cell = sheet.getRow(rowNum).getCell(colNum);
        if(cell == null){
            cell = row.createCell(colNum);
            if(defaultUprotectedCell){
		cell.setCellStyle(unlockedCellStyle);
            }
        }
        return cell;
    }
    
    /*@Override
     public void writeDropDown(String[] values,int horizontalLen,int verticalLen,Boolean... writeAndMoveParams){
     CellRangeAddressList range = new CellRangeAddressList(rowNum, rowNum + horizontalLen-1,colNum, colNum + verticalLen-1);
     DVConstraint dvConstraint = DVConstraint.createExplicitListConstraint(values);
     DataValidation dataValidation = new HSSFDataValidation(range,dvConstraint);
     dataValidation.setSuppressDropDownArrow(false);
     sheet.addValidationData(dataValidation);
     //check if boolean parameters are present. If not then by default write and move. Else use 1st param bool value
     Boolean writeAndMove = writeAndMoveParams.length > 0 ? writeAndMoveParams[0].booleanValue() : true;
     if(writeAndMove) nextCell();
     }*/

    /**
     * This function takes input as a string name and list of values. It checks if namedCells are present 
     * in the excel with this name. If this present, it assigns drop down values to those namedCells and ignores the
     * list provided. If not, 
     * then it writes this list into a hidden sheet(named __hidden__), creates namedCells and assign them the name as this string
     * name. After this it writes in the dropdown these namedCells.
     * This name should be unique accross all sheets. 
     * @param namedCells
     * @param defaultValue
     * @param writeAndMoveParams
     * @throws ExcelPOIWrapperException 
     */
    @Override
    public <T> void writeNamedDropDown(String name, Collection<T> values,String defaultValue, Boolean... writeAndMoveParams) throws ExcelPOIWrapperException {
        Name namedCells = parentExcelWriter.getNamedCell(name);
        while(namedCells == null){
            parentExcelWriter.writeDropDownContent(name,values);
            namedCells = parentExcelWriter.getNamedCell(name);
        }
        CellRangeAddressList range = new CellRangeAddressList(rowNum, rowNum, colNum, colNum);
        
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint(namedCells.getRefersToFormula());
        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);
        dataValidation.setShowErrorBox(true);
        //dataValidation.setSuppressDropDownArrow(false);
        sheet.addValidationData(dataValidation);
        //creates and returns a cell if it does not exits or returns the cell if it already exists
        Cell cell = getCell();
        cell.setCellValue(defaultValue);
        //move to next cell if parameters say so
        if (moveToNextCell(writeAndMoveParams)) {
            nextCell();
        }
    }

    @Override
    public int getDefaultColumnWidth() {
        return sheet.getDefaultColumnWidth();
    }

    @Override
    public void setDefaultColumnWidth(int width) {
        sheet.setDefaultColumnWidth(width);
    }
    
    @Override
    public void createFreezePane(int colSplit, int rowSplit, int leftmostColumn, int topRow){
        sheet.createFreezePane(colSplit, rowSplit, leftmostColumn, topRow);
    }
    
    @Override
    public void createFreezePane(int colSplit, int rowSplit){
        sheet.createFreezePane(colSplit, rowSplit);
    }
}
