/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.commons.excel.test;

import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author paroksh
 */
public class RangeDropDown {
    public static void main(String[] args) throws IOException{
        XSSFDD();
//        String countryName[] = new String[3];
//        countryName[0] = "India";
//        countryName[1] = "Australia";
//        countryName[2] = "Argentina";
//                
//    HSSFWorkbook workbook = new HSSFWorkbook();
//HSSFSheet realSheet = workbook.createSheet("Sheet xls");
//HSSFSheet hidden = workbook.createSheet("hidden");
//for (int i = 0, length= countryName.length; i < length; i++) {
//   String name = countryName[i];
//   HSSFRow row = hidden.createRow(i);
//   HSSFCell cell = row.createCell(0);
//   cell.setCellValue(name);
// }
//
// 
// Name namedCell = workbook.createName();
// namedCell.setNameName("par");
// namedCell.setRefersToFormula("hidden!$A$1:$A$" + countryName.length);
// DVConstraint constraint = DVConstraint.createFormulaListConstraint("par");
// CellRangeAddressList addressList = new CellRangeAddressList(0, 0, 0, 0);
// HSSFDataValidation validation = new HSSFDataValidation(addressList, constraint);
// workbook.setSheetHidden(1, true);
// realSheet.addValidationData(validation);
// FileOutputStream stream = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/range.xlsx");
// workbook.write(stream);
// stream.close();
    }
    
    public static void XSSFDD() throws IOException{
        
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        String countryName[] = new String[3];
        countryName[0] = "India";
        countryName[1] = "Australia";
        countryName[2] = "Argentina";
        
        //Sheet realSheet = wb.createSheet("Sheet xls");
        Sheet hidden = wb.createSheet("hidden");
        for (int i = 0, length= countryName.length; i < length; i++) {
           String name = countryName[i];
           Row row = hidden.createRow(i);
           Cell cell = row.createCell(0);
           cell.setCellValue(name);
         }

        wb.setSheetHidden(1, true);
        Name namedCell = wb.createName();
        namedCell.setNameName("par");
        namedCell.setRefersToFormula("hidden!A1:A" + countryName.length);
 
        CellRangeAddressList range = new CellRangeAddressList(0,0,0,0);
        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) sheet);
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint("par");
        XSSFDataValidation dataValidation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, range);

        //dataValidation.setSuppressDropDownArrow(true);
        sheet.addValidationData(dataValidation);
        FileOutputStream stream = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/rangeXSSF.xlsx");
        wb.write(stream);
        stream.close();
        CellReference cr = new CellReference(-3, 3);
        System.out.println(cr.formatAsString());
//.createExplicitListConstraint(values);
    }
}
