package com.myntra.commons.excel.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.myntra.commons.excel.enums.CellType;
import com.myntra.commons.excel.exceptions.ExcelPOIWrapperException;
import com.myntra.commons.excel.poiImpl.MyntraExcelReader;
import com.myntra.commons.excel.poiImpl.MyntraExcelWriter;
import com.myntra.commons.excel.poiImpl.MyntraSheetReader;
import com.myntra.commons.excel.poiImpl.MyntraSheetWriter;

public class ReadWriteTest {

    public static void main(String[] args) throws IOException, ExcelPOIWrapperException, InvalidFormatException {
        FileInputStream fileI = new FileInputStream("/home/paroksh/myntra/CMS/paroTesting/template.xls");
        MyntraExcelReader excelReader = new MyntraExcelReader(fileI);
        List<MyntraSheetReader> readSheets = excelReader.getSheetReaders();

        FileOutputStream fileO = new FileOutputStream("/home/paroksh/myntra/CMS/paroTesting/download.xlsx");
        MyntraExcelWriter excelWriter = new MyntraExcelWriter(fileO);

        Iterator<MyntraSheetReader> itr = readSheets.iterator();

        while (itr.hasNext()) {//while there exists sheets to read from. Here only template file is present so reading that
            MyntraSheetReader sheet = itr.next();

            //sheet.setColumnMode();
            //while cell is present to read article types from in 1st column 
            System.out.println("hey");
            while (sheet.hasNextRow()) {//while there exists a row to read
                sheet.nextRow();
                if (sheet.hasCell()) { //if 1st column of row exists(if not move to next cell in column mode)
                    System.out.println("in");
                    //sheet.setRowMode();
                    System.out.println(sheet.getCellType());
                    if (sheet.getCellType() != CellType.DOESNOTEXISTS
                            && sheet.getCellType() != CellType.BLANK) {
                        System.out.print(sheet.getCellType() + "\t");
                        System.out.println(sheet.readCellValue(false));
                        String ArticleType = (String) sheet.readCellValue();
                        Map<String, List<String>> attributesMap = new HashMap<String, List<String>>();

                        //while there are attributes in the row
                        while (sheet.hasCell()) {//Row mode
                            String namedCell = null;
                            sheet.setNamedCell(namedCell); //set current cell as named cell to reach this point
                            sheet.setColumnMode();
                            String attribute = (String) sheet.readCellValue();//read attribute
                            List<String> values = new ArrayList<String>();//list to store attribute values
                            while (sheet.hasCell() && sheet.getCellType() != CellType.BLANK
                                    && sheet.getCellType() != CellType.DOESNOTEXISTS) {//column mode
                                System.out.println(sheet.getCellType());
                                values.add((String) sheet.readCellValue());
                            }
                            attributesMap.put(attribute, values);
                            sheet.moveToNamedCell(namedCell); //reach back to top attribute
                            sheet.setRowMode();
                            sheet.nextCell();
                        }
                        //sheet.setRowMode();
                        //sheet.nextRow();
                        //sheet.setColumnMode();
                        //now write this map to a sheetWriter
                        MyntraSheetWriter sheetWriter = excelWriter.createSheetWriter(ArticleType);
                        for (String att : attributesMap.keySet()) {
                            sheetWriter.setColumnMode();
                            sheetWriter.writeText(att);
                            sheetWriter.writeDropDown(attributesMap.get(att), attributesMap.get(att).get(0));
                            sheetWriter.nextColumn();
                        }
                    } else {
                        sheet.nextCell();//move to next cell in column
                    }
                    //	excelWriter.save();
                    //set column mode before processing
                    sheet.setColumnMode();
                }
                sheet.setRowMode();
            }
            System.out.println("no row left");
            excelWriter.save();
        }
    }
}
