package com.myntra.erp.crm.oneview.model;

import com.myntra.erp.crm.client.entry.ShipmentStatusRevenueEntry;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pravin
 */
public class ShipmentStatusRevenueModel implements Serializable {
    
    private HashMap<String, Integer> statusRevenueMap;
    
    private HashMap<String, Integer> statusQuantityMap;

    public HashMap<String, Integer> getStatusRevenueMap() {
        return statusRevenueMap;
    }

    public void setStatusRevenueMap(HashMap<String, Integer> statusMap) {
        this.statusRevenueMap = statusMap;
    }

    public HashMap<String, Integer> getStatusQuantityMap() {
        return statusQuantityMap;
    }

    public void setStatusQuantityMap(HashMap<String, Integer> statusQuantityMap) {
        this.statusQuantityMap = statusQuantityMap;
    }
    
    public void setShipmentStatuses(ShipmentStatusRevenueEntry shipmentStatusRevenueEntry) {
        statusRevenueMap = new HashMap<String, Integer>();
        statusQuantityMap = new HashMap<String, Integer>();
        
        statusRevenueMap.put("C", 0);
        statusRevenueMap.put("DL", 0);
        statusRevenueMap.put("SH", 0);
        
        statusRevenueMap.put("OH", 0);
        statusRevenueMap.put("PK", 0);
        statusRevenueMap.put("WP", 0);
        
        statusRevenueMap.put("Q", 0);
        
        statusRevenueMap.put("D", 0);
        statusRevenueMap.put("F", 0);
        statusRevenueMap.put("L", 0);
        
        if(shipmentStatusRevenueEntry!=null && null!=shipmentStatusRevenueEntry.getShipmentStatusRevenueMap()) {
            HashMap<String, Double> statusMapDoubleValue = shipmentStatusRevenueEntry.getShipmentStatusRevenueMap();
            for (Map.Entry pairs : statusMapDoubleValue.entrySet()) {
                String status = (String) pairs.getKey();
                Double value = (Double) pairs.getValue();
                statusRevenueMap.put(status, Integer.valueOf(value.intValue()));
                statusQuantityMap.put(status, shipmentStatusRevenueEntry.getShipmentStatusQuantityMap().get(status));
            }
        }
    }
}
