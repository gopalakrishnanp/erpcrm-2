/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.erp.crm.oneview.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author pravin
 */
@ManagedBean(name = "app", eager = true)
@ApplicationScoped
public class ApplicationProperties {

    private static final String FILE = "application.properties";
    private static Properties properties;

    public ApplicationProperties() {
        properties = new Properties();
        reload();
    }

    public String getBase() {
        return getProperty("baseUrl");
    }
    
    public String getProperty(String key){
        if(properties == null){
            reload();
        }
        return properties.getProperty(key);
    }

    private String getServerConfDir() {
        return System.getProperty("catalina.base");
    }

    private void reload() {
        try {
            File f = new File(getServerConfDir(), "/conf/" + FILE);
            InputStream instream = new FileInputStream(f);
            properties.clear();
            properties.load(instream);
            instream.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ApplicationProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ApplicationProperties.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}