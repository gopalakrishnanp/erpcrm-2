package com.myntra.erp.crm.oneview.controller;

import com.myntra.commons.exception.ERPServiceException;

import com.myntra.erp.crm.client.CashbackClient;
import com.myntra.erp.crm.client.CustomerOrderClient;

import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CashbackResponse;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

import com.myntra.erp.crm.oneview.model.CashbackModel;
import com.myntra.erp.crm.oneview.model.CouponModel;
import com.myntra.erp.crm.oneview.model.CustomerProfileModel;
import com.myntra.erp.crm.oneview.model.LoyaltyModel;
import com.myntra.erp.crm.oneview.model.OrderModel;
import com.myntra.erp.crm.oneview.model.ReturnModel;
import com.myntra.erp.crm.oneview.utils.Utility;
import com.myntra.lms.client.response.OrderResponse;
import com.myntra.portal.crm.client.CouponDetailClient;
import com.myntra.portal.crm.client.ProfileClient;

import com.myntra.portal.crm.client.response.CouponResponse;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleEvent;

/**
 *
 * @author pravin
 */
@ManagedBean(name = "crm")
@ViewScoped
public class OneViewController implements Serializable {
    
    private static final long serialVersionUID = 8931635278543997819L;

    private String searchTerm;
    private String loginID;
    private String orderID;
    private String shipmentID;
    private String returnID;
    private String mobile;
    private String comments;
    private String searchParameter; // Used for passing parameter to one view
    
    /* Only need data models here. We should not use any entity/entry here. 
     * Access to entry objects should be defined in the Model classes.
     */
    private CashbackModel cashbackModel;
    private LoyaltyModel loyaltyModel;
    private CouponModel couponModel;
    private CustomerProfileModel customerProfileModel;
    private OrderModel orderModel;    
    //private ShipmentStatusRevenueModel shipmentStatusRevenueModel;
    private ReturnModel returnModel;
    private boolean isLoginEmailSearch = false;
    private boolean isMobileSearch = false;
    private boolean isOrderSearch = false;
    private boolean isReturnSearch = false;
    private int ordersToFetch;
    
    private final static int defaultNumberOfOrders = 2;

    public OneViewController() {        
    }
    
    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getShipmentID() {
        return shipmentID;
    }

    public void setShipmentID(String shipmentID) {
        this.shipmentID = shipmentID;
    }
    
    public String getReturnID() {
        return returnID;
    }

    public void setReturnID(String returnID) {
        this.returnID = returnID;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public CustomerProfileModel getCustomerProfileModel() {
        return customerProfileModel;
    }

    public void setCustomerProfileModel(CustomerProfileModel customerProfileModel) {
        this.customerProfileModel = customerProfileModel;
    }

    public OrderModel getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(OrderModel orderModel) {
        this.orderModel = orderModel;
    }

    public CashbackModel getCashbackModel() {
        return cashbackModel;
    }

    public void setCashbackModel(CashbackModel cashbackModel) {
        this.cashbackModel = cashbackModel;
    }

    public LoyaltyModel getLoyaltyModel() {
        return loyaltyModel;
    }

    public void setLoyaltyModel(LoyaltyModel loyaltyModel) {
        this.loyaltyModel = loyaltyModel;
    }
    /*
    public ShipmentStatusRevenueModel getShipmentStatusRevenueModel() {
        return shipmentStatusRevenueModel;
    }

    public void setShipmentStatusRevenueModel(ShipmentStatusRevenueModel shipmentStatusSummaryModel) {
        this.shipmentStatusRevenueModel = shipmentStatusSummaryModel;
    }
    */ 

    public ReturnModel getReturnModel() {
        return returnModel;
    }

    public void setReturnModel(ReturnModel returnModel) {
        this.returnModel = returnModel;
    }

    public CouponModel getCouponModel() {
        return couponModel;
    }

    public void setCouponModel(CouponModel couponModel) {
        this.couponModel = couponModel;
    }
    
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        if (comments != null && !comments.trim().equals("")) {
            if(this.comments==null || this.comments.trim().equals("")) {
                 this.comments=comments;
            } else {
                this.comments = this.comments + "<br/>" + comments;
            }
        }
    }

    public String getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(String searchParameter) {
        this.searchParameter = searchParameter;
    }

    public boolean isIsMobileSearch() {
        return isMobileSearch;
    }
    
    public boolean isIsOrderSearch() {
        return isOrderSearch;
    }

    public boolean isIsReturnSearch() {
        return isReturnSearch;
    }
    
    public boolean isIsLoginEmailSearch() {
        return isLoginEmailSearch;
    }

    public void setIsLoginEmailSearch(boolean isLoginEmailSearch) {
        this.isLoginEmailSearch = isLoginEmailSearch;
    }
       
    public void setIsMobileSearch(boolean isMobileSearch) {
        this.isMobileSearch = isMobileSearch;
    }

    public int getOrdersToFetch() {
        return ordersToFetch;
    }

    public void setOrdersToFetch(int ordersToFetch) {
        this.ordersToFetch = ordersToFetch;
    }

    private boolean isSearchTermSet() {
        return(isLoginEmailSearch || isMobileSearch || isOrderSearch || isReturnSearch);
    }

    private void resetOneView() {

        if (isLoginEmailSearch) {
            refreshCustomerProfileModel(false);

        } else if(isMobileSearch) {
            refreshOrderModel(false);
            
        } else if (isOrderSearch) {
            // Searching by Order ID or Shipment ID
            refreshOrderModel(false);
            setDataFromOrderSearch(getOrderID());
            
        } else if (isReturnSearch) {            
            refreshReturnModel(false);
            setDataFromReturnSearch();
        }
    }

    private void clear() {
        // Set all String fields to blank.
        loginID = "";
        orderID = "";
        shipmentID = "";
        returnID = "";
        mobile = "";
        comments = "";

        // Reset search Type
        isLoginEmailSearch = false;
        isMobileSearch = false;
        isOrderSearch = false;
        isReturnSearch = false;
        
        ordersToFetch = defaultNumberOfOrders;

        // Reset all model objects to null.
        cashbackModel = null;
        loyaltyModel = null;
        customerProfileModel = null;
        orderModel = null;
        // shipmentStatusRevenueModel = null;
        couponModel = null;
        returnModel = null;       
    }

    // Called only when searchParam is passed as get parameter in the url
    public void saveSearchParameter() {
        if(FacesContext.getCurrentInstance().isPostback()) {            
            return;
        }
        
        String searchParamGetURL = getSearchParameter();
        if(searchParamGetURL!=null) {
            setSearchTerm(searchParamGetURL);
            saveSearchTerm();
        }
    }
    
    public void saveSearchTerm(ActionEvent event) {
        saveSearchTerm();
    }
    
    public void saveSearchTerm() {  
        String term = getSearchTerm();
        
        if(term!=null) {
            term = term.trim();
            setSearchTerm(term);
        }
        boolean resetOneView = false;
        
        if (term == null || term.equals("")) {
            if(isSearchTermSet()) {
                clear();
                setComments("Reset One View");
            }
            return;
        }        

        if (term.indexOf("@") >= 0) {
            if (!term.equals(getLoginID()) || isLoginEmailSearch == false) {
                clear();
                isLoginEmailSearch = true;
                
                if(term.endsWith(".invalid")) {
                    term = term.substring(0, term.length()-8);  
                    setSearchTerm(term);
                }
                setLoginID(term);
                resetOneView = true;
            }
        } else if (Utility.isNumeric(term)) {
            if (term.length() == 10) {
                if (!term.equals(getMobile()) || isMobileSearch == false) {
                    clear();
                    isMobileSearch = true;
                    setMobile(term);
                    resetOneView = true;
                }
            } else if(term.length()<=6) {
                if (!term.equals(getReturnID()) || isReturnSearch == false) {
                    clear();
                    isReturnSearch = true;
                    setReturnID(term);
                    resetOneView = true;
                }
            } else if (term.length() < 10) {
                if (!term.equals(getOrderID()) || isOrderSearch == false) {
                    clear();
                    isOrderSearch = true;
                    setOrderID(term);
                    resetOneView = true;
                }
            } else {
                clear();
                setComments("No matching details for " + searchTerm);
            }
        } else if(term.substring(0,1).equalsIgnoreCase("r") 
                && term.length()>1 && Utility.isNumeric(term.substring(1))) {
            
            // Return search can begin with 'R' followed by numbers.
            String subTerm = term.substring(1);
            subTerm = subTerm.trim();
            
            if (!subTerm.equals(getReturnID()) || isReturnSearch == false) {
                clear();
                isReturnSearch = true;
                setReturnID(subTerm);
                resetOneView = true;
            }
            
        } else {
            clear();
            setComments("No matching details for " + searchTerm);
        }

        if (resetOneView == true) {
            resetOneView();
        }
    }

    private void refreshCustomerProfileModel(boolean hardReload) {
        
        if(loginID==null || loginID.equals("")) {
            //setComments("No associated Login ID");
            return;
        }

        if (customerProfileModel == null || hardReload) {
            customerProfileModel = new CustomerProfileModel();
            try {
                CustomerProfileResponse response = ProfileClient.findByLogin(null, getLoginID());
                if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                    customerProfileModel.setCustomerProfileModel(response.getCustomerProfileEntry());
                    setMobile(customerProfileModel.getMobile());
                }
            } catch (ERPServiceException e) {
                setComments(e.getMessage());
            }
        }
    }

    private void refreshCashbackModel(boolean hardReload) {
        if (isIsMobileSearch()) {
            return;
        }
        
        if(loginID==null || loginID.equals("")) {
            return;
        }

        try {
            if (cashbackModel == null || hardReload) {
                CashbackResponse response = CashbackClient.getCashbackLogs(null, getLoginID());
                cashbackModel = new CashbackModel();
                cashbackModel.setCashbackBalanceEntry(response.getCashbackBalanceEntry());
                cashbackModel.setCashbackLogEntryList(response.getCashbackLogEntryList());
            }
        } catch (ERPServiceException e) {
            setComments(e.getMessage());
        } catch (Exception e) {
            setComments(e.getMessage());
        }
    }
    
    private void refreshLoyaltyModel(boolean hardReload) {
        if(loginID==null || loginID.equals("")) {
            return;
        }

        try {
            if (loyaltyModel == null || hardReload) {
                loyaltyModel = new LoyaltyModel();
                loyaltyModel.fetchData(loginID);
            }
        } catch (ERPServiceException e) {
            setComments(e.getMessage());
        } catch (Exception e) {
            setComments(e.getMessage());
        }
    }

    private void refreshCouponModel(boolean hardReload) {
        
        if(loginID==null || loginID.equals("")) {
            return;   
        }

        try {
            if (couponModel == null || hardReload) {
                CouponResponse couponResponse = CouponDetailClient.getCouponDetail(null, getLoginID());
                couponModel = new CouponModel();
                couponModel.setCouponEntryList(couponResponse.getCouponEntryList());
            }
        } catch (ERPServiceException e) {
            setComments(e.getMessage());
        } catch (Exception e) {
            setComments(e.getMessage());
        }
    }
    
    public void refreshCashbackAndCouponModel(boolean hardReload) {
        refreshCashbackModel(hardReload);
        refreshCouponModel(hardReload);
        refreshLoyaltyModel(hardReload);
    }

    /*private void refreshShipmentStatusSummaryModel() {
     if (isIsMobileSearch()) {
     return;
     }

     try {
     if (shipmentStatusRevenueModel == null) {
     CustomerOrderSummaryResponse response = CustomerOrderSummaryClient.getCustomerOrderSummary(Utility.getCRMurl(), getLoginID());
     shipmentStatusRevenueModel = new ShipmentStatusRevenueModel();
     shipmentStatusRevenueModel.setShipmentStatuses(response.getShipmentStatusRevenueEntry());
     }
     } catch (ERPServiceException e) {
     setComments(e.getMessage());
     } catch (Exception e) {
     setComments(e.getMessage());
     }
     }*/
      
    
    
    public void refreshReturnModel(boolean hardReload) {
        
        if(isMobileSearch)
            return;
        
        try {           
            
            if (returnModel == null || hardReload) {
                returnModel = new ReturnModel();
                
                if(isReturnSearch){
                    Long retId = Long.valueOf(returnID);
                    returnModel.fetchData(retId, null, null);
                } else if(isLoginEmailSearch){
                    returnModel.fetchData(null, loginID, null);
                } else if(isOrderSearch) {
                    returnModel.fetchData(null, null, orderModel);
                }
            }
        } catch (ERPServiceException e) {
            setComments(e.getMessage());
        } catch (Exception e) {
            setComments(e.getMessage());
        }
        
    }

    public void refreshOrderModel(boolean hardReload) {

        try {
            if (orderModel == null || hardReload) {
                orderModel = new OrderModel();

                CustomerOrderResponse response = null;
                if (isOrderSearch || isReturnSearch) {
                    response = CustomerOrderClient.getCustomerOrderDetails(null, getOrderID(), 1, true);
                    //response = CustomerOrderClient.getCustomerOrderMinimumDetails(null, getOrderID(), 1);
                } else if (isMobileSearch) {
                    response = CustomerOrderClient.getCustomerOrderDetails(null, getMobile(), ordersToFetch, true);
                    //response = CustomerOrderClient.getCustomerOrderMinimumDetails(null, getMobile(), ordersToFetch);
                } else if (isLoginEmailSearch) {
                    response = CustomerOrderClient.getCustomerOrderDetails(null, getLoginID(), ordersToFetch, true);
                    //response = CustomerOrderClient.getCustomerOrderMinimumDetails(null, getLoginID(), ordersToFetch);
                }

                if (response != null && response.getStatus().getStatusType().equals("SUCCESS")) {
                    orderModel.setCustomerOrderEntryList(response.getCustomerOrderEntryList());
                }
            }
        } catch (ERPServiceException e) {
            setComments(e.getMessage());
        }
    }

    private void setDataFromOrderSearch(String shipmentId) {
        // This would be an order Search as orderIDs have max 8 digits.
        // We need to check if its shipment ID 
        if(orderModel==null) {
            return;
        }
        
        List<CustomerOrderEntry> customerOrderEntryList = orderModel.getCustomerOrderEntryList();
        
        if (customerOrderEntryList != null && customerOrderEntryList.size() > 0) {
            String login = customerOrderEntryList.get(0).getLogin();
            String orderId = "" + customerOrderEntryList.get(0).getOrderId();
            
            setShipmentID(shipmentId);
            setOrderID(orderId);
            setLoginID(login);
            
            refreshCustomerProfileModel(false);
        }
    }
    
    private void setDataFromReturnSearch() {        
        if(returnModel == null){
            return;
        }        
        List<CustomerReturnEntry> returnEntryList = returnModel.getReturnEntryList();
        if(returnEntryList != null && returnEntryList.size()>0){
            setOrderID(String.valueOf(returnEntryList.get(0).getOrderId()));
            setLoginID(String.valueOf(returnEntryList.get(0).getLogin()));
            setMobile(String.valueOf(returnEntryList.get(0).getMobile()));
        }
    }

    public void resetOrderPanel(Integer count) {
        if (getOrdersToFetch() != count) {
            setOrdersToFetch(count);
            setOrderModel(null);
            resetOrderPanel(count);
        }
    }

    public void handleToggle(ToggleEvent event) {
        
        String componentID = event.getComponent().getId();
        String visibility = event.getVisibility().name();
        
        if (visibility.equals("VISIBLE")) {
            if (componentID.equals("profilesummarypanel")) {
                refreshCustomerProfileModel(false);
            } else if (componentID.equals("orderhistorypanel")) {
                refreshOrderModel(false);
            } else if (componentID.equals("cashbackandcouponpanel")) {
                refreshCashbackModel(false);    
            } else if (componentID.equals("returnpanel")) {
                refreshReturnModel(false);
            }
        }
    }

    public void handleCommentsToggle(Long shipmentID) {
        if (orderModel != null) {
            orderModel.getShipmentComments(shipmentID);
        }
    }

    public void handleOnHoldCommentsToggle(Long shipmentID) {
        if (orderModel != null) {
            orderModel.getShipmentOnHoldComments(shipmentID);
        }
    }


    public void handleProfileTabChange(TabChangeEvent event) {
        if (loginID==null || loginID.equals("")) {
            return;
        }

        TabView tv = (TabView) event.getComponent();

        switch (tv.getActiveIndex()) {
            case 0:
                refreshCustomerProfileModel(false);
                break;

            case 1:
                refreshCashbackModel(false);
                break;
                
            case 2:
                refreshLoyaltyModel(false);
                break;
        }
    }

    public void handleCommentsTabChange(TabChangeEvent event) {
        
        FacesContext context = FacesContext.getCurrentInstance();
        Long shipmentIDvalue = context.getApplication().evaluateExpressionGet(context, "#{shipmentEntry.shipmentId}", Long.class);

        TabView tv = (TabView) event.getComponent();
        
        switch (tv.getActiveIndex()) {
            case 0:
                break;

            case 1:
                updateOrderComments(shipmentIDvalue);
                break;

            case 2:
                updateOnHoldComments(shipmentIDvalue);
                break;
        }
    }

    public void updateOrderComments(Long shipmentID) {
        if(orderModel!=null)
            orderModel.getShipmentComments(shipmentID);
    }   
    
    public void updateOnHoldComments(Long shipmentID) {
        if(orderModel!=null)
            orderModel.getShipmentOnHoldComments(shipmentID);
    }
    
    public void handleCashBackCouponTabChange(TabChangeEvent event) {

        if (loginID==null || loginID.equals("")) {
            return;
        }

        TabView tv = (TabView) event.getComponent();

        switch (tv.getActiveIndex()) {
            case 0:
                refreshCashbackModel(false);
                break;

            case 1:
            case 2:
            case 3:
                refreshCouponModel(false);
                break;
            case 4:
                refreshLoyaltyModel(false);
                break;
        }
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void applyShortcut(String key) {
        RequestContext context = RequestContext.getCurrentInstance();
        Character test = key.toLowerCase().charAt(0);
        
        switch (test) {
            case 'p':
                context.execute("mainForm:profilesummarypanel.toggle()");
                break;

            case 'o':
                context.execute("mainForm:orderhistorypanel.toggle()");
                break;

            case 'c':
                context.execute("mainForm:cashbackandcouponpanel.toggle()");
                break;
    
        }
    }
    
    public void updateOrderModel(String str) {
        int orderCount;
        if(str.equalsIgnoreCase("+20")) {
            orderCount=getOrdersToFetch()+20;
        } else {
            orderCount = Integer.parseInt(str);
        }
        if(orderCount!=getOrdersToFetch()) {
            setOrdersToFetch(orderCount);
            orderModel=null;
            returnModel=null;
            refreshOrderModel(true);
        }
    }
    
    public String redirectURL() {
        String term = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mainForm:searchterm");
        String url;
        if(term==null || term.trim().equals("")) {
            url = "index?faces-redirect=true";
        } else  {
            url = "index?faces-redirect=true&amp;includeViewParams=true&amp;";
            url = url + "searchParam=" + term;
        }
        return url;
    }
    
    public void changePickupStatus(String status, long returnId){
        
        OrderResponse orderResponse;
        
        try {
            orderResponse = returnModel.changePickupStatus(status, returnId);
            if(orderResponse == null){
                FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,"Server could not able to respond, please contact engineering CRM team",""));
                
            } else if(orderResponse.getStatus().getStatusType().equalsIgnoreCase("SUCCESS")){
                if(orderResponse.getStatus().getStatusCode() == 1090){
                    if(status.equalsIgnoreCase("approve")){                        
                        FacesContext.getCurrentInstance().addMessage(null, 
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Pickup Approved", "pickup is initiated from on-hold"));
                        
                    }
                    if(status.equalsIgnoreCase("reject")){
                        FacesContext.getCurrentInstance().addMessage(null, 
                            new FacesMessage(FacesMessage.SEVERITY_INFO,"Pickup Rejected", "pickup is rejected from on-hold"));
                    }
                    
                }
                
            } else if(orderResponse.getStatus().getStatusType().equalsIgnoreCase("ERROR")){                
                switch(orderResponse.getStatus().getStatusCode()){
                    case 929 : 
                        FacesContext.getCurrentInstance().addMessage(null, 
                            new FacesMessage(FacesMessage.SEVERITY_WARN,"No change in pickup Status", "The current status is already same as the one you are trying to set"));
                        break;
                        
                    case 930 :
                        FacesContext.getCurrentInstance().addMessage(null, 
                            new FacesMessage(FacesMessage.SEVERITY_WARN,"No change in pickup Status", "something has gone wrong, please contact engineering"));
                        break;                       
                        
                }
            }
            
        } catch (ERPServiceException ex) {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,ex.getMessage(),""));        
            
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_FATAL,ex.getMessage(),""));        
        }
        
    }
}