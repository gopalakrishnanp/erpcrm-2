package com.myntra.erp.crm.oneview.utils;

import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnCommentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.oneview.model.OrderModel;
import com.myntra.portal.crm.client.entry.SREntry;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 *
 * @author pravin
 */
@ManagedBean(name = "utility")
public class Utility {

    
    private static SimpleDateFormat dateFormatddMMMyyyy = new SimpleDateFormat("dd MMM, yyyy");
    private static SimpleDateFormat dateFormatddMMMMyyyHHmmaE = new SimpleDateFormat("dd-MMMM-yyyy hh:mm a (E)");

    /*
    private static HashMap<String, Integer> openStatus;

    static {
        openStatus = new HashMap<String, Integer>();
        openStatus.put("WP", 1);
        openStatus.put("Q", 1);
        openStatus.put("OH", 1);
        openStatus.put("SH", 1);
        openStatus.put("PK", 1);
    }
    */

    public static boolean isNumeric(String s) {
        String check = s.trim();
        for (int i = 0; i < check.length(); i++) {
            if (check.charAt(i) >= '0' && check.charAt(i) <= '9') {
            } else {
                return false;
            }
        }

        return true;
    }

    /*public static String getOpenAge(CustomerOrderEntry orderEntry) {
        List<CustomerOrderShipmentEntry> orderShipments = orderEntry.getOrderShipments();
        if (orderShipments != null) {
            for (CustomerOrderShipmentEntry shipment : orderShipments) {
                String status = shipment.getStatus();
                if (openStatus.containsKey(status)) {
                    long time1 = orderEntry.getCreatedOn().getTime();
                    Date d = new Date();
                    long time2 = d.getTime();

                    int hoursOpen = (int) ((time2 - time1) / 3600000);
                    
                    if(hoursOpen==0) {
                        int minutesOpen = (int) ((time2 - time1) / 60000);
                        return minutesOpen + " Mins";
                    } else {
                        return hoursOpen + " Hrs";
                    }
                }
            }
        }

        return "-";
    }*/
    
    public static String getDaysLeftForReturn(CustomerOrderShipmentEntry shipmentEntry) {
        
        if(shipmentEntry==null || shipmentEntry.getDeliveredOn()==null) {
            return "";
        }
        
        if(shipmentEntry.getStatus().equals("C") || shipmentEntry.getStatus().equals("DL")) {
            // Do nothing.
        } else {
            return "Order Status should be <br/>Delivered or Completed";
        }
        
        long time1 = shipmentEntry.getDeliveredOn().getTime();
        
        Calendar cal = Calendar.getInstance();  
        cal.setTime(shipmentEntry.getDeliveredOn());  
        cal.add(Calendar.DATE, 30); // add 30 days  
  
        long thirtyDaysAfterDeliveryDate = cal.getTime().getTime();
        
        //long thirtyDaysAfterDeliveryDate = time1 + (30 * 24 * 60 * 60 * 1000); // 30days. 1day=3600mins. 1min=1000ms
        
        long time2 = (new Date()).getTime();
        
        if(thirtyDaysAfterDeliveryDate<=time2) {
            int daysCompleted = (int) ((time2 - time1) / (24 * 60 * 60 * 1000));
            return "Cannot Return.<br/>" + daysCompleted + " days elapsed since Delivered date.";
        }
        
        int daysRemaining = (int) ((thirtyDaysAfterDeliveryDate - time2) / (24 * 60 * 60 * 1000));
        
        if(daysRemaining>0) {
            return "Return Allowed<br/>" + daysRemaining + " Days remaining";
        } else {
            int hoursRemaining = (int) ((thirtyDaysAfterDeliveryDate - time2) / (60 * 60 * 1000));
            if(hoursRemaining>0) {
                return "Return Allowed<br/>" + hoursRemaining + " Hours remaining";
            } else {
                int minutesRemaining = (int) ((thirtyDaysAfterDeliveryDate - time2) / (60 * 1000));
                return "Return Allowed<br/>" + minutesRemaining + " Minutes remaining";
            }
        } 
    }

    public String getDeliveryStatus(CustomerOrderEntry orderEntry) {
        String deliveryStatus = "";

        HashMap<String, Integer> shipmentStatusCount = new LinkedHashMap<String, Integer>();

        List<CustomerOrderShipmentEntry> orderShipments = orderEntry.getOrderShipments();
        if (orderShipments != null) {
            for (CustomerOrderShipmentEntry shipment : orderShipments) {
                String status = shipment.getStatusDisplayName();
                Integer count = shipmentStatusCount.get(status);

                if (count == null) {
                    count = 1;
                } else {
                    count = count + 1;
                }
                shipmentStatusCount.put(status, count);
            }

            boolean isFirst = true;
            for (Map.Entry<String, Integer> entry : shipmentStatusCount.entrySet()) {
                if (isFirst == false) {
                    deliveryStatus = deliveryStatus + " \n ";
                }
                deliveryStatus = deliveryStatus + entry.getValue() + " " + entry.getKey();
                isFirst = false;
            }
        }

        return deliveryStatus;
    }
    
    
    // do not suffix size to any image
    @Deprecated
    public static String get150by200ImageURL(String imageURL) {
        if(imageURL!=null) {
            int extensionIndex = imageURL.lastIndexOf('.');
            if(extensionIndex<0) {
                return imageURL;
            }
            
            String imageURL150by200 = imageURL.substring(0,extensionIndex)
                    .concat("_150_200")
                    .concat(imageURL.substring(extensionIndex));
            
            return imageURL150by200;
        }
        return imageURL;
    }
    
    public static String getCouponDescription(String couponType, Double minimum, Double mrpAmount, Double mrpPercentage) {
        String couponDescription = "";
        
        if(couponType.equals("absolute")) {
            couponDescription = "Rs. " + Math.round(mrpAmount) + " off";
        } else if (couponType.equals("percentage")) {
            couponDescription = mrpPercentage + "% off";
        } else if (couponType.equals("dual")) {
            couponDescription = mrpPercentage + "% off upto " + Math.round(mrpAmount);
        }
        
        if(minimum>0) {
            couponDescription+=" on a minimum purchase of Rs. " + Math.round(minimum);
        }
        
        return couponDescription;
    }
    
    public static String dateFormat(Date date) {
        return dateFormatddMMMyyyy.format(date);
    }
    
    public static String dateFormatWithRemaining(Date date) {
        String dateToDisplay = dateFormatddMMMyyyy.format(date);
        Days daysBetween = Days.daysBetween(LocalDate.fromDateFields(new Date()), LocalDate.fromDateFields(date));
        return dateToDisplay + " (" + daysBetween.getDays() + " Days to Expiry)";
    }
    
    public static Double getRounded(Double value) {
        if(value!=null) {
            double finalValue = (double) Math.round( value * 100.0 ) / 100.0;
            return finalValue;
        }
        return value;
    }
    
    public static Float getRounded(Float value) {        
        if(value!=null) {
            float finalValue = (float) (Math.round( value * 100.0 ) / 100.0);
            return finalValue;
        }
        return value;
    }
    
    public static Integer getIntValue(Float value) {
        return (value != null) ? value.intValue() : null;
    }
    
    public static String getFormattedDate(Date date) {
        if(date!=null) {
            String s = dateFormatddMMMMyyyHHmmaE.format(date);
            return s;
        } else {
            return "";
        }
    }
    
    public static void fixSRSummary(List<SREntry> srEntryList) {
        for(SREntry srEntry: srEntryList) {
            fixSRSummary(srEntry);
        }
    }
    
    public static void fixSRSummary(SREntry srEntry) {
        srEntry.setCreateSummary(getFormattedSummary(srEntry.getCreateSummary()));        
    }
    
    public static void fixReturnComment(List<CustomerReturnEntry> returnEntryList) {
        for(CustomerReturnEntry returnEntry: returnEntryList) {
            fixReturnComment(returnEntry);
        }
    }
    
    public static void fixReturnComment(CustomerReturnEntry returnEntry) {        
        List<CustomerReturnCommentEntry> returnCommentEntryList = returnEntry.getReturnCommentEntry();
        for(CustomerReturnCommentEntry returnCommentEntry: returnCommentEntryList){
            returnCommentEntry.setReturnCommentDescription(getFormattedSummary(returnCommentEntry.getReturnCommentDescription()));
        }
    }
    
    public static String getFormattedSummary(String srSummary) {
        if(srSummary!=null) {
            return StringEscapeUtils.unescapeHtml(srSummary);
        } else {
            return null;
        }
    }
    
    public static String getReturnCancelExchangeStyle(OrderModel orderModel, CustomerOrderItemEntry entry) {
        // the order of the condition is important here
        if(entry.getStatus().equals("IC")) {
            return "cancelledItem";
        } else if(orderModel.isReturned(entry.getId())) {
            return "returnedItem";
        } else if(entry.getExchangeOrderId()!=null || entry.getExchangeLineId()!=null) {
            return "exchangedItem";
        }
        return "";
    }
    
    public static String urlencode(String str) {
        if(str!=null && !str.trim().equals("")) {
            return URLEncoder.encode(str).replaceAll("\\+","%20");
        } else {
           return str;
        }
    }
    
    public static String newline2br(String text) {
        if(text!=null) {
            String newText = text.replaceAll("(\r\n|\n\r|\r|\n)", "<br/>");
            return newText;
        }
    
        return null;
    }
        
}
