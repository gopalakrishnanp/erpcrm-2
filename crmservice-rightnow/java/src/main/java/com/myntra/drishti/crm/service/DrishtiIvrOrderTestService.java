package com.myntra.drishti.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.drishti.crm.client.entry.DrishtiOrderEntry;
import com.myntra.drishti.crm.client.response.DrishtiIvrOrderTestResponse;

@Path("/drishti-ivr-test/")
public interface DrishtiIvrOrderTestService extends BaseService<DrishtiIvrOrderTestResponse, DrishtiOrderEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/")
	AbstractResponse getIvrOrderTestDetailsForDrishti(@DefaultValue("1") @QueryParam("count") int count,
			@DefaultValue("null") @QueryParam("date") String date) throws ERPServiceException;

}
