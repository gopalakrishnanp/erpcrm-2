package com.myntra.rightnow.crm.manager;

import java.util.List;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.rightnow.crm.client.entry.RightnowReportEntry;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;

/**
 * Manager interface(abstract) for rightnow reports
 * 
 * @author Pravin Mehta
 */
public interface RightnowReportManager extends BaseManager<RightnowReportResponse, RightnowReportEntry> {
	public RightnowReportResponse getReportData(String reportName, List<RightnowReportFilterEntry> filters)
			throws ERPServiceException;

	public RightnowReportResponse getReportDataOnDaterange(String reportName, String startDate, String endDate)
			throws ERPServiceException;

}