package com.myntra.rightnow.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

/**
 * Customer order search web service interface(abstract) which integrates detail
 * of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public interface RightnowOrderService extends BaseService<CustomerOrderResponse, CustomerOrderEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/order")
	AbstractResponse getOrderDetails(@QueryParam("orderid") long orderid)
			throws ERPServiceException;
	
	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/shipment")
	AbstractResponse getShipmentDetails(@QueryParam("shipmentid") long shipmentid)
			throws ERPServiceException;
}
