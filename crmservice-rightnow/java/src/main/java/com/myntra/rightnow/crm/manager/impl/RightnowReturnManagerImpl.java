package com.myntra.rightnow.crm.manager.impl;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.CustomerReturnClient;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.rightnow.crm.manager.RightnowReturnManager;

public class RightnowReturnManagerImpl extends BaseManagerImpl<CustomerReturnResponse, CustomerReturnEntry> implements
		RightnowReturnManager {

	@Override
	public CustomerReturnResponse getReturnDetail(Long returnId, Long orderId, String login,
			boolean isLogisticDetailNeeded) throws ERPServiceException {

		// TODO : not giving an option of getting return on login
		// for the time being
		CustomerReturnResponse returnResponse = CustomerReturnClient.getCustomerReturnDetail(null, returnId, orderId,
				null, isLogisticDetailNeeded);
		return returnResponse;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(CustomerReturnEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(CustomerReturnEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
