package com.myntra.rightnow.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.rightnow.crm.manager.RightnowOrderManager;
import com.myntra.rightnow.crm.service.RightnowOrderService;

/**
 * Customer order search web service implementation which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class RightnowOrderServiceImpl extends BaseServiceImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		RightnowOrderService {

	@Override
	public AbstractResponse getOrderDetails(long orderid) {
		try {
			return ((RightnowOrderManager) getManager()).getOrderDetails(orderid);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
	
	@Override
	public AbstractResponse getShipmentDetails(long shipmentid) {
		try {
			return ((RightnowOrderManager) getManager()).getShipmentDetails(shipmentid);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}
