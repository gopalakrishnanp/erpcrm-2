package com.myntra.rightnow.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

/**
 * Manager interface(abstract) for customer order service which integrates
 * detail of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public interface RightnowOrderManager extends BaseManager<CustomerOrderResponse, CustomerOrderEntry> {

	public CustomerOrderResponse getOrderDetails(long orderid) throws ERPServiceException;
	
	public CustomerOrderResponse getShipmentDetails(long shipmentid) throws ERPServiceException;

}