package com.myntra.rightnow.crm.constant;

public interface RightnowConstants {

	public interface NAMED_QUERIES {

		public static final String WELCOME_CALL_LOG_BY_RELEASE = "WELCOME_CALL_LOG_BY_RELEASE";
		public static final String TOTAL_WELCOME_CALL_BY_CREATE_DATE = "TOTAL_WELCOME_CALL_BY_CREATE_DATE";
	}

	public interface SQL_RESULT_SET_MAP {

		public static final String MAP_WELCOME_CALL_LOG_BY_RELEASE = "MAP_WELCOME_CALL_LOG_BY_RELEASE";
		public static final String MAP_TOTAL_WELCOME_CALL_BY_CREATE_DATE = "MAP_TOTAL_WELCOME_CALL_BY_CREATE_DATE";
	}
}