package com.myntra.rightnow.crm.base;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.impl.OMNamespaceImpl;
import org.apache.axiom.om.impl.llom.OMElementImpl;
import org.apache.axiom.soap.impl.llom.soap11.SOAP11Factory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.ws.security.WSConstants;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.utils.Misc;
import com.myntra.rightnow.crm.client.code.RightnowErrorCodes;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.constants.RightnowConstants;
import com.rightnow.ws.base.ID;
import com.rightnow.ws.base.NamedID;
import com.rightnow.ws.messages.CSVTable;
import com.rightnow.ws.messages.CSVTableSet;
import com.rightnow.ws.messages.CSVTables;
import com.rightnow.ws.messages.ClientInfoHeader;
import com.rightnow.ws.messages.RunAnalyticsReportResponseMsg;
import com.rightnow.ws.objects.AnalyticsReport;
import com.rightnow.ws.objects.AnalyticsReportFilter;
import com.rightnow.ws.wsdl.RequestErrorFault;
import com.rightnow.ws.wsdl.RightNowSyncService;
import com.rightnow.ws.wsdl.RightNowSyncServiceStub;
import com.rightnow.ws.wsdl.ServerErrorFault;
import com.rightnow.ws.wsdl.UnexpectedErrorFault;

/////////////////////////////////////////////////////////////////////////////
// Copyright 2006-2012, Oracle and/or its affiliates. All rights reserved.
// Sample code provided for training purposes only. This sample code is
// provided "as is" with no warranties of any kind express or implied.
/////////////////////////////////////////////////////////////////////////////

public class BasicRunAnalyticsReport 
{

	RightNowSyncService _service;	
	
	public BasicRunAnalyticsReport() throws AxisFault 
	{
		_service = new RightNowSyncServiceStub();
		ServiceClient serviceClient = ((org.apache.axis2.client.Stub)_service)._getServiceClient();
		serviceClient.addHeader(createSecurityHeader(RightnowConstants.RIGHTNOW_USERNAME, RightnowConstants.RIGHTNOW_PASSWORD));
		serviceClient.getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, 300000);
		serviceClient.getOptions().setProperty(HTTPConstants.SO_TIMEOUT, 300000);
	}
	
	private OMElement createSecurityHeader(String username, String password)   
	{   
	        OMNamespaceImpl wsseNS = new OMNamespaceImpl(WSConstants.WSSE_NS, WSConstants.WSSE_PREFIX);   
	        OMFactory factory = new SOAP11Factory();   
	        OMElementImpl securityHeader;   
	        OMElementImpl usernameTokenElement;   
	        OMElementImpl usernameElement;   
	        OMElementImpl passwordElement;   
	           
	        // create the Security header block   
	        securityHeader = new OMElementImpl("Security", wsseNS, factory);   
	        securityHeader.addAttribute("mustUnderstand", "1", null);   
	           
	        // nest the UsernameToken in the Security header   
	        usernameTokenElement = new OMElementImpl(WSConstants.USERNAME_TOKEN_LN, wsseNS, securityHeader, factory);   
	           
	        // nest the Username and Password elements   
	        usernameElement = new OMElementImpl(WSConstants.USERNAME_LN, wsseNS, usernameTokenElement, factory);   
	        usernameElement.setText(username);   
	           
	        passwordElement = new OMElementImpl(WSConstants.PASSWORD_LN, wsseNS, usernameTokenElement, factory);   
	        passwordElement.setText(password);   
	        passwordElement.addAttribute(WSConstants.PASSWORD_TYPE_ATTR,   
	        WSConstants.PASSWORD_TEXT, null);   
	           
	        return securityHeader;   
	}
	
	public CSVTable runAnalyticsReport(String reportName, List<RightnowReportFilterEntry> filterEntries, int start, int limit) throws ERPServiceException
	{	
		//Create new AnalyticsReport Object
		AnalyticsReport analyticsReport = new AnalyticsReport();
		//create limit and start parameters. Specifies the max number of rows to return (10,000 is the overall maximum)
		//start specifies the starting row
		//int limit = 10000;
		//int start = 0;
		if(limit == 0)
			limit = 10000;

		
		 if(filterEntries != null && filterEntries.size() > 0) {
			List<AnalyticsReportFilter> filters = new ArrayList<AnalyticsReportFilter>();
			for(RightnowReportFilterEntry filterEntry : filterEntries) {
				AnalyticsReportFilter filter = new AnalyticsReportFilter();
				
				//set datatype
				if(filterEntry.getDataType() != 0) {
					NamedID datatype = new NamedID();
					ID dtId = new ID();
					dtId.setId(filterEntry.getDataType());
					datatype.setID(dtId);
					filter.setDataType(datatype);
				}
                
                //set filter name
				filter.setName(filterEntry.getProperty());
				
				//set operator
				if (filterEntry.getOperator() != 0) {
					NamedID namedID = new NamedID();
					ID id = new ID();
					id.setId(filterEntry.getOperator());
					namedID.setID(id);
					filter.setOperator(namedID);
				}
				
				//set prompt
				filter.setPrompt(filterEntry.getProperty());
				
				//set value
                filter.setValues(filterEntry.getValues());
				filters.add(filter);
			}
			analyticsReport.setFilters(filters.toArray(new AnalyticsReportFilter[filters.size()]));
		}

		analyticsReport.setName(reportName);
		
		try 
		{			
			ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
			clientInfoHeader.setAppID("Run report sample");

			CSVTableSet thisset = new CSVTableSet();
			RunAnalyticsReportResponseMsg msg  = _service.runAnalyticsReport(analyticsReport, limit, start, RightnowConstants.RIGHTNOW_REPORT_DELIMITER, false, true, clientInfoHeader);
			thisset = msg.getCSVTableSet();

			CSVTables tables = thisset.getCSVTables();
			CSVTable[] tableArray = tables.getCSVTable();
			
			return tableArray[0];
		} 
		catch (RemoteException e) 
		{
			e.printStackTrace();
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_REPORT_FETCHED, new String[] {e.getMessage()});
		} 
		catch (ServerErrorFault e) 
		{
			e.printStackTrace();
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_REPORT_FETCHED, new String[] {e.getMessage()});
		} 
		catch (RequestErrorFault e) 
		{
			e.printStackTrace();
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_REPORT_FETCHED, new String[] {e.getMessage()});
		} 
		catch (UnexpectedErrorFault e) 
		{
			e.printStackTrace();
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_REPORT_FETCHED, new String[] {e.getMessage()});
		}
		catch(Exception e) {
			e.printStackTrace();
			throw Misc.createERPServiceException(RightnowErrorCodes.NO_REPORT_FETCHED, new String[] {e.getMessage()});
		}
	}
	
	public static void main(String[] args)
	{
		try
		{
			BasicRunAnalyticsReport basicRunReport = new BasicRunAnalyticsReport();
			List<RightnowReportFilterEntry> filterEntryList = new ArrayList<RightnowReportFilterEntry>();
			RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
			filterEntry.setProperty("Email ID");
			filterEntryList.add(filterEntry);
			filterEntry.setValues(new String[]{"'pravin.mehta@myntra.com','arun.kumar@myntra.com'"});
			basicRunReport.runAnalyticsReport("engg_contact_email", filterEntryList, 0, 0);
		}
		catch (AxisFault e)
		{
			System.out.println("Axis exception encountered: " + e.getMessage());
			e.printStackTrace();
		}	
		catch (Exception e)
		{
			System.out.println("Unexpected esception: " + e.getMessage());
		}
	}
}
