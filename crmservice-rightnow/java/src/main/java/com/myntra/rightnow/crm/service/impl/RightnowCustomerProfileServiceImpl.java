package com.myntra.rightnow.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;
import com.myntra.rightnow.crm.manager.RightnowCustomerProfileManager;
import com.myntra.rightnow.crm.service.RightnowCustomerProfileService;

public class RightnowCustomerProfileServiceImpl extends BaseServiceImpl<CustomerProfileResponse, CustomerProfileEntry> implements
		RightnowCustomerProfileService {

	@Override
	public AbstractResponse getCustomerProfileByLogin(String login) {
		try {
			return ((RightnowCustomerProfileManager) getManager()).getCustomerProfileByLogin(login);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}

	@Override
	public AbstractResponse createCustomerProfileInRightnow(String email)
			throws ERPServiceException {
		try {
			return ((RightnowCustomerProfileManager) getManager()).createCustomerProfileInRightnow(email);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}
	}
}
