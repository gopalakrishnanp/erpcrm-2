package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.ReturnEntry;
import com.myntra.portal.crm.client.response.ReturnResponse;

/**
 * Customer return web service interface(abstract) which integrates detail of
 * return and comments
 * 
 * @author Arun Kumar
 */
@Path("/return/")
public interface ReturnService extends BaseService<ReturnResponse, ReturnEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getReturnDetail(@QueryParam("returnId") Long returnId, @QueryParam("orderId") Long orderId,
			@QueryParam("login") String login) throws ERPServiceException;
}
