/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.manager;

import org.springframework.transaction.annotation.Transactional;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;

/**
 *
 * @author pravin
 */
public interface CustomerProfileManager extends BaseManager<CustomerProfileResponse, CustomerProfileEntry>{
    public CustomerProfileResponse getCustomerProfileByLogin(String login) throws ERPServiceException;
    
    public CustomerProfileResponse getCustomerProfileByMobile(String mobile) throws ERPServiceException;
    
    @Transactional(rollbackFor=Exception.class)
    public CustomerProfileResponse createCustomerProfile() throws ERPServiceException;
}
