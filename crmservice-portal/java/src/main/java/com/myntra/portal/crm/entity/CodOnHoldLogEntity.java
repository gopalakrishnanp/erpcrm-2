package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for COD order oh reason comments
 * 
 * @author Arun Kumar
 */
@Entity
@NamedNativeQuery(
	name = CRMPortalServiceConstants.NAMED_QUERIES.COD_ORDER_OH_LOG_BY_ORDERID, 
	query = "select * " +	
		//" orderid,disposition,reason,reason_display_name,trial_number,created_by,comment,created_time" +		
		" from" +
		" (select " +
		" orderid,disposition,d.reason,r.reason_display_name,trial_number,created_by,comment,created_time" +	
		" from cod_oh_orders_dispostions d" +
		" inner join mk_order_action_reasons r on r.reason=d.reason" +
		" where orderid=:orderId" +
		" union all" +
		" select" +
		" orderid,disposition,dl.reason,r.reason_display_name,trial_number,created_by,comment,created_time" +
		" from cod_oh_orders_dispostions_log dl" +
		" inner join mk_order_action_reasons r on r.reason=dl.reason" +
		" where orderid=:orderId" +
		" ) cod_oh_reason order by created_time desc", 
	resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_COD_ORDER_OH_LOG_BY_ORDERID
)

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_COD_ORDER_OH_LOG_BY_ORDERID, 
	columns = {
		@ColumnResult(name = "orderid"), @ColumnResult(name = "disposition"),
		@ColumnResult(name = "reason"), @ColumnResult(name = "reason_display_name"),
		@ColumnResult(name = "trial_number"), @ColumnResult(name = "created_by"),
		@ColumnResult(name = "comment"),	@ColumnResult(name = "created_time"),		
	}
)

public class CodOnHoldLogEntity extends BaseEntity {

	private static final long serialVersionUID = 3688398701763484215L;

	@Column(name = "orderid")
	private Long orderId;

	@Column(name = "disposition")
	private String disposition;

	@Column(name = "reason")
	private String reason;

	@Column(name = "reason_display_name")
	private String reasonDisplayName;

	@Column(name = "trial_number")
	private Integer trialNumber;	

	@Column(name = "comment")
	private String comment;

	@Column(name = "created_time")
	private Date createdDate;

	public CodOnHoldLogEntity(Long orderId, String disposition, String reason, String reasonDisplayName,
			Integer trialNumber, String createdBy, String comment, Date createdDate) {		
		this.orderId = orderId;
		this.disposition = disposition;
		this.reason = reason;
		this.reasonDisplayName = reasonDisplayName;
		this.trialNumber = trialNumber;
		this.createdBy = createdBy;
		this.comment = comment;
		this.createdDate = createdDate;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getDisposition() {
		return disposition;
	}

	public String getReason() {
		return reason;
	}

	public String getReasonDisplayName() {
		return reasonDisplayName;
	}

	public Integer getTrialNumber() {
		return trialNumber;
	}

	public String getComment() {
		return comment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setReasonDisplayName(String reasonDisplayName) {
		this.reasonDisplayName = reasonDisplayName;
	}

	public void setTrialNumber(Integer trialNumber) {
		this.trialNumber = trialNumber;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "CodOnHoldLogEntity [orderId=" + orderId + ", disposition=" + disposition + ", reason=" + reason
				+ ", reasonDisplayName=" + reasonDisplayName + ", trialNumber=" + trialNumber + ", comment=" + comment
				+ ", createdDate=" + createdDate + "]";
	}
	

		
}