package com.myntra.portal.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * Manager interface(abstract) for customer order payment log service
 * 
 * @author Arun Kumar
 */
public interface OrderPaymentLogManager extends BaseManager<OrderPaymentLogResponse, OrderPaymentLogEntry> {

	public OrderPaymentLogResponse getOrderPaymentLog(Long orderId) throws ERPServiceException;
}