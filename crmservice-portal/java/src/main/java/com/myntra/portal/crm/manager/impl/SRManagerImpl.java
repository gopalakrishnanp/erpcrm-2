package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.SREntry;
import com.myntra.portal.crm.client.response.SRResponse;
import com.myntra.portal.crm.dao.SRDAO;
import com.myntra.portal.crm.entity.SREntity;
import com.myntra.portal.crm.manager.SRManager;
import com.myntra.portal.crm.util.WebserviceUtil;

/**
 * Manager implementation for SR
 * 
 * @author Arun Kumar
 */
public class SRManagerImpl extends BaseManagerImpl<SRResponse, SREntry> implements SRManager {

	private static final Logger LOGGER = Logger.getLogger(SRManagerImpl.class);

	private SRDAO SRDAO;

	public SRDAO getSRDAO() {
		return SRDAO;
	}

	public void setSRDAO(SRDAO SRDAO) {
		this.SRDAO = SRDAO;
	}

	@Override
	public SRResponse getSRDetail(Long id, String loginOrMobile, String filterType) throws ERPServiceException {

		if (!filterType.equalsIgnoreCase("order") && !filterType.equalsIgnoreCase("SR")
				&& !filterType.equalsIgnoreCase("loginOrMobile")) {
			throw new ERPServiceException("Wrong filter chosen for SR retrieval", 0);
		}
		if (id == null && filterType.equalsIgnoreCase("order")) {
			throw new ERPServiceException("Order id is null, please provide order id", 0);
		}
		if (id == null && filterType.equalsIgnoreCase("SR")) {
			throw new ERPServiceException("SR id is null, please provide SR id", 0);
		}
		if (loginOrMobile == null && filterType.equalsIgnoreCase("loginOrMobile")) {
			throw new ERPServiceException("Login or mobile is null, please provide email or mobile", 0);
		}

		// Initialise response and entry
		SRResponse response = new SRResponse();
		List<SREntity> srEntityList = null;
		List<SREntry> srEntryList = new ArrayList<SREntry>();
		;

		try {

			if (loginOrMobile == null) {
				srEntityList = (List<SREntity>) ((SRDAO) getSRDAO()).getSRDetail(id, null, filterType);

			} else {

				if (WebserviceUtil.isNumeric(loginOrMobile)) {
					filterType = "mobile";
					srEntityList = (List<SREntity>) ((SRDAO) getSRDAO()).getSRDetail(null, loginOrMobile, filterType);

				} else {
					filterType = "login";
					srEntityList = (List<SREntity>) ((SRDAO) getSRDAO()).getSRDetail(null, loginOrMobile, filterType);
				}

			}

			if (srEntityList != null) {
				for (SREntity singleSREntity : srEntityList) {
					SREntry singleSREntry = toSREntry(singleSREntity);
					srEntryList.add(singleSREntry);
				}
			}

			response.setSREntryList(srEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		// Response with success status
		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.SERVICE_REQUEST_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getSREntryList().size());
		response.setStatus(success);
		return response;
	}

	private SREntry toSREntry(SREntity sre) {

		if (sre == null) {
			return null;
		}

		return new SREntry(sre.getCustomerEmail(), sre.getOrderId(), sre.getSRId(), sre.getCategoryId(),
				sre.getCategory(), sre.getSubCategoryId(), sre.getSubCategory(), sre.getPriorityId(),
				sre.getPriority(), sre.getStatusId(), sre.getStatus(), sre.getDepartment(), sre.getChannel(),
				sre.getCallBackDate(), sre.getCreateDate(), sre.getDueDate(), sre.getCloseDate(), sre.getTAT(),
				sre.getReporter(), sre.getAssignee(), sre.getUpdatedBy(), sre.getDeletedBy(), sre.getResolver(),
				sre.getCreateSummary(), sre.getCloseSummary(), sre.getNonOrderSRId(), sre.getOrderSRId());

	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse create(SREntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(SREntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}