package com.myntra.portal.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.CouponEntry;
import com.myntra.portal.crm.client.response.CouponResponse;
import com.myntra.portal.crm.dao.CouponDAO;
import com.myntra.portal.crm.entity.CouponEntity;
import com.myntra.portal.crm.manager.CouponManager;

/**
 * 
 * @author Pravin Mehta
 */
public class CouponManagerImpl extends BaseManagerImpl<CouponResponse, CouponEntry> implements CouponManager {

	private static final Logger LOGGER = Logger.getLogger(CouponManagerImpl.class);

	private CouponDAO couponDAO;

	public CouponDAO getCouponDAO() {
		return couponDAO;
	}

	public void setCouponDAO(CouponDAO CouponDAO) {
		this.couponDAO = CouponDAO;
	}

	@Override
	public CouponResponse getCouponForLogin(String login) throws ERPServiceException {
		CouponResponse response = new CouponResponse();

		List<CouponEntry> retList = new ArrayList<CouponEntry>();

		try {
			List<CouponEntity> list = (List<CouponEntity>) ((CouponDAO) getCouponDAO()).getCouponForLogin(login);

			if (list != null) {
				for (CouponEntity entity : list) {
					CouponEntry entry = toCouponEntry(entity);
					retList.add(entry);
				}
			}
			response.setCouponEntryList(retList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		// return response with status
		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.COUPON_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCouponEntryList().size());
		response.setStatus(success);

		return response;
	}

	private CouponEntry toCouponEntry(CouponEntity cpe) {
		if (cpe == null) {
			return null;
		}

		return new CouponEntry(cpe.getCoupon(), cpe.getMinimum(), cpe.getTimes(), cpe.getPer_user(),
				cpe.getTimes_used(), cpe.getCouponType(), cpe.getMrpAmount(), cpe.getMrpPercentage(), cpe.getExpire(),
				cpe.getStatus(), cpe.getStyleid(), cpe.getStartdate(), cpe.getGroupName(), cpe.getIsInfinite(),
				cpe.getMaxUsageByUser(), cpe.getIsInfinitePerUser(), cpe.getCouponStatus(), cpe.getOrderid(),
				cpe.getUseddate());
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(CouponEntry e) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CouponEntry e, Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int i, int i1, String string, String string1, String string2)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}