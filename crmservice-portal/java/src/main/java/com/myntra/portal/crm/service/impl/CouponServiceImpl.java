package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.CouponEntry;
import com.myntra.portal.crm.client.response.CouponResponse;
import com.myntra.portal.crm.manager.CouponManager;
import com.myntra.portal.crm.service.CouponService;

public class CouponServiceImpl extends BaseServiceImpl<CouponResponse, CouponEntry> implements CouponService {

    @Override
    public AbstractResponse getCouponsForLogin(String login) throws ERPServiceException {
        try {
            return ((CouponManager) getManager()).getCouponForLogin(login);
        } catch (ERPServiceException e) {
            return e.getResponse();
        } catch (TransactionSystemException ex) {
            // Spring encapsulates Application Exception into TransactionSystemException
            return ((ERPServiceException) ex.getApplicationException()).getResponse();
        }
    }

}
