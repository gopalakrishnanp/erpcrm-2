package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;

/**
 * Order comment log service interface to retrieve the detail of order comment
 * 
 * @author Arun Kumar
 */
@Path("/order/comment/log/{orderId}")
public interface OrderCommentLogService extends BaseService<CustomerOrderCommentResponse, CustomerOrderCommentEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getOrderCommentLog(@PathParam("orderId") Long orderId) throws ERPServiceException;
}
