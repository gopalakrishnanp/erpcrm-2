package com.myntra.portal.crm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import com.myntra.commons.entities.BaseEntity;
import com.myntra.portal.crm.constant.CRMPortalServiceConstants;

/**
 * Entity for service request detail
 * 
 * @author Arun Kumar
 */
@Entity
@NamedNativeQueries({
	@NamedNativeQuery(
		name = CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_ORDERID, 
		query = "select " +
				"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +						
				"nosr.id as nonOrderSRId, " +
				"osr.id as orderSRId, " +
				"osr.order_id as orderid, " +
				"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
				"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
				"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
				"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
				"sr.TAT, sr.reporter, sr.assignee, " +
				"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
				"from mk_sr_tracker sr  " +
				"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
				"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +						
				"left join mk_sr_categories c on c.id=sr.category_id " +
				"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
				"left join mk_sr_priority p on p.id=sr.priority " +
				"left join mk_sr_status s on s.id=sr.status " +
				"WHERE sr.sr_active='Y' " +
				"AND osr.order_id = :orderId " +
				"order by created_date desc",
				 
		resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_SR_DETAIL
	),

	@NamedNativeQuery(
		name = CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_LOGIN, 
		query = "(select " +
				"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +
				"nosr.id as nonOrderSRId, " +
				"osr.id as orderSRId, " +
				"osr.order_id as orderid, " +						
				"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
				"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
				"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
				"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
				"sr.TAT, sr.reporter, sr.assignee, " +
				"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
				"from mk_sr_tracker sr  " +
				"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
				"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +						
				"left join mk_sr_categories c on c.id=sr.category_id " +
				"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
				"left join mk_sr_priority p on p.id=sr.priority " +
				"left join mk_sr_status s on s.id=sr.status " +
				"WHERE sr.sr_active='Y' " +
				"AND nosr.customer_email=:login " +
				
				") union " +
				
				"(select " +
				"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +
				"nosr.id as nonOrderSRId, " +
				"osr.id as orderSRId, " +
				"osr.order_id as orderid, " +						
				"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
				"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
				"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
				"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
				"sr.TAT, sr.reporter, sr.assignee, " +
				"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
				"from mk_sr_tracker sr  " +
				"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
				"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +						
				"left join mk_sr_categories c on c.id=sr.category_id " +
				"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
				"left join mk_sr_priority p on p.id=sr.priority " +
				"left join mk_sr_status s on s.id=sr.status " +
				"WHERE sr.sr_active='Y' " +
				"AND osr.customer_email=:login " +
				
				") order by createTime desc",
				 
		resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_SR_DETAIL
	),

	@NamedNativeQuery(
		name = CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_SRID, 
		query = "select " +
				"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +
				"nosr.id as nonOrderSRId, " +
				"osr.id as orderSRId, " +
				"osr.order_id as orderid, " +						
				"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
				"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
				"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
				"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
				"sr.TAT, sr.reporter, sr.assignee, " +
				"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
				"from mk_sr_tracker sr  " +
				"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
				"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +						
				"left join mk_sr_categories c on c.id=sr.category_id " +
				"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
				"left join mk_sr_priority p on p.id=sr.priority " +
				"left join mk_sr_status s on s.id=sr.status " +
				"WHERE sr.sr_active='Y' " +
				"AND sr.SR_id = :SRId " +
				"order by createTime desc",
		resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_SR_DETAIL
	), 
	
	@NamedNativeQuery(
			name = CRMPortalServiceConstants.NAMED_QUERIES.SR_BY_MOBILE, 
			query = "(select " +
					"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +
					"nosr.id as nonOrderSRId, " +
					"osr.id as orderSRId, " +
					"osr.order_id as orderid, " +						
					"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
					"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
					"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
					"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
					"sr.TAT, sr.reporter, sr.assignee, " +
					"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
					"from mk_sr_tracker sr  " +					
					"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
					"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +
					"left join xcart_orders o on o.orderid=osr.order_id " +
					"left join mk_sr_categories c on c.id=sr.category_id " +
					"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
					"left join mk_sr_priority p on p.id=sr.priority " +
					"left join mk_sr_status s on s.id=sr.status " +
					"WHERE sr.sr_active='Y' " +
					"AND nosr.mobile=:mobile " +
					
					") union " +
					
					"(select " +
					"IF((nosr.customer_email IS NULL OR nosr.customer_email=''),osr.customer_email,nosr.customer_email) as customerEmail, " +
					"nosr.id as nonOrderSRId, " +
					"osr.id as orderSRId, " +
					"osr.order_id as orderid, " +						
					"sr.SR_id as SRId, sr.category_id as categoryId, c.SR_category as category, sr.sub_category_id as subCategoryId, " +
					"sc.SR_subcategory as subCategory, sr.priority as priorityId , p.SR_priority as priority, sr.status as statusId, " +
					"s.SR_status as status, sr.SR_department as department, sr.SR_channel as channel, " +					
					"sr.callbacktime as callBackTime, sr.created_date as createTime,sr.due_date as dueTime, sr.closed_date as closeTime, " +
					"sr.TAT, sr.reporter, sr.assignee, " +
					"sr.updatedby as updatedBy,sr.deletedby as deletedBy, sr.resolver, sr.create_summary as createSummary, sr.close_summary as closeSummary " +						
					"from mk_sr_tracker sr  " +					
					"left join mk_non_order_sr nosr on sr.SR_id=nosr.SR_id " +
					"left join mk_order_sr osr on sr.SR_id=osr.SR_id " +
					"left join xcart_orders o on o.orderid=osr.order_id " +
					"left join mk_sr_categories c on c.id=sr.category_id " +
					"left join mk_sr_subcategories sc on sc.id=sr.sub_category_id " +
					"left join mk_sr_priority p on p.id=sr.priority " +
					"left join mk_sr_status s on s.id=sr.status " +
					"WHERE sr.sr_active='Y' " +
					"AND o.issues_contact_number=:mobile " +
					
					") order by createTime desc",
			resultSetMapping = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_SR_DETAIL
		),
})

@SqlResultSetMapping(
	name = CRMPortalServiceConstants.SQL_RESULT_SET_MAP.MAP_SR_DETAIL, 
	columns = {
			@ColumnResult(name = "customerEmail"), @ColumnResult(name = "orderId"),
			@ColumnResult(name = "SRId"), @ColumnResult(name = "categoryId"),
			@ColumnResult(name = "category"), @ColumnResult(name = "subCategoryId"),
			@ColumnResult(name = "subCategory"), @ColumnResult(name = "priorityId"),
			@ColumnResult(name = "priority"), @ColumnResult(name = "statusId"),
			@ColumnResult(name = "status"), @ColumnResult(name = "department"),
			@ColumnResult(name = "channel"), @ColumnResult(name = "callBackTime"),
			@ColumnResult(name = "createTime"), @ColumnResult(name = "dueTime"),
			@ColumnResult(name = "closeTime"), @ColumnResult(name = "TAT"),
			@ColumnResult(name = "reporter"), @ColumnResult(name = "assignee"),
			@ColumnResult(name = "updatedBy"), @ColumnResult(name = "deletedBy"),
			@ColumnResult(name = "resolver"), @ColumnResult(name = "createSummary"),
			@ColumnResult(name = "closeSummary"),@ColumnResult(name = "nonOrderSRId"),
			@ColumnResult(name = "orderSRId")
	}
)


public class SREntity extends BaseEntity {
	
	private static final long serialVersionUID = 3688398701763484215L;
	
	@Column(name = "customerEmail")
	private String customerEmail;

	@Id
	@Column(name = "orderId")
	private Long orderId;

	@Column(name = "SRId")
	private Long SRId;

	@Column(name = "categoryId")
	private Integer categoryId;

	@Column(name = "category")
	private String category;

	@Column(name = "subCategoryId")
	private Integer subCategoryId;
	
	@Column(name = "subCategory")
	private String subCategory;

	@Column(name = "priorityId")
	private Integer priorityId;
	
	@Column(name = "priority")
	private String priority;

	@Column(name = "statusId")
	private Integer statusId;

	@Column(name = "status")
	private String status;
	
	@Column(name = "department")
	private String department;

	@Column(name = "channel")
	private String channel;

	@Column(name = "callBackTime")
	private Date callBackDate;

	@Column(name = "createTime")
	private Date createDate;

	@Column(name = "dueTime")
	private Date dueDate;

	@Column(name = "closeTime")
	private Date closeDate;

	@Column(name = "TAT")
	private Integer TAT;

	@Column(name = "reporter")
	private String reporter;

	@Column(name = "assignee")
	private String assignee;

	@Column(name = "updatedBy")
	private String updatedBy;

	@Column(name = "deletedBy")
	private String deletedBy;

	@Column(name = "resolver")
	private String resolver;	

	@Column(name = "createSummary")
	private String createSummary;

	@Column(name = "closeSummary")
	private String closeSummary;

	@Column(name = "nonOrderSRId")
	private Long nonOrderSRId;
	
	@Column(name = "orderSRId")
	private Long orderSRId;
	
	public SREntity(String customerEmail, Long orderId, Long sRId, Integer categoryId, String category,
			Integer subCategoryId, String subCategory, Integer priorityId, String priority, Integer statusId,
			String status, String department, String channel, Date callBackDate, Date createDate, Date dueDate,
			Date closeDate, Integer tAT, String reporter, String assignee, String updatedBy, String deletedBy,
			String resolver, String createSummary, String closeSummary, Long nonOrderSRId, Long orderSRId) {	
		this.customerEmail = customerEmail;
		this.orderId = orderId;
		this.SRId = sRId;
		this.categoryId = categoryId;
		this.category = category;
		this.subCategoryId = subCategoryId;
		this.subCategory = subCategory;
		this.priorityId = priorityId;
		this.priority = priority;
		this.statusId = statusId;
		this.status = status;
		this.department = department;
		this.channel = channel;
		this.callBackDate = callBackDate;
		this.createDate = createDate;
		this.dueDate = dueDate;
		this.closeDate = closeDate;
		this.TAT = tAT;
		this.reporter = reporter;
		this.assignee = assignee;
		this.updatedBy = updatedBy;
		this.deletedBy = deletedBy;
		this.resolver = resolver;
		this.createSummary = createSummary;
		this.closeSummary = closeSummary;
		this.nonOrderSRId = nonOrderSRId;
		this.orderSRId = orderSRId;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public Long getOrderId() {
		return orderId;
	}

	public Long getSRId() {
		return SRId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public String getCategory() {
		return category;
	}

	public Integer getSubCategoryId() {
		return subCategoryId;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public Integer getPriorityId() {
		return priorityId;
	}

	public String getPriority() {
		return priority;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public String getStatus() {
		return status;
	}

	public String getDepartment() {
		return department;
	}

	public String getChannel() {
		return channel;
	}

	public Date getCallBackDate() {
		return callBackDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public Integer getTAT() {
		return TAT;
	}

	public String getReporter() {
		return reporter;
	}

	public String getAssignee() {
		return assignee;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public String getResolver() {
		return resolver;
	}

	public String getCreateSummary() {
		return createSummary;
	}

	public String getCloseSummary() {
		return closeSummary;
	}

	public Long getNonOrderSRId() {
		return nonOrderSRId;
	}

	public Long getOrderSRId() {
		return orderSRId;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setSRId(Long sRId) {
		this.SRId = sRId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public void setPriorityId(Integer priorityId) {
		this.priorityId = priorityId;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public void setCallBackDate(Date callBackDate) {
		this.callBackDate = callBackDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public void setTAT(Integer tAT) {
		this.TAT = tAT;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public void setResolver(String resolver) {
		this.resolver = resolver;
	}

	public void setCreateSummary(String createSummary) {
		this.createSummary = createSummary;
	}

	public void setCloseSummary(String closeSummary) {
		this.closeSummary = closeSummary;
	}

	public void setNonOrderSRId(Long nonOrderSRId) {
		this.nonOrderSRId = nonOrderSRId;
	}

	public void setOrderSRId(Long orderSRId) {
		this.orderSRId = orderSRId;
	}

	@Override
	public String toString() {
		return "SREntity [customerEmail=" + customerEmail + ", orderId=" + orderId + ", SRId=" + SRId + ", categoryId="
				+ categoryId + ", category=" + category + ", subCategoryId=" + subCategoryId + ", subCategory="
				+ subCategory + ", priorityId=" + priorityId + ", priority=" + priority + ", statusId=" + statusId
				+ ", status=" + status + ", department=" + department + ", channel=" + channel + ", callBackDate="
				+ callBackDate + ", createDate=" + createDate + ", dueDate=" + dueDate + ", closeDate=" + closeDate
				+ ", TAT=" + TAT + ", reporter=" + reporter + ", assignee=" + assignee + ", updatedBy=" + updatedBy
				+ ", deletedBy=" + deletedBy + ", resolver=" + resolver + ", createSummary=" + createSummary
				+ ", closeSummary=" + closeSummary + ", nonOrderSRId=" + nonOrderSRId + ", orderSRId=" + orderSRId
				+ "]";
	}		
}