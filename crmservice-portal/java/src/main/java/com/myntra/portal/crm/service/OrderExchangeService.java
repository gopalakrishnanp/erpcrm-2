package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.OrderExchangeEntry;
import com.myntra.portal.crm.client.response.OrderExchangeResponse;

/**
 * Customer Order Exchange services.
 * 
 * @author Pravin Mehta
 */
@Path("/order/exchange/{orderId}")
public interface OrderExchangeService extends BaseService<OrderExchangeResponse, OrderExchangeEntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	AbstractResponse getExchangeDetail(@PathParam("orderId") Long orderId) throws ERPServiceException;
}
