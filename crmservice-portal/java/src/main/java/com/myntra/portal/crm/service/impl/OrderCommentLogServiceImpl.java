package com.myntra.portal.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.portal.crm.client.entry.CustomerOrderCommentEntry;
import com.myntra.portal.crm.client.response.CustomerOrderCommentResponse;
import com.myntra.portal.crm.manager.OrderCommentLogManager;
import com.myntra.portal.crm.service.OrderCommentLogService;

/**
 * Order comment log service interface to retrieve the detail of order comment
 * 
 * @author Arun Kumar
 */
public class OrderCommentLogServiceImpl extends
		BaseServiceImpl<CustomerOrderCommentResponse, CustomerOrderCommentEntry> implements OrderCommentLogService {

	@Override
	public AbstractResponse getOrderCommentLog(Long orderId) {
		try {
			return ((OrderCommentLogManager) getManager()).getOrderCommentLog(orderId);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}
