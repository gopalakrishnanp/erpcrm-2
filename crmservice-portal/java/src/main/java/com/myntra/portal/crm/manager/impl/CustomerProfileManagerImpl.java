/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.portal.crm.manager.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.portal.crm.client.code.CRMPortalErrorCodes;
import com.myntra.portal.crm.client.code.CRMPortalSuccessCodes;
import com.myntra.portal.crm.client.entry.CustomerProfileEntry;
import com.myntra.portal.crm.client.response.CustomerProfileResponse;
import com.myntra.portal.crm.dao.CustomerProfileDAO;
import com.myntra.portal.crm.entity.CustomerProfileEntity;
import com.myntra.portal.crm.manager.CustomerProfileManager;

/**
 * 
 * @author pravin
 */
public class CustomerProfileManagerImpl extends BaseManagerImpl<CustomerProfileResponse, CustomerProfileEntry>
		implements CustomerProfileManager {

	private static final Logger LOGGER = Logger.getLogger(CustomerProfileManagerImpl.class);
	private CustomerProfileDAO customerProfileDAO;

	public CustomerProfileDAO getCustomerProfileDAO() {
		return customerProfileDAO;
	}

	public void setCustomerProfileDAO(CustomerProfileDAO customerProfileDAO) {
		this.customerProfileDAO = customerProfileDAO;
	}

	@Override
	public CustomerProfileResponse getCustomerProfileByLogin(String login) throws ERPServiceException {

		CustomerProfileResponse response = new CustomerProfileResponse();
		CustomerProfileEntry customerProfileEntry = new CustomerProfileEntry();
		List<CustomerProfileEntity> list = null;

		try {
			list = (List<CustomerProfileEntity>) ((CustomerProfileDAO) getCustomerProfileDAO())
					.getCustomerProfileByLogin(login);

			if (list != null && list.size() > 0) {
				customerProfileEntry = toCustomerProfileEntry(list.get(0));
			}
			response.setCustomerProfileEntry(customerProfileEntry);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		if (list==null || list.size() <= 0) {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
					StatusResponse.Type.SUCCESS, 0);
			response.setStatus(success);
			
		} else {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
					StatusResponse.Type.SUCCESS, 1);
			response.setStatus(success);
		}
		return response;
	}
	
	@Override
	public CustomerProfileResponse getCustomerProfileByMobile(String mobile) throws ERPServiceException {

		CustomerProfileResponse response = new CustomerProfileResponse();
		CustomerProfileEntry customerProfileEntry = new CustomerProfileEntry();
		List<CustomerProfileEntity> list = null;
		
		try {
			list = (List<CustomerProfileEntity>) ((CustomerProfileDAO) getCustomerProfileDAO())
					.getCustomerProfileByMobile(mobile);

			if (list != null && list.size() > 0) {
				customerProfileEntry = toCustomerProfileEntry(list.get(0));
			}
			response.setCustomerProfileEntry(customerProfileEntry);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		if (list==null || list.size() <= 0) {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
					StatusResponse.Type.SUCCESS, 0);
			response.setStatus(success);
			
		} else {
			StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
					StatusResponse.Type.SUCCESS, 1);
			response.setStatus(success);
		}
		return response;
	}


	private CustomerProfileEntry toCustomerProfileEntry(CustomerProfileEntity cpe) {
		if (cpe == null) {
			return null;
		}

		Date firstLogin = new Date((long)cpe.getFirstLogin()*1000);
		Date lastLogin = new Date((long)cpe.getLastLogin()*1000);
		
		return new CustomerProfileEntry(cpe.getLogin(), cpe.getGender(), cpe.getDob(), cpe.getFirstName(),
				cpe.getLastName(), cpe.getEmail(), cpe.getPhone(), cpe.getReferer(), 
				cpe.getMobile(), firstLogin, lastLogin, cpe.getStatus());
	}

	@Override
	public CustomerProfileResponse createCustomerProfile() throws ERPServiceException {
		CustomerProfileResponse response = new CustomerProfileResponse();
		try {
			boolean test = ((CustomerProfileDAO) getCustomerProfileDAO()).createCustomerProfile();
			if (test) {
				LOGGER.debug("CheckCustomer created");
			}
		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retrieving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(CRMPortalErrorCodes.ERR_RETRIEVING, StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}
		StatusResponse success = new StatusResponse(CRMPortalSuccessCodes.CUSTOMER_PROFILE_RETRIEVED,
				StatusResponse.Type.SUCCESS, 1);
		response.setStatus(success);
		return response;
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(CustomerProfileEntry e) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerProfileEntry e, Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int i, int i1, String string, String string1, String string2)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
