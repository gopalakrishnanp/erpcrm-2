package com.myntra.portal.crm.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.portal.crm.client.entry.SREntry;
import com.myntra.portal.crm.client.response.SRResponse;

/**
 * service interface to get Service Request(SR) detail
 * 
 * @author Arun Kumar
 */
@Path("/SR/")
public interface SRService extends BaseService<SRResponse, SREntry> {

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("/{SRId}")
	AbstractResponse getSRDetailById(@PathParam("SRId") Long SRId) throws ERPServiceException;

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("order/{orderId}")
	AbstractResponse getSRDetailByOrder(@PathParam("orderId") Long orderId) throws ERPServiceException;

	@GET
	@Produces({ "application/xml", "application/json" })
	@Consumes({ "application/xml", "application/json" })
	@Path("customer/{emailIdOrMobile}")
	AbstractResponse getSRDetailByLoginOrMobile(@PathParam("emailIdOrMobile") String login) throws ERPServiceException;
}