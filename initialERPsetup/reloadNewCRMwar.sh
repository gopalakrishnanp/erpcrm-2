#!/bin/bash
echo "---"
echo "--- Shutting down server "
echo "---"
/bin/sh ./bin/shutdown.sh
sleep 4
echo "---"
echo "Remove CRM Service folders and WAR files"
echo "---"
rm -rf webapps/myntra-crm-service
rm -rf webapps/myntra-crm-service.war

echo "---"
echo "Remove work / temp / logs folder contents"
echo "---"
rm -rf temp/*
rm -rf work/*
rm -rf logs/*


echo "---"
echo "Copy war from CRM directory structure"
echo "---"
cp /home/crm/webapp/target/myntra-crm-service.war webapps/.


echo "---"
echo "Restart Apache Server with new CRM warfile"
echo "---"
#/bin/sh ./bin/startup.sh
