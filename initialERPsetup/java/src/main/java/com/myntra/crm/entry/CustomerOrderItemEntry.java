package com.myntra.crm.entry;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;

/**
 * Entry for customer order search response with relevant item(sku) fields
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "orderItemEntry")
public class CustomerOrderItemEntry extends BaseEntry {

	private static final long serialVersionUID = 1;

	// unique ids
	private Long styleId;
	private Long optionId;
	private Long skuId;
	private Integer discountRuleId;
	private Integer discountRuleRevId;
	private Integer promotionId;
	private Long cancellationReasonId;

	// status/reason
	private String status;
	private String statusDisplayName;
	private Boolean isDiscountedProduct;
	private Boolean isReturnableProduct;
	private String cancellationReason;

	// qty
	private Integer quantity;
	private Integer discountedQuantity;

	// amount detail
	private Double taxRate;
	private Double taxAmount;
	private Double unitPrice;
	private Double discount;
	private Double couponDiscount;
	private Double cartDiscount;
	private Double cashRedeemed;
	private Double pgDiscount;
	private Double finalAmount;
	private Double cashbackOffered;

	// date
	private Date cancelledOn;

	// sku detail
	private String name;
	private String code;
	private String lastUser;
	private Boolean adminDisabled;
	private String description;
	private String vendorArticleNo;
	private String vendorArticleName;
	private String size;
	private Integer brandId;
	private String brandName;
	private String brandCode;
	private Integer articleTypeId;
	private String articleTypeName;
	private String articleTypeCode;
	private String remarks;
	private Boolean jitSourced;
	private String gtin;
	private Boolean enabled;
	private Long version;

	public Boolean getIsReturnableProduct() {
		return isReturnableProduct;
	}

	public void setIsReturnableProduct(Boolean isReturnableProduct) {
		this.isReturnableProduct = isReturnableProduct;
	}
	
	

	@TransformIgnoreAttribute
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getStyleId() {
		return styleId;
	}

	public void setStyleId(Long styleId) {
		this.styleId = styleId;
	}

	public Long getOptionId() {
		return optionId;
	}

	public void setOptionId(Long optionId) {
		this.optionId = optionId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	@TransformIgnoreAttribute
	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	@TransformIgnoreAttribute
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getDiscountedQuantity() {
		return discountedQuantity;
	}

	public void setDiscountedQuantity(Integer discountedQuantity) {
		this.discountedQuantity = discountedQuantity;
	}

	@TransformIgnoreAttribute
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	@TransformIgnoreAttribute
	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@TransformIgnoreAttribute
	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCartDiscount() {
		return cartDiscount;
	}

	public void setCartDiscount(Double cartDiscount) {
		this.cartDiscount = cartDiscount;
	}

	@TransformIgnoreAttribute
	public Double getCashRedeemed() {
		return cashRedeemed;
	}

	public void setCashRedeemed(Double cashRedeemed) {
		this.cashRedeemed = cashRedeemed;
	}

	@TransformIgnoreAttribute
	public Double getPgDiscount() {
		return pgDiscount;
	}

	public void setPgDiscount(Double pgDiscount) {
		this.pgDiscount = pgDiscount;
	}

	@TransformIgnoreAttribute
	public Double getFinalAmount() {
		return finalAmount;
	}

	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	@TransformIgnoreAttribute
	public Double getCashbackOffered() {
		return cashbackOffered;
	}

	public void setCashbackOffered(Double cashbackOffered) {
		this.cashbackOffered = cashbackOffered;
	}

	public Integer getDiscountRuleId() {
		return discountRuleId;
	}

	public void setDiscountRuleId(Integer discountRuleId) {
		this.discountRuleId = discountRuleId;
	}

	public Integer getDiscountRuleRevId() {
		return discountRuleRevId;
	}

	public void setDiscountRuleRevId(Integer discountRuleRevId) {
		this.discountRuleRevId = discountRuleRevId;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Boolean getIsDiscountedProduct() {
		return isDiscountedProduct;
	}

	public void setIsDiscountedProduct(Boolean isDiscountedProduct) {
		this.isDiscountedProduct = isDiscountedProduct;
	}

	@TransformIgnoreAttribute
	public Long getCancellationReasonId() {
		return cancellationReasonId;
	}

	public void setCancellationReasonId(Long cancellationReasonId) {
		this.cancellationReasonId = cancellationReasonId;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	public Date getCancelledOn() {
		return cancelledOn;
	}

	public void setCancelledOn(Date cancelledOn) {
		this.cancelledOn = cancelledOn;
	}

	@TransformIgnoreAttribute
	public String getStatusDisplayName() {
		return statusDisplayName;
	}

	public void setStatusDisplayName(String statusDisplayName) {
		this.statusDisplayName = statusDisplayName;
	}

	// sku methods
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLastUser() {
		return lastUser;
	}

	public void setLastUser(String lastUser) {
		this.lastUser = lastUser;
	}

	public Boolean getAdminDisabled() {
		return adminDisabled;
	}

	public void setAdminDisabled(Boolean adminDisabled) {
		this.adminDisabled = adminDisabled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVendorArticleNo() {
		return vendorArticleNo;
	}

	public void setVendorArticleNo(String vendorArticleNo) {
		this.vendorArticleNo = vendorArticleNo;
	}

	public String getVendorArticleName() {
		return vendorArticleName;
	}

	public void setVendorArticleName(String vendorArticleName) {
		this.vendorArticleName = vendorArticleName;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public Integer getArticleTypeId() {
		return articleTypeId;
	}

	public void setArticleTypeId(Integer articleTypeId) {
		this.articleTypeId = articleTypeId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getArticleTypeName() {
		return articleTypeName;
	}

	public void setArticleTypeName(String articleTypeName) {
		this.articleTypeName = articleTypeName;
	}

	public String getArticleTypeCode() {
		return articleTypeCode;
	}

	public void setArticleTypeCode(String articleTypeCode) {
		this.articleTypeCode = articleTypeCode;
	}

	public Boolean getJitSourced() {
		return jitSourced;
	}

	public void setJitSourced(Boolean jitSourced) {
		this.jitSourced = jitSourced;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SkuEntry [name=").append(name).append(", code=")
				.append(code).append(", lastUser=").append(lastUser)
				.append(", adminDisabled=").append(adminDisabled)
				.append(", description=").append(description)
				.append(", vendorArticleNo=").append(vendorArticleNo)
				.append(", vendorArticleName=").append(vendorArticleName)
				.append(", size=").append(size).append(", brandId=")
				.append(brandId).append(", brandName=").append(brandName)
				.append(", brandCode=").append(brandCode)
				.append(", articleTypeId=").append(articleTypeId)
				.append(", articleTypeName=").append(articleTypeName)
				.append(", articleTypeCode=").append(articleTypeCode)
				.append(", remarks=").append(remarks).append(", jitSourced=")
				.append(jitSourced).append(", gtin=").append(gtin)
				.append(", enabled=").append(enabled).append(", version=")
				.append(version).append(", getId()=").append(getId())
				.append(", getCreatedOn()=").append(getCreatedOn())
				.append(", getLastModifiedOn()=").append(getLastModifiedOn())
				.append(", getCreatedBy()=").append(getCreatedBy()).append("]");

		return "OrderLineEntry [cancellationReason=" + cancellationReason
				+ ", cancellationReasonId=" + cancellationReasonId
				+ ", cancelledOn=" + cancelledOn + ", cartDiscount="
				+ cartDiscount + ", cashRedeemed=" + cashRedeemed
				+ ", cashbackOffered=" + cashbackOffered + ", couponDiscount="
				+ couponDiscount + ", discount=" + discount
				+ ", discountRuleId=" + discountRuleId + ", discountRuleRevId="
				+ discountRuleRevId + ", discountedQuantity="
				+ discountedQuantity + ", finalAmount=" + finalAmount
				+ ", isDiscountedProduct=" + isDiscountedProduct
				+ ", isReturnableProduct=" + isReturnableProduct
				+ ", optionId=" + optionId + ", pgDiscount="
				+ pgDiscount + ", promotionId=" + promotionId + ", quantity="
				+ quantity + ", skuId=" + skuId + ", status=" + status
				+ ", statusDisplayName" + statusDisplayName + ", styleId="
				+ styleId + ", taxAmount=" + taxAmount + ", taxRate=" + taxRate
				+ ", unitPrice=" + unitPrice + ", getCreatedBy()="
				+ getCreatedBy() + ", getCreatedOn()=" + getCreatedOn()
				+ ", getId()=" + getId() + ", getLastModifiedOn()="
				+ getLastModifiedOn() + "]" + builder.toString();
	}
}