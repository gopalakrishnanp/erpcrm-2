package com.myntra.crm.client.codes;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * List of success codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMSuccessCodes extends ERPSuccessCodes {

	public static final StatusCodes CONTACT_ADDED = new CRMSuccessCodes(101,
			"CONTACT_ADDED");
	public static final StatusCodes CONTACT_DELETED = new CRMSuccessCodes(102,
			"CONTACT_DELETED");
	public static final StatusCodes CONTACT_RETRIEVED = new CRMSuccessCodes(
			103, "CONTACT_RETRIEVED");
	public static final StatusCodes CONTACT_UPDATED = new CRMSuccessCodes(104,
			"CONTACT_UPDATED");

	public static final StatusCodes ADDRESS_ADDED = new CRMSuccessCodes(105,
			"ADDRESS_ADDED");
	public static final StatusCodes ADDRESS_DELETED = new CRMSuccessCodes(106,
			"ADDRESS_DELETED");
	public static final StatusCodes ADDRESS_RETRIEVED = new CRMSuccessCodes(
			107, "ADDRESS_RETRIEVED");
	public static final StatusCodes ADDRESS_UPDATED = new CRMSuccessCodes(108,
			"ADDRESS_UPDATED");

	private static final String BUNDLE_NAME = "CRMSuccessCodes";

	public CRMSuccessCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}
