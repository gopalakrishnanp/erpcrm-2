/**
 * Copyright (c) 2011, Myntra and/or its affiliates. All rights reserved.
 * MYNTRA PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myntra.crm.entry;

import java.io.Serializable;

/**
 * @author Pravin 
 *
 */
public abstract class BaseCRMEntry implements Serializable{
	public BaseCRMEntry() {
		super();
	}
        
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BaseEntry []");
		return builder.toString();
	}
}
