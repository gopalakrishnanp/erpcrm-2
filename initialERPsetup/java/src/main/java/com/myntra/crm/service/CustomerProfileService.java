/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myntra.crm.service;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.BaseService;
import com.myntra.crm.client.response.CustomerProfileEntry;
import com.myntra.crm.client.response.CustomerProfileResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 *
 * @author pravin
 */
@Path("/customerprofile/")
public interface CustomerProfileService extends BaseService<CustomerProfileResponse, CustomerProfileEntry>{

    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    @Path("/")
    AbstractResponse getCustomerProfileByLogin(
            @QueryParam("login") String login) throws ERPServiceException;
    
    //@POST
    //@Consumes({"application/xml", "application/json"})
    //@Produces({"application/xml", "application/json"})
    //@Path("/my/create/")
    AbstractResponse createCustomerProfile() throws ERPServiceException;
}
