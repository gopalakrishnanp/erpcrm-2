package com.myntra.crm.dao.impl;

import com.myntra.commons.dao.impl.BaseDAOImpl;
import java.util.List;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.crm.dao.CustomerProfileDAO;
import com.myntra.crm.entity.CustomerProfileEntity;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Query;
import org.apache.log4j.Logger;

public class CustomerProfileDAOImpl extends BaseDAOImpl<CustomerProfileEntity> implements CustomerProfileDAO {

    @Override
    public CustomerProfileEntity findById(Long id) {
        return null;
    }

    
    @Override
    public List<CustomerProfileEntity> getCustomerProfileByLogin(String login) throws ERPServiceException {
        Query q = getEntityManager().createNamedQuery("CustomerProfileEntity.q1");
        q.setParameter("login", login);
        //Query q = entityManager.createNamedQuery("CustomerProfileEntity.q1",  CustomerProfileEntity.class);

        List<Object[]> list = q.getResultList();
        List<CustomerProfileEntity> listCustomerProfiles = new ArrayList<CustomerProfileEntity>();

        for (Object[] objArray : list) {
            // query = "SELECT login, usertype, gender, DOB, firstname, lastname, 
            // email, phone, mobile, last_login, first_login, status, referer, language

            int index=0;
            String loginField = (String) objArray[index++];
            Character gender = (Character) objArray[index++];
            Date dob = (Date) objArray[index++];
            String firstName = (String) objArray[index++];
            String lastName = (String) objArray[index++];
            String email = (String) objArray[index++];
            String phone = (String) objArray[index++];
            String mobile = (String) objArray[index++];
            Integer firstLogin = (Integer) objArray[index++];

            CustomerProfileEntity entity = new CustomerProfileEntity(
                    loginField, gender, dob, firstName, lastName, email, phone, mobile, firstLogin);
            
            listCustomerProfiles.add(entity);
            Logger.getLogger(this.getClass()).debug("CheckCustomer : " + entity.toString());
        }

        return listCustomerProfiles;
    }

    @Override
    public boolean createCustomerProfile() throws ERPServiceException {
        Query q = getEntityManager().createNativeQuery("insert into xcart_customers values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        int index=1;
        q.setParameter(index++, "athena@myntra.com");   // login
        q.setParameter(index++, "M");                   // gender
        q.setParameter(index++, new Date(2013,2,13));   // DOB
        q.setParameter(index++, "athena");              // firstname
        q.setParameter(index++, "othena");              // lastname
        q.setParameter(index++, "athena@myntra.com");   // email
        q.setParameter(index++, "0801990099");          // phone
        q.setParameter(index++, "9900990099");          // mobile
        q.setParameter(index++, "1010101001");          // first_login
      
        int p=0;
        //int p = q.executeUpdate();
        if(p>0)
            return true;
        return false;
    }
}
