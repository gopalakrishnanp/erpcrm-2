package com.myntra.crm.client.codes;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * CRM error codes
 * 
 * @author Arun Kumar
 * 
 */
public class CRMErrorCodes extends ERPErrorCodes {

	public static final StatusCodes INVALID_NAME = new CRMErrorCodes(151,
			"INVALID_NAME");
	public static final StatusCodes INVALID_ENTITY_ID = new CRMErrorCodes(152,
			"INVALID_ENTITY_ID");
	public static final StatusCodes INVALID_ENTITY_NAME = new CRMErrorCodes(
			153, "INVALID_ENTITY_NAME");

	private static final String BUNDLE_NAME = "CRMErrorCodes";

	public CRMErrorCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}
