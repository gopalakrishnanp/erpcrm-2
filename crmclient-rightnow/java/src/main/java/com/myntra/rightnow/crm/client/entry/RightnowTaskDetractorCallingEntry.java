package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "rightnowTaskDetractorCallingEntry")
public class RightnowTaskDetractorCallingEntry extends RightnowTaskOrderEntry {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "RightnowTaskDetractorCallingEntry []";
	}		

}
