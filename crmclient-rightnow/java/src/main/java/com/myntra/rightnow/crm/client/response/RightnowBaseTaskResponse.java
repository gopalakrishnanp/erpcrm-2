/**
 * 
 */
package com.myntra.rightnow.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.rightnow.crm.client.entry.RightnowBaseTaskEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskDetractorCallingEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskOrderEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskUDCallingEntry;
import com.myntra.rightnow.crm.client.entry.RightnowTaskWelcomeCallingEntry;

/**
 * @author preetam
 * 
 */
@XmlRootElement(name = "rightnowBaseTaskResponse")
public class RightnowBaseTaskResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;

	private List<RightnowBaseTaskEntry> rightnowBaseTaskEntryList;

	public RightnowBaseTaskResponse() {
	}

	public RightnowBaseTaskResponse(List<RightnowBaseTaskEntry> rightnowBaseTaskListEntry) {

		this.rightnowBaseTaskEntryList = rightnowBaseTaskListEntry;
	}

	@XmlElementWrapper(name = "rightnowBaseTaskEntryList")
	@XmlElementRefs({ @XmlElementRef(type = RightnowTaskOrderEntry.class),
			@XmlElementRef(type = RightnowTaskUDCallingEntry.class), @XmlElementRef(type = RightnowTaskDetractorCallingEntry.class),
			@XmlElementRef(type = RightnowTaskWelcomeCallingEntry.class)})
	public List<RightnowBaseTaskEntry> getRightnowBaseTaskEntryList() {
		return rightnowBaseTaskEntryList;
	}

	public void setRightnowBaseTaskEntryList(List<RightnowBaseTaskEntry> rightnowBaseTaskEntryList) {
		this.rightnowBaseTaskEntryList = rightnowBaseTaskEntryList;
	}

	@Override
	public String toString() {
		return "RightnowBaseTaskResponse [rightnowBaseTaskEntryList=" + rightnowBaseTaskEntryList + "]";
	}

}
