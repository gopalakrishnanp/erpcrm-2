package com.myntra.rightnow.crm.client.code;

import com.myntra.commons.codes.ERPSuccessCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * List of success codes
 */
public class RightnowSuccessCodes extends ERPSuccessCodes {

	
	public static final StatusCodes RIGHTNOW_REPORT_RETRIEVED = new RightnowSuccessCodes(707, "RIGHTNOW_REPORT_RETRIEVED");
	public static final StatusCodes RIGHTNOW_TASKS_FETCHED = new RightnowSuccessCodes(708, "RIGHTNOW_TASKS_FETCHED");
	public static final StatusCodes RIGHTNOW_TASKS_SAVED = new RightnowSuccessCodes(709, "RIGHTNOW_TASKS_SAVED");
	// welcome call task code
	public static final StatusCodes SUCCESS_CREATE_WELCOME_CALL_TASK = new RightnowSuccessCodes(710, "SUCCESS_CREATE_WELCOME_CALL_TASK");

	private static final String BUNDLE_NAME = "RightnowSuccessCodes";

	public RightnowSuccessCodes(int successCode, String successMessage) {
		setAll(successCode, successMessage, BUNDLE_NAME);
	}

}
