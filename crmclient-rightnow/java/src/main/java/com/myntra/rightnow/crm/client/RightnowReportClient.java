package com.myntra.rightnow.crm.client;

import java.util.ArrayList;
import java.util.List;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.rightnow.crm.client.entry.RightnowReportFilterEntry;
import com.myntra.rightnow.crm.client.response.RightnowReportResponse;
import com.myntra.rightnow.crm.constants.RightnowConstants;

/**
 * Web client for customer order search web service which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class RightnowReportClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/report/";

	public static RightnowReportResponse getReportData(String serviceURL, String reportName) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "{reportName}", reportName);
		client.setReceiveTimeOut(60 * 1000);
		return client.get(RightnowReportResponse.class);
	}

	public static RightnowReportResponse getReportData(String serviceURL, String reportName,
			List<RightnowReportFilterEntry> filterEntries) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, RightnowConstants.CRM_RIGHTNOW_URL,
				TestHelper.dummyContextInfo1);

		client.path(SERVICE_PREFIX + "filter/{reportName}", reportName);
		client.setReceiveTimeOut(60 * 1000);
		RightnowReportResponse response = new RightnowReportResponse(filterEntries);

		return client.put(response, RightnowReportResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		RightnowReportResponse test = getReportData("http://localhost:7093/crmservice-rightnow", "3PL invalid tracking number and ODA");
		System.out.println(test.getStatus());

		// test report with filters
		// report name
		String reportName = "3PL All";

		// filter fields
		String[] RIGHTNOW_REPORT_FILTERS = { "Courier Name", "Warehouse" };

		// field values
		String value1 = "Blue Dart";
		String value2 = "Delhi-Gurgaon";

		List<RightnowReportFilterEntry> filters = new ArrayList<RightnowReportFilterEntry>();
		RightnowReportFilterEntry filter1 = null;
		RightnowReportFilterEntry filter2 = null;

		// filter 1
		filter1 = new RightnowReportFilterEntry();
		filter1.setProperty(RIGHTNOW_REPORT_FILTERS[0]);
		filter1.setValues(new String[] { value1 });
		filters.add(filter1);

		// filter 2
		filter2 = new RightnowReportFilterEntry();
		filter2.setProperty(RIGHTNOW_REPORT_FILTERS[1]);
		filter2.setValues(new String[] { value2 });
		filters.add(filter2);

		// convert filter list to an array
		// RightnowReportFilterEntry[] filterArray = filters.toArray(new
		// RightnowReportFilterEntry[filters.size()]);

		// RightnowReportResponse test1 =
		// getReportData("http://localhost:7093/crmservice-rightnow",
		// reportName,
		// filterArray);
		RightnowReportResponse test1 = getReportData("http://localhost:7093/crmservice-rightnow", reportName, filters);
		System.out.println(test1.getStatus());
		
		
		// test email response dump report with filters
		// report name
		reportName = "Email Response Dump";
		
		List<RightnowReportFilterEntry> filtersEmailDump = new ArrayList<RightnowReportFilterEntry>();
		RightnowReportFilterEntry filterEntry = new RightnowReportFilterEntry();
		filterEntry.setProperty("Date Range");
		filterEntry.setDataType(4);
		filterEntry.setOperator(9);
		filterEntry.setValues(new String[] {"2014-01-15T00:00:00Z", "2014-01-16T00:00:00Z"});
		
		filtersEmailDump.add(filterEntry);

		RightnowReportResponse test2 = getReportData("http://localhost:7093/crmservice-rightnow", reportName, filtersEmailDump);
		System.out.println(test2.getStatus());

	}
}