package com.myntra.rightnow.crm.client.entry;

import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "rightnowNoteEntry")
public class RightnowNoteEntry extends BaseEntry implements Comparable<RightnowNoteEntry>{

	private static final long serialVersionUID = 1L;
	
	private String text;
	private Date createdTime;
	
	public RightnowNoteEntry() {
		
	}
	
	public RightnowNoteEntry(Long id, String text, Date createdTime) {
		super.setId(id);
		this.text = text;
		this.createdTime = createdTime;
	}
	
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Override
	public String toString() {
		return "RightnowNoteEntry [text=" + text + "]";
	}
	
	@Override
	public int compareTo(RightnowNoteEntry o) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(o.getCreatedTime());
		
		Calendar c2 = Calendar.getInstance();
		c2.setTime(this.getCreatedTime());
		long diff = c1.getTimeInMillis()-c2.getTimeInMillis();
		return new Long(diff).intValue();
	}
}
