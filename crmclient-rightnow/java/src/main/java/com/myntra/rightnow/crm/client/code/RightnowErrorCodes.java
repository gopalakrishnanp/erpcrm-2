package com.myntra.rightnow.crm.client.code;

import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.codes.StatusCodes;

/**
 * CRM Rightnow error codes
 * 
 */
public class RightnowErrorCodes extends ERPErrorCodes {
	
	public static final StatusCodes NO_TASK_SAVED_IN_RIGHTNOW = new RightnowErrorCodes(811, "NO_TASK_SAVED_IN_RIGHTNOW");
	public static final StatusCodes NO_TASK_FETCHED = new RightnowErrorCodes(812, "NO_TASK_FETCHED");
	public static final StatusCodes NO_TASKID_IN_RIGHTNOW_REPORT= new RightnowErrorCodes(813, "NO_TASKID_IN_RIGHTNOW_REPORT");
	public static final StatusCodes TASK_CREATION_ABORTED = new RightnowErrorCodes(814, "TASK_CREATION_ABORTED");

	public static final StatusCodes NO_REPORTNAME_IN_RIGHTNOW = new RightnowErrorCodes(911, "NO_REPORTNAME_IN_RIGHTNOW");
	public static final StatusCodes NO_REPORT_FETCHED = new RightnowErrorCodes(912, "NO_REPORT_FETCHED");
	
	public static final StatusCodes NO_CONTACT_CREATED = new RightnowErrorCodes(913, "NO_CONTACT_CREATED");
	
	public static final StatusCodes ALL_TASKS_IN_CLOSED_STATE = new RightnowErrorCodes(914, "ALL_TASKS_IN_CLOSED_STATE");
	public static final StatusCodes CLOSED_TASKS_FOUND = new RightnowErrorCodes(915, "CLOSED_TASKS_FOUND");
	public static final StatusCodes TASKS_NOT_IN_PROACTIVE_OB_CATEGORY = new RightnowErrorCodes(916, "TASKS_NOT_IN_PROACTIVE_OB_CATEGORY");
	public static final StatusCodes TASKS_CLOSED_WITH_EXCEPTIONS = new RightnowErrorCodes(917, "TASKS_CLOSED_WITH_EXCEPTIONS");

	// welcome call task codes
	public static final StatusCodes ERROR_CREATE_WELCOME_CALL_TASK = new RightnowErrorCodes(918, "ERROR_CREATE_WELCOME_CALL_TASK");
	public static final StatusCodes ERROR_CREATE_WELCOME_CALL_TASK_LOG = new RightnowErrorCodes(919, "ERROR_CREATE_WELCOME_CALL_TASK_LOG");
	public static final StatusCodes ERROR_WELCOME_CALL_FILTER_STATUS = new RightnowErrorCodes(920, "ERROR_WELCOME_CALL_FILTER_STATUS");
	public static final StatusCodes ERROR_WELCOME_CALL_FILTER_ML = new RightnowErrorCodes(921, "ERROR_WELCOME_CALL_FILTER_ML");
	public static final StatusCodes ERROR_WELCOME_CALL_FILTER_DELIVERY_TAT = new RightnowErrorCodes(922, "ERROR_WELCOME_CALL_FILTER_DELIVERY_TAT");
	public static final StatusCodes ERROR_WELCOME_CALL_FILTER_RETURNABLE = new RightnowErrorCodes(923, "ERROR_WELCOME_CALL_FILTER_RETURNABLE");
	public static final StatusCodes ERROR_WELCOME_CALL_FILTER_FIRST_ORDER = new RightnowErrorCodes(924, "ERROR_WELCOME_CALL_FILTER_FIRST_ORDER");
	public static final StatusCodes ERROR_WELCOME_CALL_TASK_EXISTS = new RightnowErrorCodes(925, "ERROR_WELCOME_CALL_TASK_EXISTS");
	public static final StatusCodes ERROR_WELCOME_CALL_CC_CAPACITY = new RightnowErrorCodes(926, "ERROR_WELCOME_CALL_CC_CAPACITY");
	public static final StatusCodes ERROR_WELCOME_CALL_DISTRIBUTION_CRITERIA = new RightnowErrorCodes(927, "ERROR_WELCOME_CALL_DISTRIBUTION_CRITERIA");
		
		
	private static final String BUNDLE_NAME = "RightnowErrorCodes";

	public RightnowErrorCodes(int errorCode, String errorMessage) {
		setAll(errorCode, errorMessage, BUNDLE_NAME);
	}

}
