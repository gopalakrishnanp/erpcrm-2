package com.myntra.rightnow.crm.client.entry;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;
import com.myntra.commons.tranformer.TransformIgnoreAttribute;

@XmlRootElement(name = "rightnowTaskListEntry")
public class RightnowTaskListEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;

	private List<RightnowTaskEntry> taskEntryList;

	@XmlElementWrapper(name = "rightnowTaskEntries")
	@XmlElement(name = "rightnowTaskEntry")
	@TransformIgnoreAttribute
	public List<RightnowTaskEntry> getTaskEntryList() {
		return taskEntryList;
	}

	public void setTaskEntryList(List<RightnowTaskEntry> taskEntryList) {
		this.taskEntryList = taskEntryList;
	}

	@Override
	public String toString() {
		return "RightnowTaskListEntry [taskEntryList=" + taskEntryList + "]";
	}

}