package com.myntra.rightnow.crm.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.myntra.rightnow.crm.client.entry.RightnowTaskIdEntry;

public class TestMarshall {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RightnowTaskIdEntry taskEntry = new RightnowTaskIdEntry();
		List<String> taskIds = new ArrayList<String>();

		try {
			for (long i = 1; i <= 10; i++) {
				taskIds.add(i+"");
			}
			taskEntry.setTaskId(taskIds);
			JAXBContext jaxbContext = JAXBContext.newInstance(RightnowTaskIdEntry.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// jaxbMarshaller.marshal(response, file);
			jaxbMarshaller.marshal(taskEntry, System.out);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
