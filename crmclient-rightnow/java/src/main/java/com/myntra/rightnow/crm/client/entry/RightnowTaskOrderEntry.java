/**
 * 
 */
package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author preetam
 * 
 */
@XmlRootElement(name = "rightnowTaskOrderEntry")
public class RightnowTaskOrderEntry extends RightnowBaseTaskEntry {

	private static final long serialVersionUID = 1L;

	private long orderId;

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "RightnowTaskOrderEntry [orderId=" + orderId + "]";
	}

}
