package com.myntra.rightnow.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

@XmlRootElement(name = "rightnowProactiveTaskEntry")
public class RightnowProactiveTaskEntry extends BaseEntry {

	private static final long serialVersionUID = 1L;
	
	private long orderId;
	private String mainCategory;
	private String subCategory;
	private String subSubCategory;
	
	
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getMainCategory() {
		return mainCategory;
	}
	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getSubSubCategory() {
		return subSubCategory;
	}
	public void setSubSubCategory(String subSubCategory) {
		this.subSubCategory = subSubCategory;
	}
	
	@Override
	public String toString() {
		return "RightnowProactiveTaskEntry [orderId=" + orderId
				+ ", mainCategory=" + mainCategory + ", subCategory="
				+ subCategory + ", subSubCategory=" + subSubCategory + "]";
	}	
	
}
