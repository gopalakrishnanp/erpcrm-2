package com.myntra.erp.crm.manager.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.myntra.catalog.client.ProductClient;
import com.myntra.catalog.client.domain.enums.ImageQuality;
import com.myntra.catalog.client.domain.enums.ImageType;
import com.myntra.catalog.client.domain.response.ImageEntry;
import com.myntra.catalog.client.domain.response.ImageParameters;
import com.myntra.catalog.client.domain.response.ProductEntry;
import com.myntra.client.wms.response.SkuEntry;
import com.myntra.client.wms.response.location.WarehouseEntry;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTrackingDetailEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTripAssignmentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnCommentEntry;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;
import com.myntra.erp.crm.client.response.CustomerReturnResponse;
import com.myntra.erp.crm.manager.CustomerReturnManager;
import com.myntra.erp.crm.manager.util.ServiceManagerUtil;
import com.myntra.lms.client.response.OrderTrackingDetailEntry;
import com.myntra.lms.client.response.OrderTrackingEntry;
import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.response.TripOrderAssignementEntry;
import com.myntra.lms.client.response.TripOrderAssignmentResponse;
import com.myntra.portal.crm.client.entry.ReturnCommentEntry;
import com.myntra.portal.crm.client.entry.ReturnEntry;

/**
 * Manager implementation for customer return web service which integrates
 * detail of return comments, skus, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerReturnManagerImpl extends BaseManagerImpl<CustomerReturnResponse, CustomerReturnEntry> implements
		CustomerReturnManager {

	private static final Logger LOGGER = Logger.getLogger(CustomerReturnManagerImpl.class);

	private ServiceManagerUtil serviceUtil;

	public ServiceManagerUtil getServiceUtil() {
		return serviceUtil;
	}

	public void setServiceUtil(ServiceManagerUtil serviceUtil) {
		this.serviceUtil = serviceUtil;
	}

	@Override
	public CustomerReturnResponse getReturnDetail(Long returnId, Long orderId, String login,
			boolean isLogisticDetailNeeded) throws ERPServiceException {

		// Initialise customer return response and entry
		CustomerReturnResponse response = new CustomerReturnResponse();
		List<CustomerReturnEntry> custReturnEntryList = new ArrayList<CustomerReturnEntry>();
		List<ReturnEntry> portalReturnEntryList;

		try {

			portalReturnEntryList = getServiceUtil().getReturnDetail(returnId, orderId, login);

			if (portalReturnEntryList != null && portalReturnEntryList.size() > 0) {

				for (ReturnEntry singlePortalReturnEntry : portalReturnEntryList) {
					CustomerReturnEntry singleReturnEntry = toCustomerReturnEntry(singlePortalReturnEntry);

					// add return comments
					List<ReturnCommentEntry> poratlReturnCommentEntryList = singlePortalReturnEntry
							.getReturnCommentEntry();
					List<CustomerReturnCommentEntry> customerReturnCommentEntryList = new ArrayList<CustomerReturnCommentEntry>();

					if (poratlReturnCommentEntryList != null && poratlReturnCommentEntryList.size() > 0) {
						for (ReturnCommentEntry singlePortalReturnCommentEntry : poratlReturnCommentEntryList) {
							CustomerReturnCommentEntry singleReturnCommentEntry = toCustomerReturnCommentEntry(singlePortalReturnCommentEntry);
							customerReturnCommentEntryList.add(singleReturnCommentEntry);
						}
						singleReturnEntry.setReturnCommentEntry(customerReturnCommentEntryList);
					}

					// append sku detail
					try {
						CustomerOrderItemEntry itemEntry = new CustomerOrderItemEntry();
						SkuEntry skuEntry = getServiceUtil().getSKUEntry(null, singleReturnEntry.getSkuCode());
						if (skuEntry != null) {
							itemEntry.setSkuId(skuEntry.getId());
							itemEntry.setSize(skuEntry.getSize());
							itemEntry.setArticleTypeName(skuEntry.getArticleTypeName());
							itemEntry.setBrandName(skuEntry.getBrandName());
						}

						// style image and title detail from cms portal
						// service
						Long skuId = itemEntry.getSkuId();
						try {
							ProductEntry styleEntry = getServiceUtil().getStylePropertyBySku(skuId);

							if (styleEntry != null) {
								itemEntry.setStyleId(styleEntry.getProductId());
								itemEntry.setStyleName(styleEntry.getProductDisplayName());
								itemEntry.setUnitPrice(styleEntry.getPrice());
								itemEntry.setVendorArticleNo(styleEntry.getArticleNumber());

								if (styleEntry != null) {
									Map<ImageType, ImageEntry> imageTypeEntryMap = styleEntry.getImageCollectionEntry()
											.getImageEntryMap();
									if (imageTypeEntryMap != null) {
										//ImageEntry imageEntry = imageTypeEntryMap.get(ImageType.DEFAULT);											
										ImageParameters imageParams = new ImageParameters("150X200", true, ImageQuality.HIGH);
										ImageEntry imgEntry = ProductClient.fillResolutionURL(imageTypeEntryMap.get(ImageType.DEFAULT), imageParams);		
										String relativePath = imgEntry.getResolutionToURLMap().get("150X200Xmini");
										if(relativePath!=null){
											itemEntry.setSearchImagezURL(imgEntry.getDomain()+"/"+relativePath);
										}
										else {
											throw new ERPServiceException("CMS Error : Could not fetch Image for product", -1111);
										}
									}									
								}
							}
						} catch (ERPServiceException e) {
							LOGGER.debug("Catalog service exception");
						} catch (Exception e) {
							LOGGER.debug("Catalog service exception");
						}

						singleReturnEntry.setOrderItemEntry(itemEntry);
					} catch (ERPServiceException e) {
						LOGGER.debug("SKU service exception");
					} catch (Exception e) {
						LOGGER.debug("SKU service exception");
					}

					// Add warehouse detail to shipment
					try {

						WarehouseEntry warehouseEntry = getServiceUtil().getWarehouseEntry(
								singleReturnEntry.getWareHouseId());

						if (warehouseEntry != null) {
							singleReturnEntry.setWareHouseName(warehouseEntry.getName());
						}

					} catch (ERPServiceException e) {
						LOGGER.debug("warehouseResponseError :" + "\nInput Params : warehouseId="
								+ singleReturnEntry.getWareHouseId() + "\nReference Params :returnId="
								+ singleReturnEntry.getReturnId());
					}
					// add tracking detail to shipment
					if (isLogisticDetailNeeded) {
						String returnMode = singleReturnEntry.getReturnMode().toLowerCase();

						if (returnMode.equals("pickup")) {

							try {
								OrderTrackingEntry returnTrackingEntry = getServiceUtil().getTrackingEntry(
										singleReturnEntry.getTrackingNumber(), singleReturnEntry.getCourierService());
								if (returnTrackingEntry != null) {						
									
									if(returnTrackingEntry.getDeliveryStatus()!=null)
										singleReturnEntry.setPickupDeliveryStatus(returnTrackingEntry.getDeliveryStatus().name());
									
									List<OrderTrackingDetailEntry> returnTrackingDetailEntryList = returnTrackingEntry
											.getOrderTrackingDetailEntry();
									List<CustomerOrderTrackingDetailEntry> customerReturnTrackingDetailEntryList = new ArrayList<CustomerOrderTrackingDetailEntry>();

									if (returnTrackingDetailEntryList != null) {
										for (OrderTrackingDetailEntry returnTrackingDetail : returnTrackingDetailEntryList) {
											CustomerOrderTrackingDetailEntry singleCustomerReturnTrackingDetailEntry = new CustomerOrderTrackingDetailEntry(
													returnTrackingDetail.getLocation(),
													returnTrackingDetail.getActionDate(),
													returnTrackingDetail.getActivityType(),
													returnTrackingDetail.getRemark());

											customerReturnTrackingDetailEntryList
													.add(singleCustomerReturnTrackingDetailEntry);
										}
									}

									// set tracking detail list for shipment
									singleReturnEntry.setTrackingDetailEntry(customerReturnTrackingDetailEntryList);

									// add trip detail to shipment
									try {
										List<TripOrderAssignementEntry> tripReturnAssignmentEntryList = getServiceUtil()
												.getTripAssignmentEntry(singleReturnEntry.getTrackingNumber());
										List<CustomerOrderTripAssignmentEntry> customerReturnTripAssignmentEntryList = new ArrayList<CustomerOrderTripAssignmentEntry>();

										if (tripReturnAssignmentEntryList != null) {
											for (TripOrderAssignementEntry tripReturnAssignment : tripReturnAssignmentEntryList) {
												CustomerOrderTripAssignmentEntry singleCustomerReturnTripAssignmentEntry = new CustomerOrderTripAssignmentEntry();

												singleCustomerReturnTripAssignmentEntry
														.setDeliveryStaffName(tripReturnAssignment
																.getDeliveryStaffName());
												singleCustomerReturnTripAssignmentEntry.setRemark(tripReturnAssignment
														.getRemark());
												customerReturnTripAssignmentEntryList
														.add(singleCustomerReturnTripAssignmentEntry);
											}
										}

										// set trip assignment list for shipment
										singleReturnEntry.setTripAssignmentEntry(customerReturnTripAssignmentEntryList);

									} catch (ERPServiceException e) {
										TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
										StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
												StatusResponse.Type.ERROR);
										tripOrderAssignmentResponse.setStatus(error);
										LOGGER.debug("tripOrderAssignemntResponseError :"
												+ tripOrderAssignmentResponse.getStatus().toString()
												+ "\nInput Params : trackingNumber="
												+ singleReturnEntry.getTrackingNumber()
												+ "\nReference Params :returnId=" + returnId + ",orderId=" + orderId
												+ ",login=" + login);
									}

								}

							} catch (ERPServiceException e) {
								OrderTrackingResponse orderTrackingResponse = new OrderTrackingResponse();
								StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
										StatusResponse.Type.ERROR);
								orderTrackingResponse.setStatus(error);
								LOGGER.debug("orderTrackingResponseError :"
										+ orderTrackingResponse.getStatus().toString()
										+ "\nInput Params : trackingNumber=" + singleReturnEntry.getTrackingNumber()
										+ ", courierService=" + singleReturnEntry.getCourierService()
										+ "\nReference Params :returnId=" + returnId + ",orderId=" + orderId
										+ ",login=" + login);
							}
						}
					}

					LOGGER.debug("CheckCustomerReturnEntry " + singleReturnEntry.toString());
					custReturnEntryList.add(singleReturnEntry);
				}
			}

			response.setCustomerReturnEntryList(custReturnEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);

		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_RETURN_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCustomerReturnEntryList().size());
		response.setStatus(success);

		return response;
	}

	private CustomerReturnEntry toCustomerReturnEntry(ReturnEntry portalReturnEntry) {

		if (portalReturnEntry == null) {
			return null;
		}

		return new CustomerReturnEntry(portalReturnEntry.getReturnId(), portalReturnEntry.getOrderId(), portalReturnEntry.getItemId(),
				portalReturnEntry.getSkuCode(), portalReturnEntry.getQuantity(), portalReturnEntry.getSize(),
				portalReturnEntry.getReturnStatus(), portalReturnEntry.getLogin(), portalReturnEntry.getCustomerName(),
				portalReturnEntry.getReturnAddress(), portalReturnEntry.getCity(), portalReturnEntry.getState(),
				portalReturnEntry.getCountry(), portalReturnEntry.getZipCode(), portalReturnEntry.getMobile(),
				portalReturnEntry.getReturnMode(), portalReturnEntry.getCourierService(),
				portalReturnEntry.getTrackingNumber(), portalReturnEntry.getReturnReason(),
				portalReturnEntry.getReturnDescription(), portalReturnEntry.getPickupCharge(),
				portalReturnEntry.getRefundAmount(), portalReturnEntry.getReturnCreatedDate(),
				portalReturnEntry.getDCCode(), portalReturnEntry.getIsRefunded(), portalReturnEntry.getRefundDate(),
				portalReturnEntry.getRefundModeCredit(), portalReturnEntry.getReturnStatusCode(),
				portalReturnEntry.getReturnStatusName(), portalReturnEntry.getReturnStatusDescription(),
				portalReturnEntry.getItemBarCode(), portalReturnEntry.getWareHouseId());
	}

	private CustomerReturnCommentEntry toCustomerReturnCommentEntry(ReturnCommentEntry portalReturnCommentEntry) {

		if (portalReturnCommentEntry == null) {
			return null;
		}

		return new CustomerReturnCommentEntry(portalReturnCommentEntry.getReturnComment(),
				portalReturnCommentEntry.getReturnCommentBy(), portalReturnCommentEntry.getReturnCommentDescription(),
				portalReturnCommentEntry.getReturnCommentCreatedDate(), portalReturnCommentEntry.getImageURL());

	}

	@Override
	public AbstractResponse search(int i, int i1, String string, String string1, String string2)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse create(CustomerReturnEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(CustomerReturnEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}