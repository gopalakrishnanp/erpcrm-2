package com.myntra.erp.crm.manager.impl;

import java.util.List;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.utils.Context;
import com.myntra.commons.utils.ServiceURLProperty;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderReleaseResponse;

public class PaymentLogDetailTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String searchTerms = "id.eq:8452305"; 
		OrderReleaseResponse releaseResponse = new OrderReleaseResponse();

		try {
			// TODO move this to OMS Client codebase.
			BaseWebClient client = new BaseWebClient("http://54.251.103.6:7030/myntra-oms-service/", ServiceURLProperty.OMS_URL, Context.getContextInfo());
			client.path("/oms/orderrelease/search");
			client.query("q", searchTerms);
			releaseResponse = client.get(OrderReleaseResponse.class);

		} catch (Exception e) {
			e.printStackTrace();			
		}
		
		List<OrderReleaseEntry> releaseEntryList = releaseResponse.getData();
		OrderReleaseEntry releaseEntry = releaseEntryList.get(0);
		releaseEntry.setDeliveredOn(null);
		try {
			if (releaseEntry.getDeliveredOn().compareTo(releaseEntry.getExpectedDeliveryPromise()) > 0) {
				System.out.println("+ve result: delayed order");				
			}else {
				System.out.println("-ve or 0 result: not a delayed order");
			}
			System.out.println("delivered on:"+releaseEntry.getDeliveredOn());
			System.out.println("promised on:"+releaseEntry.getExpectedDeliveryPromise());
		} catch (NullPointerException e) {
			e.printStackTrace();
			System.out.println("+ve result: delayed order");
		}
		
		/*try {
			//String styleDetailURL = WebserviceUtil.getServiceUrlForKey("styleDetailURL");
			HttpGet getRequest = new HttpGet("http://54.251.99.27:8082/payments/admin/paymentlog/" + "34");						
			getRequest.addHeader("accept", "application/json");

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(getRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String data;
			StringBuilder outputBuilder = new StringBuilder(2048);
			while ((data = br.readLine()) != null) {
				outputBuilder.append(data);
			}
			
			JSONObject styleObject = new JSONObject(outputBuilder.toString());						
			System.out.println(styleObject.toString());
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}