package com.myntra.erp.crm.util;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.myntra.client.tools.response.ApplicationPropertiesEntry;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.codes.ERPErrorCodes;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.utils.LoadApplicationProperties;
import com.myntra.commons.utils.LoadApplicationPropertiesFile;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class WebserviceUtil {

	private static final Logger logger = Logger.getLogger(WebserviceUtil.class.getName());

	private static Map<String, String> urlList = new HashMap<String, String>();

	public static <T extends AbstractResponse> T get(String url, Class<T> objClass) {
		logger.log(Level.INFO, "Fire GET request to - " + url);
		WebResource.Builder builder = createServiceClient(url);
		T response = builder.get(objClass);
		return response;
	}

	private static WebResource.Builder createServiceClient(String url) {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(UriBuilder.fromUri(url).build());

		// String auth = new
		// String(Base64.encodeBase64("sanjay~sanjay~sanjay".getBytes()));
		return service.accept(MediaType.APPLICATION_JSON) // .header("Authorization",
															// "Basic " + auth)
				.type(MediaType.APPLICATION_JSON).header("Accept-Language", "en-us");
	}

	public static String getServiceUrlForKey(String key) throws ERPServiceException {
		if (urlList.isEmpty()) {
			WebserviceUtil.reload();
		}
		if (urlList.get(key) != null)
			return urlList.get(key);
		else
			throw new ERPServiceException(ERPErrorCodes.INVALID_SERVICE_URL);
	}

	private static void reload() throws ERPServiceException {
		logger.info("Refresh service urls at " + new Date());
		LoadApplicationProperties properties = new LoadApplicationPropertiesFile(
				new String[] { "serviceurls.properties" });
		properties.loadProperties();
		Hashtable<String, ApplicationPropertiesEntry> hm = properties.getProperties();
		for (String s : hm.keySet()) {
			urlList.put(s, hm.get(s).getValue());
		}
	}
	
	public static ContextInfo getContextInfo() {
		String currentHostName = "";
		try {
			currentHostName = java.net.InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.info("Could not find Host Name for generating ContextInfo");			
		}
		
		ContextInfo ctxInfo = new ContextInfo("service user", "serviceuser", "serviceuser@myntra.com","myntra123", "product-service", currentHostName, Locale.ENGLISH);
		return ctxInfo;
	}

}