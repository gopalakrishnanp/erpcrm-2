package com.myntra.erp.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.erp.crm.manager.TripDeliveryStaffManager;
import com.myntra.erp.crm.service.TripDeliveryStaffService;

/**
 * Trip delivery staff service implementation to retrieve the detail of the trip
 * delivery staff along with delivery center detail
 * 
 * @author Arun Kumar
 */
public class TripDeliveryStaffServiceImpl extends BaseServiceImpl<TripDeliveryStaffResponse, TripDeliveryStaffEntry>
		implements TripDeliveryStaffService {

	@Override
	public AbstractResponse getTripDeliveryStaffDetail(Long deliveryStaffId, boolean isDCDetailNeeded) {
		try {
			return ((TripDeliveryStaffManager) getManager()).getTripDeliveryStaffDetail(deliveryStaffId, isDCDetailNeeded);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}
