package com.myntra.erp.crm.manager;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.BaseManager;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;

/**
 * Manager interface(abstract) for customer order service which integrates
 * detail of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public interface CustomerOrderManager extends BaseManager<CustomerOrderResponse, CustomerOrderEntry> {

	public CustomerOrderResponse getOrderDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms, boolean isWarehouseDetailNeeded, boolean isSKUDetailNeeded,
			boolean isLogisticDetailNeeded, boolean isPaymentLogNeeded) throws ERPServiceException;

	public CustomerOrderResponse getOrderMinimumDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms) throws ERPServiceException;
}