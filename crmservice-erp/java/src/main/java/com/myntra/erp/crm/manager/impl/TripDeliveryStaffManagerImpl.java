package com.myntra.erp.crm.manager.impl;

import org.apache.log4j.Logger;

import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.TripDeliveryStaffEntry;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.erp.crm.manager.TripDeliveryStaffManager;
import com.myntra.erp.crm.manager.util.ServiceManagerUtil;
import com.myntra.lms.client.response.DeliveryCenterEntry;
import com.myntra.lms.client.response.DeliveryCenterResponse;
import com.myntra.lms.client.response.DeliveryStaffEntry;

/**
 * Manager implementation to retrieve trip delivery staff along with delivery
 * center detail
 * 
 * @author Arun Kumar
 */
public class TripDeliveryStaffManagerImpl extends BaseManagerImpl<TripDeliveryStaffResponse, TripDeliveryStaffEntry>
		implements TripDeliveryStaffManager {

	private static final Logger LOGGER = Logger.getLogger(TripDeliveryStaffManagerImpl.class);

	private ServiceManagerUtil serviceUtil;

	public ServiceManagerUtil getServiceUtil() {
		return serviceUtil;
	}

	public void setServiceUtil(ServiceManagerUtil serviceUtil) {
		this.serviceUtil = serviceUtil;
	}

	@Override
	public TripDeliveryStaffResponse getTripDeliveryStaffDetail(Long deliveryStaffId, boolean isDCDetailNeeded)
			throws ERPServiceException {

		// Initialize customer order response and entry
		TripDeliveryStaffResponse response = new TripDeliveryStaffResponse();
		TripDeliveryStaffEntry tripDeliveryStaffEntry = new TripDeliveryStaffEntry();
		DeliveryStaffEntry dcStaffEntry = null;
		
		try {

			// get dcstaff id and set staff detail			
			dcStaffEntry = getServiceUtil().getDeliveryStaffDetail(deliveryStaffId);
			tripDeliveryStaffEntry.setFirstName(dcStaffEntry.getFirstName());
			tripDeliveryStaffEntry.setLastName(dcStaffEntry.getLastName());
			tripDeliveryStaffEntry.setMobile(dcStaffEntry.getMobile());
			tripDeliveryStaffEntry.setAvailable(dcStaffEntry.getAvailable());
			tripDeliveryStaffEntry.setDeleted(dcStaffEntry.getDeleted());
			tripDeliveryStaffEntry.setCode(dcStaffEntry.getCode());
			

			// get dc id from staff and set dc detail
			try {
				if (isDCDetailNeeded) {
					if (dcStaffEntry != null) {
						DeliveryCenterEntry dcEntry = getServiceUtil().getDeliveryCenterEntry(
								dcStaffEntry.getDeliveryCenterId());
						tripDeliveryStaffEntry.setDcCode(dcEntry.getCode());
						tripDeliveryStaffEntry.setDcName(dcEntry.getName());
						tripDeliveryStaffEntry.setDcManager(dcEntry.getManager());
						tripDeliveryStaffEntry.setDcContactNumber(dcEntry.getContactNumber());
						tripDeliveryStaffEntry.setDcActive(dcEntry.getActive());
						tripDeliveryStaffEntry.setDcCity(dcEntry.getCity());
						tripDeliveryStaffEntry.setDcCityCode(dcEntry.getCityCode());
						tripDeliveryStaffEntry.setDcState(dcEntry.getState());
						tripDeliveryStaffEntry.setDcAddress(dcEntry.getAddress());
						tripDeliveryStaffEntry.setDcCourierCode(dcEntry.getCourierCode());
						tripDeliveryStaffEntry.setDcPincode(dcEntry.getPincode());
						tripDeliveryStaffEntry.setDcSelfShipSupport(dcEntry.getSelfShipSupported());
					}

				}
			} catch (ERPServiceException e) {
				DeliveryCenterResponse dcResponse = new DeliveryCenterResponse();
				StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
				dcResponse.setStatus(error);
				LOGGER.debug("delivertCenterResponseError :" + dcResponse.getStatus().toString()
						+ "\nInput Params : DCId=" + dcStaffEntry.getDeliveryCenterId()
						+ "\nReference Params :orderId=" + deliveryStaffId);
			}

			response.setTripDeliveryStaffEntry(tripDeliveryStaffEntry);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.TRIP_DELIVERY_STAFF_DETAIL_RETRIEVED,
				StatusResponse.Type.SUCCESS, (dcStaffEntry != null)? 1 : 0);
		response.setStatus(success);
		

		return response;
	}

	@Override
	public AbstractResponse create(TripDeliveryStaffEntry entry) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse update(TripDeliveryStaffEntry entry, Long itemId) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse findById(Long id) throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AbstractResponse search(int start, int fetchSize, String sortBy, String sortOrder, String searchTerms)
			throws ERPServiceException {
		// TODO Auto-generated method stub
		return null;
	}
}