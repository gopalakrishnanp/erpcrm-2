package com.myntra.erp.crm.ops;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.erp.crm.client.TripDeliveryStaffClient;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;

/**
 * HttpServlet class for encapsulating logic for providing health check of the
 * service. The servlet needs to first check weather essential health indicators
 * of the service are running and then checks weather the application has been
 * manually pulled down. The second step is required for enabling manually
 * moving any application out of the LB
 */
public class HealthCheck extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	try {
    		TripDeliveryStaffResponse staffResponse =  TripDeliveryStaffClient
    				.getTripDeliveryStaffDetail("http://localhost:7090/crmservice-erp", 42L, false);
    			
    		if(staffResponse!=null && staffResponse.getStatus().getStatusType().equals("SUCCESS") 
    				&& null!=staffResponse.getTripDeliveryStaffEntry()) {
    			response.getOutputStream().print("OK");
    		} else {
    			response.setStatus(HttpServletResponse.SC_NOT_FOUND);	
    		}
    	} catch (ERPServiceException e) {
    		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    	}
    }

}
