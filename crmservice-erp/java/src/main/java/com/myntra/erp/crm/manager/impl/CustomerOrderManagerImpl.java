package com.myntra.erp.crm.manager.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.myntra.catalog.client.ProductClient;
import com.myntra.catalog.client.domain.enums.ImageQuality;
import com.myntra.catalog.client.domain.enums.ImageType;
import com.myntra.catalog.client.domain.response.ImageEntry;
import com.myntra.catalog.client.domain.response.ImageParameters;
import com.myntra.catalog.client.domain.response.ProductEntry;
import com.myntra.client.wms.response.ItemEntry;
import com.myntra.client.wms.response.SkuEntry;
import com.myntra.client.wms.response.SkuResponse;
import com.myntra.client.wms.response.location.WarehouseEntry;
import com.myntra.client.wms.response.location.WarehouseResponse;
import com.myntra.commons.codes.StatusResponse;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.manager.impl.BaseManagerImpl;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.code.CRMErpSuccessCodes;
import com.myntra.erp.crm.client.entry.CustomerOrderBillingAddressEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderShipmentEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTrackingDetailEntry;
import com.myntra.erp.crm.client.entry.CustomerOrderTripAssignmentEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.erp.crm.client.response.TripDeliveryStaffResponse;
import com.myntra.erp.crm.manager.CustomerOrderManager;
import com.myntra.erp.crm.manager.OrderPaymentLogManager;
import com.myntra.erp.crm.manager.TripDeliveryStaffManager;
import com.myntra.erp.crm.manager.util.ServiceManagerUtil;
import com.myntra.lms.client.response.OrderTrackingDetailEntry;
import com.myntra.lms.client.response.OrderTrackingEntry;
import com.myntra.lms.client.response.OrderTrackingResponse;
import com.myntra.lms.client.response.PincodeEntry;
import com.myntra.lms.client.response.TripOrderAssignementEntry;
import com.myntra.lms.client.response.TripOrderAssignmentResponse;
import com.myntra.oms.client.entry.BillingAddressEntry;
import com.myntra.oms.client.entry.OrderEntry;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.portal.crm.client.entry.OrderPaymentLogEntry;
import com.myntra.portal.crm.client.entry.OrderRtoEntry;
import com.myntra.portal.crm.client.response.OrderPaymentLogResponse;

/**
 * Manager implementation for customer order search web service which integrates
 * detail of order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerOrderManagerImpl extends BaseManagerImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		CustomerOrderManager {

	private static final Logger LOGGER = Logger.getLogger(CustomerOrderManagerImpl.class);

	private TripDeliveryStaffManager deliveryStaffManager;

	private OrderPaymentLogManager paymentLogManager;

	private ServiceManagerUtil serviceUtil;

	public TripDeliveryStaffManager getDeliveryStaffManager() {
		return deliveryStaffManager;
	}

	public void setDeliveryStaffManager(TripDeliveryStaffManager deliveryStaffManager) {
		this.deliveryStaffManager = deliveryStaffManager;
	}

	public OrderPaymentLogManager getPaymentLogManager() {
		return paymentLogManager;
	}

	public void setPaymentLogManager(OrderPaymentLogManager paymentLogManager) {
		this.paymentLogManager = paymentLogManager;
	}

	public ServiceManagerUtil getServiceUtil() {
		return serviceUtil;
	}

	public void setServiceUtil(ServiceManagerUtil serviceUtil) {
		this.serviceUtil = serviceUtil;
	}

	@Override
	public CustomerOrderResponse getOrderDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms, boolean isWarehouseDetailNeeded, boolean isSKUDetailNeeded,
			boolean isLogisticDetailNeeded, boolean isPaymentLogNeeded) throws ERPServiceException {		

		// Initialise customer order response and entry
		CustomerOrderResponse response = new CustomerOrderResponse();
		List<CustomerOrderEntry> custOrderEntryList = new ArrayList<CustomerOrderEntry>();

		try {
			List<OrderEntry> orderEntryList = getServiceUtil().getOrderDetail(start, fetchSize, sortBy, sortOrder, searchTerms);
			
			if(orderEntryList == null){
				StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_ORDER_RETRIEVED,
						StatusResponse.Type.SUCCESS, 0);
				response.setStatus(success);
			}
				
			for (OrderEntry orderEntry : orderEntryList) {

				CustomerOrderEntry singleOrderEntry = new CustomerOrderEntry();

				singleOrderEntry.setIsMinimumOrder(false);
				singleOrderEntry.setOrderId(orderEntry.getId());

				// Customer detail
				singleOrderEntry.setLogin(orderEntry.getLogin());
				singleOrderEntry.setCustomerName(orderEntry.getCustomerName());
				singleOrderEntry.setReceiverName("-"); // Will be set while
														// iterating shipments.
				singleOrderEntry.setUserContactNo(orderEntry.getUserContactNo());
				singleOrderEntry.setCreatedOn(orderEntry.getCreatedOn());
				singleOrderEntry.setUserContactNo(orderEntry.getUserContactNo());

				// Add order billing detail to order
				CustomerOrderBillingAddressEntry customerBillingAddressEntry = getCustomerOrderBillingDetailEntry(orderEntry);
				singleOrderEntry.setBillingAddress(customerBillingAddressEntry);

				// coupon
				singleOrderEntry.setCouponCode(orderEntry.getCouponCode());

				// payment detail
				singleOrderEntry.setPaymentMethod(orderEntry.getPaymentMethod());
				singleOrderEntry.setPgDiscount(orderEntry.getPgDiscount());
				singleOrderEntry.setPaymentMethodDisplay("");

				if(isPaymentLogNeeded){
					setOnlinePaymentDetail(singleOrderEntry);	
				}
				
				// amount
				singleOrderEntry.setShippingCharge(orderEntry.getShippingCharge());
				singleOrderEntry.setGiftCharge(orderEntry.getGiftCharge());
				singleOrderEntry.setCartDiscount(orderEntry.getCartDiscount());
				singleOrderEntry.setCashRedeemed(orderEntry.getCashRedeemed());
				singleOrderEntry.setCashbackOffered(orderEntry.getCashRedeemed());
				singleOrderEntry.setCodCharge(orderEntry.getCodCharge());
				singleOrderEntry.setCouponDiscount(orderEntry.getCartDiscount());
				singleOrderEntry.setDiscount(orderEntry.getDiscount());
				singleOrderEntry.setMrpTotal(orderEntry.getMrpTotal());
				singleOrderEntry.setFinalAmount(orderEntry.getFinalAmount());
				singleOrderEntry.setEmiCharge(orderEntry.getEmiCharge());

				singleOrderEntry.setQuantity(0);
				singleOrderEntry.setOrderType(orderEntry.getOrderType());
				singleOrderEntry.setOrderTypeDisplay(orderEntry.getOrderTypeDisplay());

				// Set gift order boolean
				singleOrderEntry.setGiftOrder(orderEntry.isGiftOrder());

				// status
				singleOrderEntry.setOnHold(orderEntry.getOnHold());
				singleOrderEntry.setOnHoldReason(orderEntry.getOnHoldReason());

				List<CustomerOrderShipmentEntry> custOrderShipmentEntryList = new ArrayList<CustomerOrderShipmentEntry>();

				for (OrderReleaseEntry orderShipment : orderEntry.getOrderReleases()) {

					CustomerOrderShipmentEntry singleShipmentEntry = new CustomerOrderShipmentEntry();
					singleShipmentEntry.setShipmentId(orderShipment.getId());
					singleShipmentEntry.setExchangeReleaseId(orderShipment.getExchangeReleaseId());

					// logistic
					singleShipmentEntry.setCourierCode(orderShipment.getCourierCode());
					singleShipmentEntry.setCourierDisplayName(orderShipment.getCourierDisplayName());
					singleShipmentEntry.setTrackingNo(orderShipment.getTrackingNo());
					singleShipmentEntry.setShippingMethod(orderShipment.getShippingMethod());

					// date
					singleShipmentEntry.setPackedOn(orderShipment.getPackedOn());
					singleShipmentEntry.setShippedOn(orderShipment.getShippedOn());
					singleShipmentEntry.setDeliveredOn(orderShipment.getDeliveredOn());
					singleShipmentEntry.setPromisedDate(orderShipment.getExpectedDeliveryPromise());

					// status/reason
					singleShipmentEntry.setPaymentMethodDisplay(orderShipment.getPaymentMethodDisplay());

					// Fix this at order entry level as well.
					if (singleOrderEntry.getPaymentMethodDisplay().equals("")) {
						singleOrderEntry.setPaymentMethodDisplay(orderShipment.getPaymentMethodDisplay());
					}

					singleShipmentEntry.setStatus(orderShipment.getStatus());
					singleShipmentEntry.setStatusDisplayName(orderShipment.getStatusDisplayName());

					// receiver detail
					singleShipmentEntry.setReceiverName(orderShipment.getReceiverName());
					singleOrderEntry.setReceiverName(orderShipment.getReceiverName());

					singleShipmentEntry.setAddress(orderShipment.getAddress());
					singleShipmentEntry.setCity(orderShipment.getCity());
					singleShipmentEntry.setState(orderShipment.getState());
					singleShipmentEntry.setCountry(orderShipment.getCountry());
					singleShipmentEntry.setLocality(orderShipment.getLocality());
					singleShipmentEntry.setMobile(orderShipment.getMobile());
					singleShipmentEntry.setZipcode(orderShipment.getZipcode());

					// amount
					singleShipmentEntry.setShippingCharge(orderShipment.getShippingCharge());
					singleShipmentEntry.setGiftCharge(orderShipment.getGiftCharge());
					singleShipmentEntry.setCartDiscount(orderShipment.getCartDiscount());
					singleShipmentEntry.setCashRedeemed(orderShipment.getCashRedeemed());
					singleShipmentEntry.setCashbackOffered(orderShipment.getCashbackOffered());
					singleShipmentEntry.setCodCharge(orderShipment.getCodCharge());
					singleShipmentEntry.setCouponDiscount(orderShipment.getCouponDiscount());
					singleShipmentEntry.setDiscount(orderShipment.getDiscount());
					singleShipmentEntry.setMrpTotal(orderShipment.getMrpTotal());
					singleShipmentEntry.setFinalAmount(orderShipment.getFinalAmount());
					singleShipmentEntry.setTaxAmount(orderShipment.getTaxAmount());
					
					// loyalty detail
					singleShipmentEntry.setLoyaltyPointAwarded(orderShipment.getLoyaltyPointsAwarded());
					singleShipmentEntry.setLoyaltyPointUsed(orderShipment.getLoyaltyPointsUsed());
					singleShipmentEntry.setLoyaltyAmountUsed(orderShipment.getLoyaltyPointsCredit());

					// We need to add up qty from orderLines.
					singleShipmentEntry.setQuantity(0);

					// Add warehouse detail to shipment
					try {
						if (isWarehouseDetailNeeded) {
							WarehouseEntry warehouseEntry = getServiceUtil().getWarehouseEntry(
									orderShipment.getWarehouseId());

							if (warehouseEntry != null) {
								singleShipmentEntry.setWarehouseName(warehouseEntry.getName());
								singleShipmentEntry.setWarehouseAddress(warehouseEntry.getAddress());
							}
						}
					} catch (ERPServiceException e) {
						WarehouseResponse warehouseResponse = new WarehouseResponse();
						StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
								StatusResponse.Type.ERROR);
						warehouseResponse.setStatus(error);
						LOGGER.debug("warehouseResponseError :" + warehouseResponse.getStatus().toString()
								+ "\nInput Params : warehouseId=" + orderShipment.getWarehouseId()
								+ "\nReference Params :orderId=" + singleOrderEntry.getOrderId() + ",shipmentId="
								+ singleShipmentEntry.getShipmentId());
					}
					
					HashMap<Long, Long> exchangeOrderMap = getServiceUtil().getExchangeDetailMap(singleShipmentEntry.getShipmentId());
					
					// add items detail to shipment
					List<CustomerOrderItemEntry> custOrderItemEntryList = new ArrayList<CustomerOrderItemEntry>();

					for (OrderLineEntry orderItem : orderShipment.getOrderLines()) {

						CustomerOrderItemEntry singleItemEntry = new CustomerOrderItemEntry();

						// unique ids
						singleItemEntry.setId(orderItem.getId());
						singleItemEntry.setSkuId(orderItem.getSkuId());
						singleItemEntry.setStyleId(orderItem.getStyleId());
						singleItemEntry.setReleaseId(orderItem.getOrderReleaseId());
						singleItemEntry.setExchangeLineId(orderItem.getExchangeLineId());
						
						// Possible Exchange OrderID
						singleItemEntry.setExchangeOrderId(exchangeOrderMap.get(orderItem.getId()));

						// amount
						singleItemEntry.setUnitPrice(orderItem.getUnitPrice());
						singleItemEntry.setTaxAmount(orderItem.getTaxAmount());
						singleItemEntry.setCartDiscount(orderItem.getCartDiscount());
						singleItemEntry.setCashRedeemed(orderItem.getCashRedeemed());
						singleItemEntry.setCashbackOffered(orderItem.getCashbackOffered());
						singleItemEntry.setCouponDiscount(orderItem.getCouponDiscount());
						singleItemEntry.setDiscount(orderItem.getDiscount());
						singleItemEntry.setFinalAmount(orderItem.getFinalAmount());

						// loyalty detail
						singleItemEntry.setLoyaltyPointAwarded(orderItem.getLoyaltyPointsAwarded());
						singleItemEntry.setLoyaltyPointUsed(orderItem.getLoyaltyPointsUsed());
						singleItemEntry.setLoyaltyAmountUsed(orderItem.getLoyaltyPointsCredit());
						
						// status/reason
						singleItemEntry.setStatus(orderItem.getStatus());
						singleItemEntry.setStatusDisplayName(orderItem.getStatusDisplayName());
						singleItemEntry.setIsReturnableProduct(orderItem.getIsReturnableProduct());
						singleItemEntry.setCancellationReason(orderItem.getCancellationReason());
						singleItemEntry.setIsCustomizable(orderItem.getIsCustomizable());
						singleItemEntry.setSupplyType(orderItem.getSupplyType());
						
						if(singleItemEntry.getIsCustomizable() != null && singleItemEntry.getIsCustomizable()){
							singleItemEntry.setCustomizedMessage(orderItem.getCustomizedMessage());
							String[] customMsg = singleItemEntry.getCustomizedMessage().split(":",2);
							singleItemEntry.setCustomizedNumber(customMsg[0]);
							singleItemEntry.setCustomizedName(customMsg[1]);
						}

						// qty
						singleItemEntry.setQuantity(orderItem.getQuantity());

						// Update qty at singleShipment and singleOrder levels.
						singleShipmentEntry.setQuantity(singleItemEntry.getQuantity()
								+ singleShipmentEntry.getQuantity());
						singleOrderEntry.setQuantity(singleItemEntry.getQuantity() + singleOrderEntry.getQuantity());

						// append sku detail to item
						try {
							if (isSKUDetailNeeded) {

								SkuEntry skuEntry = getServiceUtil().getSKUEntry(orderItem.getSkuId(), null);
								if (skuEntry != null) {
									singleItemEntry.setSize(skuEntry.getSize());
									singleItemEntry.setArticleTypeName(skuEntry.getArticleTypeName());
									singleItemEntry.setBrandName(skuEntry.getBrandName());
								}

								// style image and title detail from cms portal
								// service
								Long styleId = singleItemEntry.getStyleId();
								try {
									ProductEntry styleEntry = getServiceUtil().getStyleProperty(styleId);
									
									if (styleEntry != null) {
										Map<ImageType, ImageEntry> imageTypeEntryMap = styleEntry.getImageCollectionEntry()
												.getImageEntryMap();
										if (imageTypeEntryMap != null) {
											//ImageEntry imageEntry = imageTypeEntryMap.get(ImageType.DEFAULT);											
											ImageParameters imageParams = new ImageParameters("150X200", true, ImageQuality.HIGH);
											ImageEntry imgEntry = ProductClient.fillResolutionURL(imageTypeEntryMap.get(ImageType.DEFAULT), imageParams);		
											String relativePath = imgEntry.getResolutionToURLMap().get("150X200Xmini");
											if(relativePath!=null){
												singleItemEntry.setSearchImagezURL(imgEntry.getDomain()+"/"+relativePath);
											}
											else {
												throw new ERPServiceException("CMS Error : Could not fetch Image for product", -1111);
											}
										}
										singleItemEntry.setStyleName(styleEntry.getProductDisplayName());
									}
								} catch (ERPServiceException e) {
									LOGGER.error("Catalog service exception");
								} catch (Exception e) {
									LOGGER.error("Catalog service exception");
								}

							}

						} catch (ERPServiceException e) {
							SkuResponse skuResponse = new SkuResponse();
							StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
									StatusResponse.Type.ERROR);
							skuResponse.setStatus(error);
							LOGGER.debug("warehouseResponseError :" + skuResponse.getStatus().toString()
									+ "\nInput Params : SKUId=" + orderItem.getSkuId() + "\nReference Params :orderId="
									+ singleOrderEntry.getOrderId() + ",shipmentId="
									+ singleShipmentEntry.getShipmentId());
						}

						custOrderItemEntryList.add(singleItemEntry);
					}

					// set items list for shipment
					singleShipmentEntry.setOrderItems(custOrderItemEntryList);

					// add tracking detail to shipment
					if (isLogisticDetailNeeded) {
						
						// get the DC detail on pincode of the shipment
						try {
							PincodeEntry dc = getServiceUtil().getDeliveryCenterOnPincode(orderShipment.getZipcode(), orderShipment.getCourierCode());
							if(dc != null){
								singleShipmentEntry.setDeliveryCenterForPincode(dc);	
							}
							
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							
						}
						
						try {
							OrderTrackingEntry orderTrackingEntry = getServiceUtil().getTrackingEntry(
									orderShipment.getTrackingNo(), orderShipment.getCourierCode());
							if (orderTrackingEntry != null) {
								singleShipmentEntry.setDeliveryStatus(orderTrackingEntry.getDeliveryStatus());
								singleShipmentEntry.setFailedAttempts(orderTrackingEntry.getFailedAttempts());

								List<OrderTrackingDetailEntry> orderTrackingDetailEntryList = orderTrackingEntry
										.getOrderTrackingDetailEntry();
								List<CustomerOrderTrackingDetailEntry> customerOrderTrackingDetailEntryList = new ArrayList<CustomerOrderTrackingDetailEntry>();

								if (orderTrackingDetailEntryList != null) {
									for (OrderTrackingDetailEntry orderTrackingDetail : orderTrackingDetailEntryList) {
										CustomerOrderTrackingDetailEntry singleCustomerOrderTrackingDetailEntry = new CustomerOrderTrackingDetailEntry(
												orderTrackingDetail.getLocation(), orderTrackingDetail.getActionDate(),
												orderTrackingDetail.getActivityType(), orderTrackingDetail.getRemark());

										customerOrderTrackingDetailEntryList
												.add(singleCustomerOrderTrackingDetailEntry);
									}
								}

								// set tracking detail list for shipment
								singleShipmentEntry.setOrderTrackingDetailEntry(customerOrderTrackingDetailEntryList);

								// add trip detail to shipment
								try {
									List<TripOrderAssignementEntry> tripOrderAssignmentEntryList = getServiceUtil()
											.getTripAssignmentEntry(orderShipment.getTrackingNo());

									List<CustomerOrderTripAssignmentEntry> customerOrderTripAssignmentEntryList = new ArrayList<CustomerOrderTripAssignmentEntry>();

									if (tripOrderAssignmentEntryList != null) {
										for (TripOrderAssignementEntry tripOrderAssignment : tripOrderAssignmentEntryList) {
											CustomerOrderTripAssignmentEntry singleCustomerOrderTripAssignmentEntry = new CustomerOrderTripAssignmentEntry();

											singleCustomerOrderTripAssignmentEntry
													.setDeliveryStaffName(tripOrderAssignment.getDeliveryStaffName());
											singleCustomerOrderTripAssignmentEntry
													.setDeliveryStaffId(tripOrderAssignment.getDeliveryStaffId());
											singleCustomerOrderTripAssignmentEntry
													.setDeliveryCenterId(tripOrderAssignment.getDeliveryCenterId());
											singleCustomerOrderTripAssignmentEntry.setRemark(tripOrderAssignment
													.getRemark());
											singleCustomerOrderTripAssignmentEntry
													.setTripOrderStatus(tripOrderAssignment.getTripOrderStatus());
											singleCustomerOrderTripAssignmentEntry
													.setCorrespondingTripStatus(tripOrderAssignment
															.getCorrespondingTripStatus());
											singleCustomerOrderTripAssignmentEntry
													.setTripOrderStatusValue(tripOrderAssignment.getTripStatusValue());
											singleCustomerOrderTripAssignmentEntry.setTripStartTime(tripOrderAssignment
													.getTripStartTime());
											singleCustomerOrderTripAssignmentEntry.setTripEndTime(tripOrderAssignment
													.getTripEndTime());
											singleCustomerOrderTripAssignmentEntry
													.setPickupReasonCode(tripOrderAssignment.getPickupReasonCode());

											TripDeliveryStaffResponse tripStaffDeliveryResponse = getDeliveryStaffManager()
													.getTripDeliveryStaffDetail(
															tripOrderAssignment.getDeliveryStaffId(), false);
											singleCustomerOrderTripAssignmentEntry
													.setDeliveryStaffMobile(tripStaffDeliveryResponse
															.getTripDeliveryStaffEntry().getMobile());

											customerOrderTripAssignmentEntryList
													.add(singleCustomerOrderTripAssignmentEntry);
										}
									}

									// set trip assignment list for shipment
									singleShipmentEntry
											.setOrderTripAssignmentEntry(customerOrderTripAssignmentEntryList);

								} catch (ERPServiceException e) {
									TripOrderAssignmentResponse tripOrderAssignmentResponse = new TripOrderAssignmentResponse();
									StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
											StatusResponse.Type.ERROR);
									tripOrderAssignmentResponse.setStatus(error);
									LOGGER.debug("tripOrderAssignemntResponseError :"
											+ tripOrderAssignmentResponse.getStatus().toString()
											+ "\nInput Params : trackingNumber=" + orderShipment.getTrackingNo()
											+ "\nReference Params :orderId=" + singleOrderEntry.getOrderId()
											+ ",shipmentId=" + singleShipmentEntry.getShipmentId());
								}

							}

						} catch (ERPServiceException e) {
							OrderTrackingResponse orderTrackingResponse = new OrderTrackingResponse();
							StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(),
									StatusResponse.Type.ERROR);
							orderTrackingResponse.setStatus(error);
							LOGGER.debug("orderTrackingResponseError :" + orderTrackingResponse.getStatus().toString()
									+ "\nInput Params : trackingNumber=" + orderShipment.getTrackingNo()
									+ ", courierCode=" + orderShipment.getCourierCode()
									+ "\nReference Params :orderId=" + singleOrderEntry.getOrderId() + ",shipmentId="
									+ singleShipmentEntry.getShipmentId());
						}
					}
					
					// add rto detail for the shipment
					if(singleShipmentEntry.getStatus().equalsIgnoreCase("SH") || 
							singleShipmentEntry.getStatus().equalsIgnoreCase("RTO")) {
						try {
							OrderRtoEntry rtoEntry = getServiceUtil().getRtoDetail(singleShipmentEntry.getShipmentId());
							if (rtoEntry != null) {
								singleShipmentEntry.setRtoStatus(rtoEntry.getRtoStatus());
								singleShipmentEntry.setRtoStatusDisplay(rtoEntry.getRtoStatusDisplay());
							}
						} catch (ERPServiceException e) {
							LOGGER.debug("RTO service exception" + e);
						} catch (Exception e) {
							LOGGER.debug("RTO service exception" + e);
						}
					}

					custOrderShipmentEntryList.add(singleShipmentEntry);

					try {
						// get all items barcode from warehouse for the order
						appendWMSItemDetail(singleShipmentEntry);
					} catch (ERPServiceException e) {
						LOGGER.error("No item barcode information from WMS");
					}
				}

				// set shipment list for order
				singleOrderEntry.setOrderShipments(custOrderShipmentEntryList);
				custOrderEntryList.add(singleOrderEntry);
			}

			// set the customer order response
			response.setCustomerOrderEntryList(custOrderEntryList);

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}
		
		// return response with status
		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_ORDER_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCustomerOrderEntryList().size());
		response.setStatus(success);

		return response;
	}

	private void setOnlinePaymentDetail(CustomerOrderEntry orderEntry) {
		if (orderEntry.getPaymentMethod().equalsIgnoreCase("on")) {
			
			orderEntry.setPaymentMethodDisplay("online");
			try {
				// TODO instead of erp manager we have to use portal service client 
				// to get payment log and should move this to serviceManagerUtil.java
				// and remove payment service, manager from crmservice-erp
				OrderPaymentLogResponse paymentLog = getPaymentLogManager().getOrderPaymentLog(
						orderEntry.getOrderId());
				OrderPaymentLogEntry paymentEntry = paymentLog.getOrderPaymentLogEntry();
				if (paymentEntry != null) {
					String paymentType = paymentEntry.getPaymentOption() + " / " + paymentEntry.getGateway()
							+ " / " + paymentEntry.getPaymentIssuer();
					orderEntry.setPaymentMethodDisplay(paymentType);
					
					if(null!=paymentEntry.getCardBankName()) {
						orderEntry.setPaymentMethodCardBankName(paymentEntry.getCardBankName());
					}
				}

			} catch (Exception e) {
				LOGGER.debug("Exception in payment Logs : " + e.getMessage());
			}
		}
	}
	
	@Override
	public CustomerOrderResponse getOrderMinimumDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms) throws ERPServiceException {	

		// Initialise customer order response and entry
		CustomerOrderResponse response = new CustomerOrderResponse();
		List<CustomerOrderEntry> custOrderEntryList = new ArrayList<CustomerOrderEntry>();

		try {
			List<OrderEntry> orderEntryList = getServiceUtil().getOrderDetail(start, fetchSize, sortBy, sortOrder, searchTerms);

			if(orderEntryList==null) {
				StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_ORDER_RETRIEVED,
						StatusResponse.Type.SUCCESS, 0);
				response.setStatus(success);
				return response;
			}
			

			for (OrderEntry orderEntry : orderEntryList) {

				CustomerOrderEntry singleOrderEntry = new CustomerOrderEntry();

				singleOrderEntry.setIsMinimumOrder(true);
				singleOrderEntry.setOrderId(orderEntry.getId());
				singleOrderEntry.setLogin(orderEntry.getLogin());
				
				singleOrderEntry.setOrderType(orderEntry.getOrderType());
				singleOrderEntry.setGiftOrder(orderEntry.isGiftOrder());				
				
				singleOrderEntry.setCreatedOn(orderEntry.getCreatedOn());
				
				singleOrderEntry.setFinalAmount(orderEntry.getFinalAmount());				
				singleOrderEntry.setCouponCode(orderEntry.getCouponCode());
				singleOrderEntry.setCashRedeemed(orderEntry.getCashRedeemed());
				
				singleOrderEntry.setPaymentMethod(orderEntry.getPaymentMethod());
				singleOrderEntry.setPaymentMethodDisplay("");
				
				// needed to avoid null pointer exception while calculating qty at item(line) level
				singleOrderEntry.setQuantity(0);				
				
				setOnlinePaymentDetail(singleOrderEntry);

				List<CustomerOrderShipmentEntry> custOrderShipmentEntryList = new ArrayList<CustomerOrderShipmentEntry>();
				for (OrderReleaseEntry orderShipment : orderEntry.getOrderReleases()) {
					CustomerOrderShipmentEntry singleShipmentEntry = new CustomerOrderShipmentEntry();
					
					singleShipmentEntry.setLogin(orderShipment.getLogin());
					singleShipmentEntry.setShipmentId(orderShipment.getId());
					singleShipmentEntry.setExchangeReleaseId(orderShipment.getExchangeReleaseId());
					singleShipmentEntry.setReceiverName(orderShipment.getReceiverName());
					singleOrderEntry.setReceiverName(orderShipment.getReceiverName());
					singleShipmentEntry.setStatusDisplayName(orderShipment.getStatusDisplayName());
					
					// needed to avoid null pointer exception while calculating qty at item(line) level
					singleShipmentEntry.setQuantity(0);
					
					// add items detail to shipment
					List<CustomerOrderItemEntry> custOrderItemEntryList = new ArrayList<CustomerOrderItemEntry>();
					for (OrderLineEntry orderItem : orderShipment.getOrderLines()) {
						CustomerOrderItemEntry singleItemEntry = new CustomerOrderItemEntry();

						singleItemEntry.setQuantity(orderItem.getQuantity());
						singleShipmentEntry.setQuantity(singleItemEntry.getQuantity() + singleShipmentEntry.getQuantity());
						singleOrderEntry.setQuantity(singleItemEntry.getQuantity() + singleOrderEntry.getQuantity());
						
						custOrderItemEntryList.add(singleItemEntry);
					}

					// set items list for shipment
					singleShipmentEntry.setOrderItems(custOrderItemEntryList);
					custOrderShipmentEntryList.add(singleShipmentEntry);
				}

				// set shipment list for order
				singleOrderEntry.setOrderShipments(custOrderShipmentEntryList);
				custOrderEntryList.add(singleOrderEntry);
			}

			// set the customer order response
			response.setCustomerOrderEntryList(custOrderEntryList);
			

		} catch (ERPServiceException e) {
			LOGGER.error("Error validating/retreiving data", e);
			StatusResponse error = new StatusResponse(e.getCode(), e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		} catch (Exception e) {
			LOGGER.error("error", e);
			StatusResponse error = new StatusResponse(1, e.getMessage(), StatusResponse.Type.ERROR);
			response.setStatus(error);
			throw new ERPServiceException(response);
		}

		// return response with status
		StatusResponse success = new StatusResponse(CRMErpSuccessCodes.CUSTOMER_ORDER_RETRIEVED,
				StatusResponse.Type.SUCCESS, response.getCustomerOrderEntryList().size());
		response.setStatus(success);

		return response;
	}

	private CustomerOrderBillingAddressEntry getCustomerOrderBillingDetailEntry(OrderEntry orderEntry) {

		BillingAddressEntry billingAddress = orderEntry.getBillingAddress();
		CustomerOrderBillingAddressEntry customerBillingAddress = new CustomerOrderBillingAddressEntry(
				billingAddress.getBillingFirstName(), billingAddress.getBillingLastName(),
				billingAddress.getBillingAddress(), billingAddress.getBillingCity(), billingAddress.getBillingCounty(),
				billingAddress.getBillingState(), billingAddress.getBillingZipCode(),
				billingAddress.getBillingMobile(), billingAddress.getBillingEmail());

		return customerBillingAddress;
	}

	private void appendWMSItemDetail(CustomerOrderShipmentEntry singleShipmentEntry) throws ERPServiceException {

		HashMap<Long, String> itemBarCodeMap = new HashMap<Long, String>();

		List<ItemEntry> itemEntryList = getServiceUtil().getSKUItemEntry(singleShipmentEntry.getShipmentId());

		if (itemEntryList != null) {
			for (ItemEntry wmsItemEntry : itemEntryList) {
				if (itemBarCodeMap.get(wmsItemEntry.getSku().getId()) == null) {
					itemBarCodeMap.put(wmsItemEntry.getSku().getId(), wmsItemEntry.getBarcode());
				} else {
					String barCode = itemBarCodeMap.get(wmsItemEntry.getSku().getId());
					itemBarCodeMap.put(wmsItemEntry.getSku().getId(), barCode + " " + wmsItemEntry.getBarcode());
				}
			}
		} else {
			return;
		}

		for (CustomerOrderItemEntry orderItemEntry : singleShipmentEntry.getOrderItems()) {
			String barCodeList = itemBarCodeMap.get(orderItemEntry.getSkuId());
			if (barCodeList != null) {
				orderItemEntry.setItemBarcodes(barCodeList);
			}
		}

	}

	@Override
	public AbstractResponse create(CustomerOrderEntry e) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse update(CustomerOrderEntry e, Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse search(int i, int i1, String string, String string1, String string2)
			throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public AbstractResponse findById(Long l) throws ERPServiceException {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
