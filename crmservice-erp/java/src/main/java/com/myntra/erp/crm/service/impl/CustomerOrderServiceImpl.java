package com.myntra.erp.crm.service.impl;

import org.springframework.transaction.TransactionSystemException;

import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.response.AbstractResponse;
import com.myntra.commons.service.impl.BaseServiceImpl;
import com.myntra.erp.crm.client.entry.CustomerOrderEntry;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.erp.crm.manager.CustomerOrderManager;
import com.myntra.erp.crm.service.CustomerOrderService;

/**
 * Customer order search web service implementation which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerOrderServiceImpl extends BaseServiceImpl<CustomerOrderResponse, CustomerOrderEntry> implements
		CustomerOrderService {

	@Override
	public AbstractResponse getOrderDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms, boolean isWarehouseDetailNeeded, boolean isSKUDetailNeeded,
			boolean isLogisticDetailNeeded, boolean isPaymentLogNeeded) {
		try {
			return ((CustomerOrderManager) getManager()).getOrderDetails(start, fetchSize, sortBy, sortOrder,
					searchTerms, isWarehouseDetailNeeded, isSKUDetailNeeded, isLogisticDetailNeeded, isPaymentLogNeeded);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
	
	@Override
	public AbstractResponse getOrderMinimumDetails(int start, int fetchSize, String sortBy, String sortOrder,
			String searchTerms) {
		try {
			return ((CustomerOrderManager) getManager()).getOrderMinimumDetails(start, fetchSize, sortBy, sortOrder,
					searchTerms);

		} catch (ERPServiceException e) {
			return e.getResponse();

		} catch (TransactionSystemException ex) {
			return ((ERPServiceException) ex.getApplicationException()).getResponse();
		}

	}
}
