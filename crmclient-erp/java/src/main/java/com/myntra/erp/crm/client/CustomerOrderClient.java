package com.myntra.erp.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.erp.crm.client.response.CustomerOrderResponse;
import com.myntra.erp.crm.client.util.ClientUtils;
import com.myntra.erp.crm.constant.CRMErpClientConstants;

/**
 * Web client for customer order search web service which integrates detail of
 * order, shipments, skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
public class CustomerOrderClient extends ResponseHandler {

	public static final String SERVICE_PREFIX = "/crm/order/";

	public static CustomerOrderResponse getCustomerOrderDetails(String serviceURL, String login, int limit,
			boolean isDetailNeeded) throws ERPServiceException {

		String searchTerms = "login.eq:" + login;

		if (ClientUtils.isNumeric(login)) {
			if (login.trim().length() <= 8) {
				// Search is for Order ID
				searchTerms = "id.eq:" + login;
			} else if (login.trim().length() == 10) {
				searchTerms = "userContactNo.eq:" + login;
			}
		}

		return getCustomerOrderDetails(serviceURL, 0, limit, "createdOn", "DESC", searchTerms, isDetailNeeded,
				isDetailNeeded, isDetailNeeded, isDetailNeeded);
	}

	public static CustomerOrderResponse getCustomerOrderMinimumDetails(String serviceURL, String login, int limit)
			throws ERPServiceException {

		String searchTerms = "login.eq:" + login;

		if (ClientUtils.isNumeric(login)) {
			if (login.trim().length() <= 8) {
				// Search is for Order ID
				searchTerms = "id.eq:" + login;
			} else if (login.trim().length() == 10) {
				searchTerms = "userContactNo.eq:" + login;
			}
		}

		return getCustomerOrderMinimumDetails(serviceURL, 0, limit, "createdOn", "DESC", searchTerms);
	}

	public static CustomerOrderResponse getCustomerOrderDetails(String serviceURL, String login, int limit)
			throws ERPServiceException {
		return getCustomerOrderDetails(serviceURL, login, limit, false);
	}

	// client api to call customer order service
	public static CustomerOrderResponse getCustomerOrderDetails(String serviceURL, int start, int fetchSize,
			String sortBy, String sortOrder, String searchTerms, boolean isWarehouseDetailNeeded,
			boolean isSKUDetailNeeded, boolean isLogisticDetailNeeded, boolean isPaymentLogNeeded)
			throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX);
		client.query("start", start);
		client.query("fetchSize", fetchSize);

		if (sortBy != null)
			client.query("sortBy", sortBy);
		if (sortOrder != null)
			client.query("sortOrder", sortOrder);
		if (searchTerms != null)
			client.query("q", searchTerms);

		client.query("isWarehouseDetailNeeded", isWarehouseDetailNeeded);
		client.query("isSKUDetailNeeded", isSKUDetailNeeded);
		client.query("isLogisticDetailNeeded", isLogisticDetailNeeded);
		client.query("isPaymentLogNeeded", isPaymentLogNeeded);

		client.setReceiveTimeOut(60 * 1000);

		return client.get(CustomerOrderResponse.class);
	}

	// client api to call customer order service
	public static CustomerOrderResponse getCustomerOrderMinimumDetails(String serviceURL, int start, int fetchSize,
			String sortBy, String sortOrder, String searchTerms) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL,
				TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX + "/minimum/");
		client.query("start", start);
		client.query("fetchSize", fetchSize);

		if (sortBy != null)
			client.query("sortBy", sortBy);
		if (sortOrder != null)
			client.query("sortOrder", sortOrder);
		if (searchTerms != null)
			client.query("q", searchTerms);

		return client.get(CustomerOrderResponse.class);
	}	

	public static void main(String args[]) throws ERPServiceException {

		// customer order service test
		CustomerOrderResponse orderResponse = getCustomerOrderDetails("http://localhost:7090/crmservice-erp",
				"arun.kumar@myntra.com", 5, true);
		System.out.println(orderResponse);

		// customer order minimum service test
		CustomerOrderResponse orderMinimumResponse = getCustomerOrderMinimumDetails(
				"http://localhost:7090/crmservice-erp", "arun.kumar@myntra.com", 5);
		System.out.println(orderMinimumResponse);

	}
}