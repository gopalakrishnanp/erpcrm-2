package com.myntra.erp.crm.client;

import com.myntra.commons.client.BaseWebClient;
import com.myntra.commons.exception.ERPServiceException;
import com.myntra.commons.test.TestHelper;
import com.myntra.commons.utils.ResponseHandler;
import com.myntra.erp.crm.client.response.CashbackResponse;
import com.myntra.erp.crm.constant.CRMErpClientConstants;

/**
 * Web client for all the contact web services
 */
public class CashbackClient extends ResponseHandler {

	public static final String SERVICE_PREFIX_PERSON = "/crm/cashback/";

	public static CashbackResponse getCashbackLogs(String serviceURL, String loginID) throws ERPServiceException {

		BaseWebClient client = new BaseWebClient(serviceURL, CRMErpClientConstants.CRM_ERP_URL, TestHelper.dummyContextInfo1);
		client.path(SERVICE_PREFIX_PERSON);
		client.query("login", loginID);
		return client.get(CashbackResponse.class);
	}

	public static void main(String args[]) throws ERPServiceException {
		CashbackResponse test = getCashbackLogs("http://localhost:7090/crmservice-erp", "pravin.mehta@myntra.com");
		System.out.println(test);
	}
}