package com.myntra.erp.crm.constant;

/**
 * Constants required for crm portal client 
 * @author arunkumar
 *
 */
public class CRMErpClientConstants {
	public static final String CRM_ERP_URL = "crmErpURL";
	
}
