package com.myntra.erp.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CustomerOrderItemEntry;

/**
 * Response for customer order search web service with detail of order
 * items(skus)
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "CustomerOrderItemResponse")
public class CustomerOrderItemResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;	
	private List<CustomerOrderItemEntry> customerOrderItemEntryList;
	
	@XmlElementWrapper(name = "customerOrderItemEntryList")
	@XmlElement(name = "customerOrderItemEntry")	
	public List<CustomerOrderItemEntry> getCustomerOrderItemEntryList() {
		return customerOrderItemEntryList;
	}

	public void setCustomerOrderItemEntryList(List<CustomerOrderItemEntry> customerOrderItemEntryList) {
		this.customerOrderItemEntryList = customerOrderItemEntryList;
	}

	public CustomerOrderItemResponse() {
	}

	@Override
	public String toString() {
		return "CustomerOrderItemResponse [customerOrderItemEntryList=" + customerOrderItemEntryList + "]";
	}
}