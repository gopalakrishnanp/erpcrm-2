package com.myntra.erp.crm.client.entry;

import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.entries.BaseEntry;

/**
 * Entry for Cashback balance fields
 * 
 * @author Pravin Mehta
 */
@XmlRootElement(name = "cashbackBalanceEntry")
public class CashbackBalanceEntry extends BaseEntry {
	
	private static final long serialVersionUID = 1;
	private Double cashbackBalance = 0.0;
	private Double storeCreditBalance = 0.0;
	private Double earnedCreditBalance = 0.0;
	
	public CashbackBalanceEntry() {
	}
	
	public Double getCashbackBalance() {
		return cashbackBalance;
	}

	public void setCashbackBalance(Double cashbackBalance) {
		this.cashbackBalance = cashbackBalance;
	}

	public Double getStoreCreditBalance() {
		return storeCreditBalance;
	}

	public void setStoreCreditBalance(Double storeCreditBalance) {
		this.storeCreditBalance = storeCreditBalance;
	}

	public Double getEarnedCreditBalance() {
		return earnedCreditBalance;
	}

	public void setEarnedCreditBalance(Double earnedCreditBalance) {
		this.earnedCreditBalance = earnedCreditBalance;
	}

	@Override
	public String toString() {
		return "CashbackBalanceEntry [cashbackBalance=" + cashbackBalance + ", storeCreditBalance="
				+ storeCreditBalance + ", earnedCreditbalance=" + earnedCreditBalance + "]";
	}
}
