package com.myntra.erp.crm.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.myntra.commons.response.AbstractResponse;
import com.myntra.erp.crm.client.entry.CustomerReturnEntry;

/**
 * Response for customer order search web service with detail of shipments,
 * skus, warehouse, tracking, trip detail etc..
 * 
 * @author Arun Kumar
 */
@XmlRootElement(name = "customerReturnResponse")
public class CustomerReturnResponse extends AbstractResponse {

	private static final long serialVersionUID = 1L;
	private List<CustomerReturnEntry> customerReturnEntryList;

	public CustomerReturnResponse() {
		super();
	}
	
	@XmlElementWrapper(name = "customerReturnEntryList")
	@XmlElement(name = "customerReturnEntry")
	public List<CustomerReturnEntry> getCustomerReturnEntryList() {
		return customerReturnEntryList;
	}	

	public void setCustomerReturnEntryList(List<CustomerReturnEntry> customerReturnEntryList) {
		this.customerReturnEntryList = customerReturnEntryList;
	}

	@Override
	public String toString() {
		return "CustomerReturnResponse [customerReturnEntryList=" + customerReturnEntryList + "]";
	}

}